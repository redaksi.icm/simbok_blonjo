-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.22-MariaDB-0ubuntu0.19.10.1 - Ubuntu 19.10
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table simbok_marketplace.img_produk
DROP TABLE IF EXISTS `img_produk`;
CREATE TABLE IF NOT EXISTS `img_produk` (
  `id` varchar(255) NOT NULL,
  `produk_id` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `ins_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table simbok_marketplace.img_produk: ~37 rows (approximately)
DELETE FROM `img_produk`;
/*!40000 ALTER TABLE `img_produk` DISABLE KEYS */;
INSERT INTO `img_produk` (`id`, `produk_id`, `img`, `ins_number`) VALUES
	('0bac11409b81cbc84bead7b313025ff3', '8b6d8cebefa200b5fb5fb0ceee709333', '9944675c1508c15870deacfb964d8291.jpeg', 9),
	('1b9ab037acd0d9e67bbe971988c6485b', '33e59922702ba8308ab2855565fa8a3a', '1d191da9b013342e41cf0b969f9a7af2.png', 2),
	('1ea13cb7f6f3335571aafdb6b4ea78a9', '8b6d8cebefa200b5fb5fb0ceee709333', '8bd6ac7b1e5d9460bcba148e38c2508b.jpg', 11),
	('276ce1d7df2adfa621f27bc67dcd85c5', 'b0693d4eba1cac805b4a4d450209e926', '3ad83944d58168e6e58cc83563b2e96e.jpeg', 0),
	('2b6cd7124cfaeae5666d0d4671454cf5', '9fdedb085a3536b05633d73ce392bd78', '9cce82c6abe517d80ec418bec4ba7c89.jpg', 0),
	('3385cf2084c68ae84f56b4e7cf9de4fc', '15964d93520290c00169dab2fe3b596c', '5b3cb1185aba2bf30cb73bfc4865d8e1.jpg', 0),
	('39c2038984f831d579538f8cff5e6d3f', '8b6d8cebefa200b5fb5fb0ceee709333', '35d34b5b49314b3965b776e5dc638678.jpeg', 13),
	('3a01e653b4a5d9e2ece88d512f773836', '0d4dc0ef494459cfce4af3f3d4160e35', 'e3f54459092dc6cff847eac1fd33b235.png', 3),
	('3c36489d932da4e6704317b3e638847f', '8b6d8cebefa200b5fb5fb0ceee709333', 'e943640edda83a140b3b5396df67ecdc.jpg', 0),
	('4365aeff047ddf2e54a6a5730def060a', '9022cf923be728661430752d393f2cdb', '32b7e0f76821c83253240269db3ca384.jpg', 0),
	('44e2a2f790c42d7a35b2a002df0c3751', 'd673cac9094a178f1d293646b1eef04d', '92d1c5e049c1601c8aeaf55c94afbd37.jpeg', 0),
	('470663bdd99bda106f1775b867e18322', 'c316a3a644ac18d864418a8318cdb278', '811de8097dd9bf2f1b20e7f80611340c.jpg', 0),
	('4c9b6c1c46a9991c6670364a9d59941f', 'ffcb71923c6b884d32e4890fa83df6e6', 'e61b73220e2db5dcbe979909f4ec1b55.png', 2),
	('51fc187b4e85a78b16a30b2c96ce44e3', 'ffcb71923c6b884d32e4890fa83df6e6', '1aa344f2a7216328965aafed7d5a28a6.jpg', 2),
	('5ba51bb57a3a336ea167a8c0d5d1d5a3', 'eb97cbe2c58f89247d3284de6201d2ae', '3674182ea5c07426506bd235b0bb3a64.jpg', 0),
	('5dbc82dea9b7440a3a54df14f71d8111', '8b6d8cebefa200b5fb5fb0ceee709333', 'e897ff18d15b09eadaa0c1ad7b53aaa4.jpg', 12),
	('5fb1073da739e0dceaf24299a4c63720', '8b6d8cebefa200b5fb5fb0ceee709333', 'c1301b2d0348c05fba02bf9e2efbaf51.jpg', 2),
	('625431b412ae547ec683256f6411b0c0', '0d4dc0ef494459cfce4af3f3d4160e35', 'd8f455f9ced6d69a19ed8b5414c8c752.png', 2),
	('6713ff72f69cf076ca3518d38bc9ab7c', 'ffcb71923c6b884d32e4890fa83df6e6', '6a29c8b746cb3177797451e37e3dd187.png', 2),
	('687b8291ae7fb0e271d8effc4f071309', '0d4dc0ef494459cfce4af3f3d4160e35', 'c865bbb8957f1db2e2f99ba98d39ff09.png', 4),
	('6d52062adb828cd07ef0a89c91a8869f', 'c1fde0fbd365b762db31852c0f819de2', 'dc1ed5a47a64c08dd8d284a8eb068adc.jpeg', 0),
	('70403747278b5927a3435551193980ca', 'eb97cbe2c58f89247d3284de6201d2ae', '5b289cb29d596875467030c5d569948b.jpg', 2),
	('75e4d47ce97316c04ccba0f3958bb103', '8b6d8cebefa200b5fb5fb0ceee709333', '703028e694931960dd0d3aea92fde947.jpg', 3),
	('806eea93720a6085d4553b0c7e8cbaa3', '8b6d8cebefa200b5fb5fb0ceee709333', 'c32dcde8dfb2132cd50b890fb9662448.jpg', 2),
	('94b7e001ad3e85300a4a2953ff98f2eb', 'c27495e81dbbdddb58e01334cec07c87', '3becfdf20e6868fd183f2049bbed4afe.jpg', 0),
	('a09130ec8a121de2d3090db0d2819a4c', '1b2f7ed6cc436d849f8254000eed7ba8', 'a4308a034af36a7469009abfa9b9a40f.png', 0),
	('aed997c3ef3fa26b9024815c62358773', '8b6d8cebefa200b5fb5fb0ceee709333', 'a9cc308a48efd383e8613daa25d09929.jpeg', 10),
	('b52647797724266530ca69d0f24bff13', '8b6d8cebefa200b5fb5fb0ceee709333', 'f92fa85c2f79b19f50d40819881e3e0a.jpg', 8),
	('ba3baa27cfd66b9d8ba339d3bb828e08', '8b6d8cebefa200b5fb5fb0ceee709333', '37805f8c46fd0542784f926d152c20f4.jpg', 5),
	('c62a8ad34fa74738aaa80f8382aa7a9a', '576aa4d9aeee3044e15ef507da053b5e', 'c6bb332362d663fcb42b9bd94a188ac5.jpg', 0),
	('cfc5c8cc36fb6c9e6afdc97d4f5c4652', '8b6d8cebefa200b5fb5fb0ceee709333', '1a07ca2933fa36853c90db8b51b98633.jpg', 6),
	('d203edbdef4ef3d2a0177c14b2ad6d64', '8b6d8cebefa200b5fb5fb0ceee709333', '1d4a7af241f756bc72add343eeeb6a9b.jpeg', 7),
	('dd532c5def31a3141d689a9d98101db7', '8b6d8cebefa200b5fb5fb0ceee709333', 'c8ede9253c55825e1d9f57b97b60ce72.jpg', 4),
	('e7f8d4f0879a196f04321aec2ac30b9d', '8b6d8cebefa200b5fb5fb0ceee709333', 'e362536be03072fdc406438adef3f3e3.jpg', 14),
	('ecf57cb27f7287e58ae28e818adb63b8', '70f23f4cf53409138972a7f44fff3bdd', '75c0d0ec9743baedb8cf10e6d684db99.jpg', 0),
	('ef689ca90a7cc6b72a8fa8f0967b7749', '82f26ef424f65f2e4c72ee58aab7411f', 'a8cabd43eb2bfacd1314ac6bce4838ac.jpeg', 0);
/*!40000 ALTER TABLE `img_produk` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.kecamatan
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE IF NOT EXISTS `kecamatan` (
  `id` varchar(10) NOT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `lat` varchar(50) DEFAULT NULL,
  `lng` varchar(50) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table simbok_marketplace.kecamatan: ~26 rows (approximately)
DELETE FROM `kecamatan`;
/*!40000 ALTER TABLE `kecamatan` DISABLE KEYS */;
INSERT INTO `kecamatan` (`id`, `kecamatan`, `lat`, `lng`, `created`, `created_by`, `updated`, `updated_by`, `is_active`, `is_deleted`) VALUES
	('010', 'AYAH', '-7.7073199', '109.3493649', '2019-02-07 05:49:15', 'System', '2019-04-01 11:19:15', 'Super Administrator', 1, 0),
	('020', 'BUAYAN', '-7.6829503', '109.3968238', '2019-02-07 05:49:15', 'System', '2019-04-01 11:22:47', NULL, 1, 0),
	('030', 'PURING', '-7.7288645', '109.4860527', '2019-02-07 05:49:15', 'System', '2019-04-01 11:23:06', NULL, 1, 0),
	('040', 'PETANAHAN', '-7.7334876', '109.5578573', '2019-02-07 05:49:15', 'System', '2019-04-01 11:23:28', NULL, 1, 0),
	('050', 'KLIRONG', '-7.7366363', '109.5611974', '2019-02-07 05:49:15', 'System', '2019-04-01 11:24:17', NULL, 1, 0),
	('060', 'BULUSPESANTREN', '-7.7507281', '109.6472502', '2019-02-07 05:49:15', 'System', '2019-04-01 11:24:35', NULL, 1, 0),
	('070', 'AMBAL', '-7.7654471', '109.6977118', '2019-02-07 05:49:15', 'System', '2019-04-01 11:24:51', NULL, 1, 0),
	('080', 'MIRIT', '-7.7744052', '109.7255103', '2019-02-07 05:49:15', 'System', '2019-04-01 11:25:08', NULL, 1, 0),
	('081', 'BONOROWO', '-7.7615895', '109.7784262', '2019-02-07 05:49:15', 'System', '2019-04-01 11:25:40', NULL, 1, 0),
	('090', 'PREMBUN', '-7.6997366', '109.7653447', '2019-02-07 05:49:15', 'System', '2019-04-01 11:25:53', NULL, 1, 0),
	('091', 'PADURESO', '-7.633832', '109.7531382', '2019-02-07 05:49:15', 'System', '2019-04-01 11:25:58', NULL, 1, 0),
	('100', 'KUTOWINANGUN', '-7.7006095', '109.7123753', '2019-02-07 05:49:15', 'System', '2019-04-01 11:26:15', NULL, 1, 0),
	('110', 'ALIAN', '-7.6321185', '109.6747153', '2019-02-07 05:49:15', 'System', '2019-04-01 11:26:44', NULL, 1, 0),
	('111', 'PONCOWARNO', '-7.6558927', '109.7149602', '2019-02-07 05:49:15', 'System', '2019-04-01 11:26:50', NULL, 1, 0),
	('120', 'KEBUMEN', '-7.6825786', '109.6456957', '2019-02-07 05:49:15', 'System', '2019-04-01 11:27:15', NULL, 1, 0),
	('130', 'PEJAGOAN', '-7.6394169', '109.5712279', '2019-02-07 05:49:15', 'System', '2019-04-01 11:27:19', NULL, 1, 0),
	('140', 'SRUWENG', '-7.6435644', '109.5376588', '2019-02-07 05:49:15', 'System', '2019-04-01 11:27:38', NULL, 1, 0),
	('150', 'ADIMULYO', '-7.6694161', '109.5228152', '2019-02-07 05:49:15', 'System', '2019-04-01 11:27:58', NULL, 1, 0),
	('160', 'KUWARASAN', '-7.6663686', '109.4814402', '2019-02-07 05:49:15', 'System', '2019-04-01 11:28:23', NULL, 1, 0),
	('170', 'ROWOKELE', '7.6048004', '109.3758889', '2019-02-07 05:49:15', 'System', '2019-04-01 11:28:28', NULL, 1, 0),
	('180', 'SEMPOR', '-7.5604484', '109.4400113', '2019-02-07 05:49:15', 'System', '2019-04-01 11:28:52', NULL, 1, 0),
	('190', 'GOMBONG', '-7.6082297', '109.5013498', '2019-02-07 05:49:15', 'System', '2019-04-01 11:29:00', NULL, 1, 0),
	('200', 'KARANGANYAR', '-7.6052836', '109.5378852', '2019-02-07 05:49:15', 'System', '2019-04-01 11:29:19', NULL, 1, 0),
	('210', 'KARANGGAYAM', '7.548563', '109.5470738', '2019-02-07 05:49:15', 'System', '2019-04-01 11:29:45', NULL, 1, 0),
	('220', 'SADANG', '7.5008147', '109.6903903', '2019-02-07 05:49:15', 'System', '2019-04-01 11:29:51', NULL, 1, 0),
	('221', 'KARANGSAMBUNG', '-7.5627214,', '109.6362137', '2019-02-07 05:49:15', 'System', '2019-04-01 11:30:06', NULL, 1, 0);
/*!40000 ALTER TABLE `kecamatan` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.kelurahan
DROP TABLE IF EXISTS `kelurahan`;
CREATE TABLE IF NOT EXISTS `kelurahan` (
  `id` varchar(10) NOT NULL,
  `id_kecamatan` varchar(10) NOT NULL DEFAULT '',
  `kelurahan` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(50) NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`,`id_kecamatan`),
  KEY `kelurahan_has_kecamatan` (`id_kecamatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table simbok_marketplace.kelurahan: ~460 rows (approximately)
DELETE FROM `kelurahan`;
/*!40000 ALTER TABLE `kelurahan` DISABLE KEYS */;
INSERT INTO `kelurahan` (`id`, `id_kecamatan`, `kelurahan`, `created`, `created_by`, `updated`, `updated_by`, `is_active`, `is_deleted`) VALUES
	('001', '010', 'AYAH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '020', 'KARANGBOLONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '030', 'TAMBAKMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '040', 'KARANGREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '050', 'JOGOSIMO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '060', 'AYAMPUTIH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '070', 'PLEMPUKAN KEMBARAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '080', 'MIRITPETIKUSAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '081', 'PATUKREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '090', 'TERSOBO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '091', 'PEJENGKOLAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '100', 'PEKUNDEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '111', 'JATIPURUS', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '120', 'MUKTISARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '130', 'LOGEDE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '140', 'MENGANTI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '150', 'SUGIHWARAS', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '160', 'KAMULYAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '170', 'REDISARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '180', 'SIDOHARUM', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '190', 'KALITENGAH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '200', 'SIDOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '210', 'KARANGGAYAM', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('001', '221', 'WIDORO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '010', 'CANDIRENGGO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '020', 'JLADRI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '030', 'SUROREJAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '040', 'KARANGGADUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '050', 'TANGGULANGIN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '060', 'SETROJENAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '070', 'ENTAK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '080', 'TLOGO DEPOK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '081', 'NGASINAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '090', 'PREMBUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '091', 'BALINGASAL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '100', 'TANJUNGMERU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '111', 'LEREP KEBUMEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '120', 'MUKTIREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '130', 'KEWAYUHAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '140', 'TRIKARSO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '150', 'TAMBAKHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '160', 'SIDOMUKTI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '170', 'KALISARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '180', 'SELOKERTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '190', 'KEMUKUS', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '200', 'PANJATAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '210', 'KAJORAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('002', '221', 'SELING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '010', 'MANGUNWENI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '020', 'ADIWARNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '030', 'WALUYOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '040', 'TEGALRETNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '050', 'PANDANLOR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '060', 'BRECONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '070', 'KENOYOJAYAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '080', 'MIRIT', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '081', 'PUJODADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '090', 'KEBEKELAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '091', 'MERDEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '100', 'KUWARISAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '111', 'BLATER', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '120', 'DEPOKREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '130', 'KEDAWUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '140', 'SIDOHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '150', 'TEPAKYANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '160', 'TAMBAKSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '170', 'PRINGTUTUL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '180', 'KALIBEJI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '190', 'BANJARSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '200', 'KARANGANYAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '210', 'KARANGTENGAH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('003', '221', 'PENCIL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '010', 'TLOGOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '020', 'RANGKAH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '030', 'SIDOHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '040', 'AMPELSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '050', 'TAMBAKPROGATEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '060', 'BANJURPASAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '070', 'AMBALRESMI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '080', 'TLOGOPRAGOTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '081', 'BALOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '090', 'TUNGGALROSO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '091', 'KALIJERING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '100', 'KUTOWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '111', 'PONCOWARNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '120', 'MENGKOWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '130', 'PEJAGOAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '140', 'GIWANGRETNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '150', 'SIDOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '160', 'KALIPURWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '170', 'ROWOKELE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '180', 'JATINEGARA', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '190', 'PANJANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '200', 'JATILUHUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '210', 'KARANGMOJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('004', '221', 'KEDUNGWARU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '010', 'KALIBANGKANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '020', 'WONODADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '030', 'PULIHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '040', 'MUNGGU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '050', 'KEDUNGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '060', 'INDROSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '070', 'KAIBONPETANGKURAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '080', 'LEMBUPURWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '081', 'TLOGOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '090', 'KEDUNGWARU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '091', 'KALIGUBUG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '100', 'LUNDONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '111', 'TEGALREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '120', 'GESIKAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '130', 'KEBULUSAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '140', 'JABRES', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '150', 'WOJOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '160', 'PURWODADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '170', 'BUMIAGUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '180', 'BEJIRUYUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '190', 'PATEMON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '200', 'CANDI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '210', 'PENIMBUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('005', '221', 'KALIGENDING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '010', 'WATUKELIR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '020', 'GEBLUG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '030', 'PURWOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '040', 'KEWANGUNAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '050', 'BENDOGARAP', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '060', 'BULUSPESANTREN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '070', 'KAIBON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '080', 'WIROMARTAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '081', 'ROWOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '090', 'BAGUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '091', 'SIDOTOTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '100', 'MEKARSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '111', 'JEMBANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '120', 'KALIBAGOR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '130', 'ADITIRTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '140', 'SRUWENG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '150', 'CANDIWULAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '160', 'PONDOKGEBANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '170', 'JATILUHUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '180', 'PEKUNCEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '190', 'KEDUNGPUJI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '200', 'GIRIPURNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '210', 'KALIREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('006', '221', 'PLUMBON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '010', 'KALIPOH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '020', 'ROGODADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '030', 'KRANDEGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '040', 'KARANGDUWUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '050', 'KLEGENREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '060', 'BANJURMUKADAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '070', 'SUMBERJATI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '080', 'ROWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '081', 'BONOROWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '090', 'SIDOGEDE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '091', 'RAHAYU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '100', 'BABADSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '111', 'KEDUNGDOWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '120', 'ARGOPENI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '130', 'KARANGPOH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '140', 'KARANGGEDANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '150', 'ADIKARTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '160', 'HARJODOWO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '170', 'KRETEK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '180', 'KEDUNGJATI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '190', 'WERO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '200', 'PLARANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '210', 'PAGEBANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('007', '221', 'PUJOTIRTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '010', 'ARGOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '020', 'PAKURAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '030', 'KALENG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '040', 'PETANAHAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '050', 'GEBANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '060', 'WALUYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '070', 'BLENGOR WETAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '080', 'SINGOYUDAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '081', 'SIRNOBOYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '090', 'SEMBIRKADIPATEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '091', 'SENDANGDALEM', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '100', 'UNGARAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '111', 'KARANGTENGAH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '120', 'JATISARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '130', 'JEMUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '140', 'PURWODESO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '150', 'ADIMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '160', 'LEMAHDUWUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '170', 'SUKOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '180', 'SEMALI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '190', 'GOMBONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '200', 'KARANG KEMIRI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '210', 'CLAPAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('008', '221', 'WADASMALANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '010', 'BANJARHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '020', 'BUAYAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '030', 'TUKINGGEDONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '040', 'GROGOLPENATUS', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '050', 'KLIRONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '060', 'BOCOR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '070', 'BLENGOR KULON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '080', 'WERGONAYAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '081', 'BONJOK KIDUL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '090', 'KEDUNGBULUS', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '091', 'PADURESO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '100', 'MRINEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '111', 'TIRTOMOYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '120', 'KALIREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '130', 'PRIGI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '140', 'KLEPUSANGGAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '150', 'TEMANGGAL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '160', 'MADURESO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '170', 'GIYANTI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '180', 'BONOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '190', 'WONOKRIYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '200', 'WONOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '210', 'LOGANDU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('009', '221', 'TLEPOK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '010', 'ARGOPENI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '020', 'SIKAYU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '030', 'PURWOHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '040', 'GROGOLBENINGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '050', 'KLEGENWONOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '060', 'MADURETNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '070', 'BENERWETAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '080', 'SELOTUMPENG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '081', 'BONJOK LOR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '090', 'MULYASRI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '100', 'PEJAGATAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '111', 'SOKA', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '120', 'SELANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '130', 'KEBAGORAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '140', 'TANGGERAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '150', 'JOHO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '160', 'MANGLI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '170', 'WONOHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '180', 'SEMPOR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '190', 'SEMONDO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '200', 'GRENGGENG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '210', 'KEBAKALAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('010', '221', 'KALISONO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '010', 'KARANGDUWUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '020', 'KARANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '030', 'SITIADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '040', 'JOGOMERTAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '050', 'JERUKAGUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '060', 'AMBALKUMOLO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '070', 'BENERKULON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '080', 'PATUKREJOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '081', 'MRENTUL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '090', 'PESUNINGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '100', 'TRIWARNO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '110', 'BOJONGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '111', 'KEBAPANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '120', 'ADIKARSO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '130', 'PENGARINGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '140', 'KARANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '150', 'ADILUHUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '160', 'KUWARASAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '170', 'WAGIRPANDAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '180', 'TUNJUNGSETO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '190', 'SEMANDING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '200', 'POHKUMBANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '210', 'KARANGREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('011', '221', 'LANGSE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '010', 'SRATI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '020', 'ROGODONO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '030', 'BANJAREJA', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '040', 'TANJUNGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '050', 'RANTEREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '060', 'RANTEWRINGIN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '070', 'AMBALKLIWONAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '080', 'SITIBENTAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '090', 'PECARIKAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '100', 'KOROWELANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '110', 'SUROTRUNAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '120', 'TAMANWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '130', 'PENIRON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '140', 'KARANGPULE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '150', 'TEGALSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '160', 'GANDUSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '180', 'SAMPANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '190', 'SIDAYU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '210', 'WONOTIRTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('012', '221', 'BANIORO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '010', 'JINTUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '020', 'BANYUMUDAL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '030', 'WETON KULON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '040', 'GRUJUGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '050', 'KARANGGLONGGONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '060', 'TAMBAKREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '070', 'PASARSENEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '080', 'KARANGGEDE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '090', 'KABUARAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '100', 'JLEGIWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '110', 'KAMBANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '120', 'PANJER', '2019-02-07 08:59:16', '', '2019-02-25 10:57:56', 'Super Administrator', 1, 0),
	('013', '130', 'WATULAWANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '140', 'PAKURAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '150', 'SEKARTEJA', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '160', 'O R I', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '180', 'DONOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '190', 'WONOSIGRO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '210', 'KALIBENING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('013', '221', 'KARANGSAMBUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '010', 'PASIR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '020', 'TUGU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '030', 'PASURUHAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '040', 'KEBONSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '050', 'JATIMALANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '060', 'SANGUBANYU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '070', 'PUCANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '080', 'WIROGATEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '100', 'LUMBU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '110', 'JATIMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '120', 'KEMBARAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '140', 'PENGEMPON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '150', 'KEMUJAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '160', 'SERUT', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '180', 'KEDUNGWRINGIN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '190', 'KLOPOGODO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '210', 'GUNUNGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('014', '221', 'TOTOGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '010', 'JATIJAJAR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '020', 'NOGORAJI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '030', 'WETON WETAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '040', 'TRESNOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '050', 'KALIWUNGU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '060', 'ARJOWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '070', 'AMBALKEBREK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '080', 'PEKUTAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '100', 'TANJUNGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '110', 'TANUHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '120', 'SUMBERADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '140', 'KEJAWANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '150', 'MANGUNHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '160', 'BANJAREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '180', 'KENTENG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '210', 'GINANDONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('015', '220', 'PUCANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '010', 'DEMANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '020', 'MERGOSONO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '030', 'KEDALEMAN KULON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '040', 'PODOURIP', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '050', 'SITIREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '060', 'AMPIH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '070', 'GONDANGLEGI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '080', 'MANGUNRANAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '100', 'KALIPUTIH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '110', 'KARANGTANJUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '120', 'WONOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '140', 'KARANGJAMBU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '150', 'BANYUROTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '160', 'GUMAWANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '180', 'SOMOGEDE', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '210', 'BINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('016', '220', 'SEBORO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '010', 'KEDUNGWERU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '020', 'SEMAMPIR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '030', 'KEDALEMAN WETAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '040', 'NAMPUDADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '050', 'GADUNGREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '060', 'JOGOPATEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '070', 'BANJARSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '080', 'KERTODESO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '100', 'TUNJUNGSETO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '110', 'KEMANGGUAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '120', 'ROWOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '140', 'SIDOAGUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '150', 'MELES', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '160', 'WONOYOSO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '210', 'GLONTOR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('017', '220', 'WONOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '010', 'BULUREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '020', 'JOGOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '030', 'SRUSUHJURUTENGAH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '040', 'KRITIG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '050', 'TAMBAKAGUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '060', 'KLOPOSAWIT', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '070', 'LAJER', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '100', 'PESALAKAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '110', 'KALIJOYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '120', 'TANAHSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '140', 'DONOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '150', 'CARUBAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '160', 'GUNUNGMUJIL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '210', 'SELOGIRI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('018', '220', 'SADANG KULON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '020', 'PURBOWANGI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '030', 'BUMIREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '040', 'SIDOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '050', 'WOTBUONO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '060', 'SIDOMORO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '070', 'SINGOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '100', 'KARANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '110', 'KARANG KEMBANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '120', 'BANDUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '140', 'PANDANSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '150', 'BONJOK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '160', 'KUWARU', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '210', 'GIRITIRTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('019', '220', 'CANGKRING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '020', 'JATIROTO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '030', 'ARJOWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '040', 'BANJARWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '050', 'KEDUNGWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '060', 'TANJUNGREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '070', 'SIDOLUHUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '110', 'SELILING', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '120', 'CANDIMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '140', 'CONDONGCAMPUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '150', 'ARJOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '160', 'BENDUNGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('020', '220', 'SADANG WETAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '030', 'MADUREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '040', 'JATIMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '050', 'PODOLUHUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '060', 'TANJUNGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '070', 'SINUNGREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '110', 'TLOGOWULUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '120', 'KALIJIREK', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '140', 'PENUSUPAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '150', 'ARJOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '160', 'JATIMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('021', '220', 'KEDUNGGONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '030', 'SIDOBUNDER', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '050', 'DOROWATI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '070', 'AMBARWINANGUN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '080', 'PATUKGAWEMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '120', 'CANDIWULAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '150', 'PEKUWON', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('022', '160', 'SAWANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('023', '030', 'SIDODADI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('023', '050', 'KEBADONGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('023', '070', 'PENEKET', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('023', '110', 'KALIPUTIH', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('023', '120', 'KAWEDUSAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('023', '150', 'SIDOMUKTI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('024', '050', 'BUMIHARJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('024', '070', 'SIDOREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('024', '110', 'WONOKROMO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('024', '120', 'KEBUMEN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('025', '070', 'SIDOMULYO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('025', '110', 'SAWANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('025', '120', 'KUTOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('026', '070', 'SIDOMUKTI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('026', '110', 'KALIRANCANG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('026', '120', 'BUMIREJO', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('027', '070', 'PRASUTAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('027', '110', 'KRAKAL', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('027', '120', 'GEMEKSEKTI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('028', '070', 'KRADENAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('028', '120', 'KARANGSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('029', '070', 'PAGEDANGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('029', '120', 'JEMUR', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('030', '070', 'SUROBAYAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('030', '080', 'NGABEAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('031', '070', 'DUKUHREJOSARI', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('031', '080', 'WINONG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('032', '070', 'KEMBANGSAWIT', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('032', '080', 'SARWOGADUNG', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0),
	('033', '080', 'KRUBUNGAN', '2019-02-07 08:59:16', '', NULL, NULL, 1, 0);
/*!40000 ALTER TABLE `kelurahan` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.ms_kategori
DROP TABLE IF EXISTS `ms_kategori`;
CREATE TABLE IF NOT EXISTS `ms_kategori` (
  `id` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace.ms_kategori: 0 rows
DELETE FROM `ms_kategori`;
/*!40000 ALTER TABLE `ms_kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `ms_kategori` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.pa_kategori
DROP TABLE IF EXISTS `pa_kategori`;
CREATE TABLE IF NOT EXISTS `pa_kategori` (
  `id` varchar(50) NOT NULL,
  `parent_id` varchar(50) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `kategori` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace.pa_kategori: 18 rows
DELETE FROM `pa_kategori`;
/*!40000 ALTER TABLE `pa_kategori` DISABLE KEYS */;
INSERT INTO `pa_kategori` (`id`, `parent_id`, `type`, `kategori`, `icon`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('1', '', 1, 'Pakaian', 'fas fa-tshirt', '2020-03-04 14:10:26', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	('1.2', '1', 2, 'Pakean Anak', '#', '2020-03-13 14:53:48', 'Super Administrator', '2020-03-23 11:39:47', 'Super Administrator', NULL, NULL, 0, 1),
	('2', '', 1, 'Aksesoris', 'fas fa-crown', '2020-03-04 14:12:03', 'Super Administrator', '2020-03-13 10:00:48', 'Super Administrator', NULL, NULL, 0, 1),
	('3', '', 1, 'Elektronik', 'fas fa-tv', '2020-03-04 14:13:59', 'Super Administrator', '2020-03-13 14:22:24', 'Super Administrator', NULL, NULL, 0, 1),
	('1.1', '1', 2, 'Batik', '#', '2020-03-13 11:24:34', 'Super Administrator', '2020-03-23 11:39:30', 'Super Administrator', NULL, NULL, 0, 1),
	('4', '', 1, 'Kerajinan', 'fas fa-palette', '2020-03-13 10:12:06', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	('5', '', 1, 'Makanan', 'fas fa-hamburger', '2020-03-13 10:13:40', 'Super Administrator', '2020-03-13 10:24:09', 'Super Administrator', NULL, NULL, 0, 1),
	('6', '', 1, 'Perlengkapan Dapur', 'fas fa-utensils', '2020-03-13 10:19:53', 'Super Administrator', '2020-03-13 10:24:59', 'Super Administrator', NULL, NULL, 0, 1),
	('7', '', 1, 'Hasil Tani', 'fas fa-seedling', '2020-03-13 10:23:45', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	('8', '', 1, 'Hasil Ternak', 'fas fa-horse', '2020-03-13 10:27:36', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	('9', '', 1, 'Hasil Laut', 'fas fa-ship', '2020-03-13 10:29:22', 'Super Administrator', '2020-03-13 10:29:34', 'Super Administrator', NULL, NULL, 0, 1),
	('1.3', '1', 2, 'Kebaya', '#', '2020-03-13 14:53:57', 'Super Administrator', '2020-03-23 11:40:00', 'Super Administrator', NULL, NULL, 0, 1),
	('1.4', '1', 2, 'Gaun', '#', '2020-03-13 14:54:05', 'Super Administrator', '2020-03-23 11:40:09', 'Super Administrator', NULL, NULL, 0, 1),
	('1.5', '1', 2, 'Jaket', '#', '2020-03-13 14:54:12', 'Super Administrator', '2020-03-23 11:40:36', 'Super Administrator', NULL, NULL, 0, 1),
	('1.6', '1', 2, 'Dress', '#', '2020-03-13 14:54:19', 'Super Administrator', '2020-03-23 11:40:48', 'Super Administrator', NULL, NULL, 0, 1),
	('1.7', '1', 2, 'Pakaian Olahraga', '#', '2020-03-13 14:54:27', 'Super Administrator', '2020-03-23 11:41:48', 'Super Administrator', NULL, NULL, 0, 1),
	('1.8', '1', 2, 'Baju Santai', '#', '2020-03-13 14:54:37', 'Super Administrator', '2020-03-23 11:42:14', 'Super Administrator', NULL, NULL, 0, 1),
	('1.9', '1', 2, 'Jeens', '#', '2020-03-13 14:54:43', 'Super Administrator', '2020-03-23 11:42:58', 'Super Administrator', NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `pa_kategori` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.pa_slide
DROP TABLE IF EXISTS `pa_slide`;
CREATE TABLE IF NOT EXISTS `pa_slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL DEFAULT '',
  `img` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace.pa_slide: 2 rows
DELETE FROM `pa_slide`;
/*!40000 ALTER TABLE `pa_slide` DISABLE KEYS */;
INSERT INTO `pa_slide` (`id`, `judul`, `deskripsi`, `img`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	(11, 'Jajanan Tradisional', 'Lestarikan budaya lokal..!  cinta produk lokal..!', '0920600f118b2796006abbe089d5613e.jpg', '2020-03-16 13:42:01', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	(10, 'Festifal Batik Daerah', 'Ramaikan Festifal Batik Daerah!', '1f2f23e741caac87c33e9bd662dd4d97.jpg', '2020-03-16 12:48:52', 'Super Administrator', '2020-03-16 13:37:44', 'Super Administrator', NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `pa_slide` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.tb_img_produk
DROP TABLE IF EXISTS `tb_img_produk`;
CREATE TABLE IF NOT EXISTS `tb_img_produk` (
  `id` varchar(255) NOT NULL,
  `id_produk` varchar(255) NOT NULL DEFAULT '0',
  `nama_img` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace.tb_img_produk: 0 rows
DELETE FROM `tb_img_produk`;
/*!40000 ALTER TABLE `tb_img_produk` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_img_produk` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.tb_produk
DROP TABLE IF EXISTS `tb_produk`;
CREATE TABLE IF NOT EXISTS `tb_produk` (
  `id` varchar(255) NOT NULL,
  `id_umkm` varchar(255) NOT NULL DEFAULT '0',
  `nama_produk` varchar(255) NOT NULL,
  `harga` int(15) NOT NULL DEFAULT 0,
  `id_induk_kategori` varchar(255) DEFAULT NULL,
  `id_sub_kategori` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kelurahan` varchar(255) DEFAULT NULL,
  `rt` varchar(255) DEFAULT NULL,
  `rw` varchar(255) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace.tb_produk: 13 rows
DELETE FROM `tb_produk`;
/*!40000 ALTER TABLE `tb_produk` DISABLE KEYS */;
INSERT INTO `tb_produk` (`id`, `id_umkm`, `nama_produk`, `harga`, `id_induk_kategori`, `id_sub_kategori`, `kecamatan`, `kelurahan`, `rt`, `rw`, `deskripsi`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('70f23f4cf53409138972a7f44fff3bdd', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Daging Ayam', 23000, '8', '1.1', NULL, NULL, NULL, NULL, 'daging ayam boiler, segar potong di tempat', '2020-03-10 11:48:11', 'Parinah', '2020-03-23 11:14:49', 'Super Administrator', NULL, NULL, 0, 1),
	('c316a3a644ac18d864418a8318cdb278', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Gaun Pengantin Mewah', 2000000, '1', '1.2', NULL, NULL, NULL, NULL, 'Gaun Pengantin yang sangat elegan dan di buat dari bahan ang sangat berkualitas', '2020-03-10 11:44:43', 'Parinah', '2020-03-23 11:27:50', 'Super Administrator', NULL, NULL, 0, 1),
	('9fdedb085a3536b05633d73ce392bd78', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Beras Ketan', 20000, '7', '1.1', NULL, NULL, NULL, NULL, 'Beras ketan kulent dan sangat nikmat', '2020-03-10 11:45:05', 'Parinah', '2020-03-23 11:22:11', 'Super Administrator', NULL, NULL, 0, 1),
	('8b6d8cebefa200b5fb5fb0ceee709333', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Daging sapi Tenderloin', 250000, '8', '1.1', NULL, NULL, NULL, NULL, 'Daging segar asli.. jenis tenderloin tanpa lemak', '2020-03-10 11:47:48', 'Parinah', '2020-03-23 16:16:22', 'Parinah', NULL, NULL, 0, 1),
	('576aa4d9aeee3044e15ef507da053b5e', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Ikan Tongkol Segar', 20000, '9', NULL, NULL, NULL, NULL, NULL, 'Ikan Tongkol segar yang langsung di ambil dari laut', '2020-03-10 11:49:10', 'Parinah', '2020-03-23 11:11:07', 'Super Administrator', NULL, NULL, 0, 1),
	('9022cf923be728661430752d393f2cdb', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Putu Ayu', 2000, '5', '1.1', NULL, NULL, NULL, NULL, 'makanan tradisional Indonesia yang diperjualbelikan di pasar, khususnya di pasar-pasar tradisional. Dalam pengertian lain, adalah berbagai macam kue yang pada awalnya diperjualbelikan di pasar-pasar tradisional.', '2020-03-10 11:49:37', 'Parinah', '2020-03-23 11:05:28', 'Super Administrator', NULL, NULL, 0, 1),
	('c1fde0fbd365b762db31852c0f819de2', 'fd61b4104b60629b89ec3a14a68f7ff5', 'jonta jajanan pasar', 200000, '5', '2.1', NULL, NULL, NULL, NULL, 'makanan tradisional Indonesia yang diperjualbelikan di pasar, khususnya di pasar-pasar tradisional. Dalam pengertian lain, adalah berbagai macam kue yang pada awalnya diperjualbelikan di pasar-pasar tradisional.', '2020-03-10 11:50:23', 'Parinah', '2020-03-23 11:03:44', 'Super Administrator', NULL, NULL, 0, 1),
	('b0693d4eba1cac805b4a4d450209e926', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Kotak Tisu Box Tissue Tempat Tisu kerajinan tangan enceng gondok', 60000, '4', '1.1', NULL, NULL, NULL, NULL, '- Kualitas Super Kotak Box Tisu kerajinan tangan bahan enceng gondok\r\n- Ukuran : 24x14x10 cm \r\n- Warna : coklat natural di beri melamin\r\n- Pengerjaan Rapi, halus & presisi\r\n- Berfungsi untuk menyimpan tisu sehingga nampak rapi saat di letakkan di rumah\r\nataupun buat hiasan\r\n- Di buat dengan bahan dasar Enceng gondok,dengan desain yang mewah dan natural\r\n- Anti jamur \r\n- 90 % pasar produk kami ada di luar negeri , sementara batu memulai untuk\r\nmenjual di dalam negeri dengan harga minim\r\n\r\nFoto yang di tampilkan adalah foto produk asli', '2020-03-10 13:42:24', 'Parinah', '2020-03-24 14:04:32', 'Super Administrator', NULL, NULL, 0, 1),
	('c27495e81dbbdddb58e01334cec07c87', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Batik cople', 200000, '1', '1.1', NULL, NULL, NULL, NULL, 'M: Ld:104cm/Pb:69cm\r\nL: Ld:108cm/Pb:70cm\r\nXL: Ld:112cm/Pb:71cm\r\n\r\nCewek brukat dan roknya\r\nCowok kemeja\r\n\r\nLd= Lingkar dada\r\nPb= Panjang baju\r\n\r\nMenerima pesanan seragam.\r\nHarga grosir lebih murah ya\r\n#gratis ongkir sesuai ketentuan shopee\r\n\r\n\r\nHappy shopping bro/sist.\r\n', '2020-03-13 08:39:44', 'Parinah', '2020-03-31 12:54:43', 'Super Administrator', NULL, NULL, 0, 1),
	('d673cac9094a178f1d293646b1eef04d', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Kolase Biji Biji-Bijian Kerajinan Kolase Properti Foto Dekorasi Rustik', 10000, '4', '1.1', NULL, NULL, NULL, NULL, 'Beragam jenis biji-bijian untuk kolase.\r\n\r\nSemua barang READY ya kakak. Langsung saja diorder.\r\nMenerima Dropship (langsung klik dropshipper). \r\nTERIMA KASIH.\r\n\r\nFOTO real picture. Bahan kerajinan tangan dan prakarya, dapat dikreasikan dengan berbagai bahan alam yang tersedia disini. \r\nPERHATIAN: Warna tidak selalu sama dengan foto, Bahan alam sehingga bentuk dan ukuran tidak identik. Membeli berarti paham.\r\n\r\nFREE PACKING AMAN dengan kardus/kertas/plastik/buble warp untuk pembelian minimal 30.000 rupiah.', '2020-03-10 12:19:11', 'Parinah', '2020-03-23 10:57:02', 'Super Administrator', NULL, NULL, 0, 1),
	('82f26ef424f65f2e4c72ee58aab7411f', 'fd61b4104b60629b89ec3a14a68f7ff5', 'BUNGA KLOBOT JAGUNG - BUNGA JAGUNG - KLOBOT JAGUNG -MAHAR - BAHAN KERAJINAN - RUSTIC - BUNGA KERING', 15000, '4', '1.1', NULL, NULL, NULL, NULL, 'BUNGA KLOBOT JAGUNG \r\n\r\nSmall Diameter 5-6 cm : 7.000\r\nMedium Diameter 7 cm : 8.000\r\nLarge Diameter 8 cm : 9.000\r\n\r\n#bahanrustic #rustic #kelobotjagung #bungakelobotjagung #bungajagung #bahankerajinan #jualbungakelobotjagung #hiasmahar #bahanmahar #kerajinanrustic', '2020-03-10 12:47:18', 'Parinah', '2020-03-24 14:04:43', 'Super Administrator', NULL, NULL, 0, 1),
	('eb97cbe2c58f89247d3284de6201d2ae', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Singkong Kuing', 1000, '7', NULL, 'KLIRONG', 'TANGGULANGIN', '01', '02', 'Singkong kuning, buket, enak, tur murah', '2020-03-23 12:54:00', 'Parinah', '2020-04-01 14:31:09', 'Parinah', NULL, NULL, 0, 1),
	('15964d93520290c00169dab2fe3b596c', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Ikan Tuna', 300000, '9', NULL, 'AYAH', 'CANDIRENGGO', '01', '02', 'Ikan segar dari laut asli', '2020-03-30 09:45:22', 'Parinah', '2020-04-01 14:28:47', 'Parinah', NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `tb_produk` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace.tb_umkm
DROP TABLE IF EXISTS `tb_umkm`;
CREATE TABLE IF NOT EXISTS `tb_umkm` (
  `id` varchar(255) NOT NULL,
  `id_user` varchar(255) NOT NULL,
  `nama_market` varchar(255) DEFAULT NULL,
  `nama_pemilik` varchar(255) DEFAULT NULL,
  `nik` bigint(20) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kelurahan` varchar(255) DEFAULT NULL,
  `alamat_lengkap` text DEFAULT NULL,
  `ordinat_s` varchar(20) DEFAULT NULL,
  `ordinat_e` varchar(20) DEFAULT NULL,
  `latdegree` varchar(20) DEFAULT NULL,
  `latminute` varchar(20) DEFAULT NULL,
  `latsecond` varchar(20) DEFAULT NULL,
  `lngdegree` varchar(20) DEFAULT NULL,
  `lngminute` varchar(20) DEFAULT NULL,
  `lngsecond` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace.tb_umkm: 2 rows
DELETE FROM `tb_umkm`;
/*!40000 ALTER TABLE `tb_umkm` DISABLE KEYS */;
INSERT INTO `tb_umkm` (`id`, `id_user`, `nama_market`, `nama_pemilik`, `nik`, `no_hp`, `email`, `kecamatan`, `kelurahan`, `alamat_lengkap`, `ordinat_s`, `ordinat_e`, `latdegree`, `latminute`, `latsecond`, `lngdegree`, `lngminute`, `lngsecond`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('fd61b4104b60629b89ec3a14a68f7ff5', 'fd61b4104b60629b89ec3a14a68f7ff5', 'Batik Kebumen', 'Parinah', 3310061412650009, '+6285712729478', 'parinah@gmail.com', 'AYAH', 'CANDIRENGGO', 'RT.02 RW.03', '-7.683050', '109.396167', '7', '40', '59.0', '109', '23', '46.2', '2020-02-27 21:14:22', 'umkm', '2020-04-08 13:12:12', 'Super Administrator', NULL, NULL, 0, 0),
	('0e2d71ef77f54f27b20296ff15cdcb89', '1006a03b9d82d34f80991f895e6537de', 'onyip gedang', 'onyip gedang', 3310061412650009, '085712729478', 'slemador@gmail.com', 'AYAH', 'TLOGOSARI', 'RT 02/ RW 03', '-7.700152', '109.416988', '7', '42', '0.5', '109', '25', '1.2', '2020-04-06 09:46:25', NULL, '2020-04-08 13:12:12', 'Super Administrator', NULL, NULL, 0, 0);
/*!40000 ALTER TABLE `tb_umkm` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._access
DROP TABLE IF EXISTS `_access`;
CREATE TABLE IF NOT EXISTS `_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._access: 8 rows
DELETE FROM `_access`;
/*!40000 ALTER TABLE `_access` DISABLE KEYS */;
INSERT INTO `_access` (`id`, `access`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	(1, 'read', 'Mengakses menu.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(2, 'create', 'Menambah data.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(3, 'update', 'Mengubah data.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(4, 'delete', 'Menghapus data.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(5, 'report', 'Membuat laporan. ', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(6, 'login', 'Masuk ke aplikasi.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(7, 'loginfailed', 'Gagal masuk ke aplikasi.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	(8, 'logout', 'Keluar dari aplikasi.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `_access` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._group
DROP TABLE IF EXISTS `_group`;
CREATE TABLE IF NOT EXISTS `_group` (
  `id` varchar(32) NOT NULL,
  `group` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._group: 3 rows
DELETE FROM `_group`;
/*!40000 ALTER TABLE `_group` DISABLE KEYS */;
INSERT INTO `_group` (`id`, `group`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('0adf192fa95d7a62bc416f6316b61204', 'User', 'User biasa.', '2020-01-09 20:26:42', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	('c4ca4238a0b923820dcc509a6f75849b', 'Superadmin', 'Root access.', '2019-07-02 21:59:26', 'System', '2020-01-09 21:06:40', 'Super Administrator', NULL, NULL, 0, 1),
	('297a139cfbf6720acb448ee95d9f7a12', 'Penjual / UMKM', 'Group Untuk Kelompok atau UMKM yang Ikut Memasarkan Produknya Di SIMBOK Marketplace', '2020-02-26 09:06:28', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `_group` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._group_menu
DROP TABLE IF EXISTS `_group_menu`;
CREATE TABLE IF NOT EXISTS `_group_menu` (
  `menu_id` varchar(32) NOT NULL,
  `group_id` varchar(50) NOT NULL DEFAULT '0',
  `_read` tinyint(1) NOT NULL DEFAULT 0,
  `_create` tinyint(1) NOT NULL DEFAULT 0,
  `_update` tinyint(1) NOT NULL DEFAULT 0,
  `_delete` tinyint(1) NOT NULL DEFAULT 0,
  `_report` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`menu_id`,`group_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._group_menu: 29 rows
DELETE FROM `_group_menu`;
/*!40000 ALTER TABLE `_group_menu` DISABLE KEYS */;
INSERT INTO `_group_menu` (`menu_id`, `group_id`, `_read`, `_create`, `_update`, `_delete`, `_report`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('99.04', '0adf192fa95d7a62bc416f6316b61204', 1, 0, 1, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.03', '0adf192fa95d7a62bc416f6316b61204', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.02', '0adf192fa95d7a62bc416f6316b61204', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.01', '0adf192fa95d7a62bc416f6316b61204', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99', '0adf192fa95d7a62bc416f6316b61204', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('31.02', '297a139cfbf6720acb448ee95d9f7a12', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.05', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 1, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.04', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 1, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.03', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.02', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99.01', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('91.01', '0adf192fa95d7a62bc416f6316b61204', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('90', '0adf192fa95d7a62bc416f6316b61204', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('99', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('91.01', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('91', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('90', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('42', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('41', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('31.01', '297a139cfbf6720acb448ee95d9f7a12', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('31', '297a139cfbf6720acb448ee95d9f7a12', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('10', '297a139cfbf6720acb448ee95d9f7a12', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('40', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('23', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('22', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('10', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('21', 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 1, 1, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('20', 'c4ca4238a0b923820dcc509a6f75849b', 1, 0, 0, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1),
	('32', '297a139cfbf6720acb448ee95d9f7a12', 1, 1, 1, 0, 0, NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `_group_menu` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._menu
DROP TABLE IF EXISTS `_menu`;
CREATE TABLE IF NOT EXISTS `_menu` (
  `id` varchar(50) NOT NULL,
  `parent_id` varchar(50) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `menu` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `is_create` tinyint(1) NOT NULL DEFAULT 0,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0,
  `is_report` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._menu: 21 rows
DELETE FROM `_menu`;
/*!40000 ALTER TABLE `_menu` DISABLE KEYS */;
INSERT INTO `_menu` (`id`, `parent_id`, `type`, `menu`, `controller`, `icon`, `url`, `is_read`, `is_create`, `is_update`, `is_delete`, `is_report`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('10', '', 3, 'Dashboard', '_dashboard', 'fas fa-tachometer-alt', 'index', 1, 0, 0, 0, 0, '2019-07-04 11:45:32', 'System', '2020-01-09 20:38:33', 'Super Administrator', NULL, NULL, 0, 1),
	('90', '', 1, 'MENU KONFIGURASI', '#', '#', '#', 1, 0, 0, 0, 0, '2019-07-05 05:35:00', 'System', '2020-01-09 12:36:35', 'Super Administrator', NULL, NULL, 0, 1),
	('99', '', 2, 'Pengaturan', '#', 'fas fa-list', '#', 1, 0, 0, 0, 0, '2019-07-05 05:35:21', 'System', '2020-01-10 08:23:36', 'Super Administrator', NULL, NULL, 0, 1),
	('99.01', '99', 3, 'Menu', '_menu', 'fas fa-bars', 'index', 1, 1, 1, 1, 0, '2019-07-05 05:38:26', 'System', '2020-01-10 08:22:30', 'Super Administrator', NULL, NULL, 0, 1),
	('99.02', '99', 3, 'Group', '_group', 'fas fa-users', 'index', 1, 1, 1, 1, 0, '2019-07-16 07:15:08', 'Super Administrator', '2020-01-10 07:35:14', 'Super Administrator', NULL, NULL, 0, 1),
	('99.03', '99', 3, 'Pengguna', '_user', 'fas fa-user', 'index', 1, 1, 1, 1, 0, '2019-07-17 04:12:26', 'Super Administrator', '2020-01-10 07:35:25', 'Super Administrator', NULL, NULL, 0, 1),
	('99.04', '99', 3, 'Profil', '_profile', 'fas fa-address-card', 'form', 1, 0, 1, 0, 0, '2019-07-21 14:11:08', 'Super Administrator', '2020-01-10 13:00:40', 'Super Administrator', NULL, NULL, 0, 1),
	('91', '', 2, 'Master Data', '#', 'fas fa-microchip', '#', 1, 0, 0, 0, 0, '2020-01-10 11:59:37', 'Super Administrator', '2020-01-10 12:01:19', 'Super Administrator', NULL, NULL, 0, 1),
	('91.01', '91', 3, 'Parameter', '_parameter', 'fas fa-list', 'index', 1, 1, 1, 1, 0, '2020-01-10 12:01:11', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1),
	('99.05', '99', 3, 'Tema', '_theme', 'fas fa-image', 'form', 1, 0, 1, 0, 0, '2020-01-10 13:00:26', 'Super Administrator', '2020-01-10 13:00:33', 'Super Administrator', NULL, NULL, 0, 1),
	('20', '', 1, 'PENGELOLA UMKM', '#', '#', '#', 1, 0, 0, 0, 0, '2020-02-26 10:57:45', 'Super Administrator', '2020-02-26 11:00:26', 'Super Administrator', NULL, NULL, 0, 1),
	('22', '', 3, 'UMKM Terdaftar', 'umkm_terdaftar', 'fas fa-users', 'index', 1, 1, 1, 1, 0, '2020-02-26 11:09:45', 'Super Administrator', '2020-02-27 12:57:05', 'Super Administrator', NULL, NULL, 0, 1),
	('23', '', 3, 'Produk UMKM', 'umkm_produk', 'fas fa-gifts', 'index', 1, 1, 1, 1, 0, '2020-02-26 11:14:14', 'Super Administrator', '2020-03-12 08:12:26', 'Super Administrator', NULL, NULL, 0, 1),
	('31', '', 2, 'Produk', '#', 'fas fa-gifts', '#', 1, 1, 1, 1, 0, '2020-02-26 14:27:01', 'Super Administrator', '2020-03-10 09:07:49', 'Super Administrator', NULL, NULL, 0, 1),
	('32', '', 3, 'Profil Market', 'umkm_profil', 'fas fa-store', 'index', 1, 1, 1, 0, 0, '2020-02-26 14:30:06', 'Super Administrator', '2020-04-07 09:49:16', 'Super Administrator', NULL, NULL, 0, 1),
	('21', '', 3, 'UMKM Pendaftar ', 'umkm_pendaftar', 'fas fa-user-check', 'index', 1, 1, 1, 1, 0, '2020-02-27 12:02:05', 'Super Administrator', '2020-02-27 12:57:19', 'Super Administrator', NULL, NULL, 0, 1),
	('40', '', 1, 'PENGELOLA APLIKASI', '#', '#', '#', 1, 0, 0, 0, 0, '2020-03-04 09:18:20', 'Super Administrator', '2020-03-04 09:19:09', 'Super Administrator', NULL, NULL, 0, 1),
	('41', '', 3, 'Pengaturan Slide Show', 'pa_slide', 'fas fa-forward', 'index', 1, 1, 1, 1, 0, '2020-03-04 09:29:23', 'Super Administrator', '2020-03-04 14:58:35', 'Super Administrator', NULL, NULL, 0, 1),
	('42', '', 3, 'Kategori', 'pa_kategori', 'fas fa-cubes', 'index', 1, 1, 1, 1, 0, '2020-03-04 10:53:22', 'Super Administrator', '2020-03-04 13:19:27', 'Super Administrator', NULL, NULL, 0, 1),
	('31.01', '31', 3, 'Dipublikasi', 'umkm_produk', 'fas fa-clipboard-check', 'index', 1, 1, 1, 1, 0, '2020-03-10 09:07:22', 'Super Administrator', '2020-03-10 09:07:39', 'Super Administrator', NULL, NULL, 0, 1),
	('31.02', '31', 3, 'Diarsipkan', 'umkm_produk_arsip', 'fas fa-archive', 'index', 1, 1, 1, 1, 0, '2020-03-10 09:10:36', 'Super Administrator', '2020-03-10 09:24:00', 'Super Administrator', NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `_menu` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._parameter
DROP TABLE IF EXISTS `_parameter`;
CREATE TABLE IF NOT EXISTS `_parameter` (
  `id` varchar(255) NOT NULL,
  `parameter` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._parameter: 1 rows
DELETE FROM `_parameter`;
/*!40000 ALTER TABLE `_parameter` DISABLE KEYS */;
INSERT INTO `_parameter` (`id`, `parameter`, `value`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`) VALUES
	('0adf192fa95d7a62bc416f6316b61204', 'TAHUN_MULAI', '2020', 'User biasa.', '2020-01-09 20:26:42', 'Super Administrator', '2020-02-27 13:04:34', 'Super Administrator', NULL, NULL, 0, 1);
/*!40000 ALTER TABLE `_parameter` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._profile
DROP TABLE IF EXISTS `_profile`;
CREATE TABLE IF NOT EXISTS `_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `singkatan` varchar(255) NOT NULL,
  `instansi` varchar(255) NOT NULL,
  `pemda` varchar(255) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `logo` text NOT NULL,
  `version` varchar(20) NOT NULL,
  `start_year` int(4) NOT NULL DEFAULT 0,
  `navbar_variant` varchar(255) NOT NULL DEFAULT '0',
  `brand_logo_variant` varchar(255) NOT NULL DEFAULT '0',
  `sidebar_variant` varchar(255) NOT NULL DEFAULT '0',
  `sidebar_accent` varchar(255) NOT NULL DEFAULT '0',
  `navbar_border` tinyint(1) NOT NULL DEFAULT 0,
  `body_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `navbar_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `sidebarnav_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `footer_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `flat_sidebar` tinyint(1) NOT NULL DEFAULT 0,
  `legacy_sidebar` tinyint(1) NOT NULL DEFAULT 0,
  `compact_sidebar` tinyint(1) NOT NULL DEFAULT 0,
  `nav_child_indent` tinyint(1) NOT NULL DEFAULT 0,
  `layout_fixed` tinyint(1) NOT NULL,
  `layout_navbar_fixed` tinyint(1) NOT NULL,
  `layout_footer_fixed` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._profile: 1 rows
DELETE FROM `_profile`;
/*!40000 ALTER TABLE `_profile` DISABLE KEYS */;
INSERT INTO `_profile` (`id`, `app`, `singkatan`, `instansi`, `pemda`, `telp`, `fax`, `email`, `description`, `logo`, `version`, `start_year`, `navbar_variant`, `brand_logo_variant`, `sidebar_variant`, `sidebar_accent`, `navbar_border`, `body_small_text`, `navbar_small_text`, `sidebarnav_small_text`, `footer_small_text`, `flat_sidebar`, `legacy_sidebar`, `compact_sidebar`, `nav_child_indent`, `layout_fixed`, `layout_navbar_fixed`, `layout_footer_fixed`, `created`, `created_by`, `updated`, `updated_by`, `is_active`, `is_deleted`) VALUES
	(1, 'SIMBOK BLONJO', 'SMP', 'ADD Komputer', 'Yogyakarta', '-', '-', 'developer.addkomputer@gmail.com', 'BAKULANE WONG KEBUMEN', 'f07391abe267823e8af5eb69cd26585c.png', '0.1.0', 2020, 'navbar-dark navbar-primary', 'navbar-primary', 'sidebar-dark-', 'info', 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, '2019-07-02 07:41:39', 'System', '2020-03-31 12:40:28', 'Super Administrator', 0, 0);
/*!40000 ALTER TABLE `_profile` ENABLE KEYS */;

-- Dumping structure for table simbok_marketplace._user
DROP TABLE IF EXISTS `_user`;
CREATE TABLE IF NOT EXISTS `_user` (
  `id` varchar(32) NOT NULL,
  `group_id` varchar(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT 'System',
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table simbok_marketplace._user: 3 rows
DELETE FROM `_user`;
/*!40000 ALTER TABLE `_user` DISABLE KEYS */;
INSERT INTO `_user` (`id`, `group_id`, `user_name`, `user_password`, `fullname`, `photo`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `is_deleted`, `is_active`, `is_locked`) VALUES
	('17c4520f6cfd1ab53d8745e84681eb49', 'c4ca4238a0b923820dcc509a6f75849b', 'superadmin', '7363a0d0604902af7b70b271a0b96480', 'Super Administrator', '3feaf401ef181de9bd672bdff3d9cf0c.png', NULL, 'System', '2020-04-08 13:21:30', 'Super Administrator', NULL, NULL, 0, 1, 1),
	('fd61b4104b60629b89ec3a14a68f7ff5', '297a139cfbf6720acb448ee95d9f7a12', 'umkm', '7363a0d0604902af7b70b271a0b96480', 'Parinah', 'b0b24307c540b73b05c0f8e62a79833b.png', '2020-02-27 21:14:22', 'Super Administrator', '2020-04-08 13:12:12', 'Super Administrator', NULL, NULL, 0, 0, 0),
	('1006a03b9d82d34f80991f895e6537de', '297a139cfbf6720acb448ee95d9f7a12', 'ogdang', '7363a0d0604902af7b70b271a0b96480', 'onyip gedang', '', '2020-04-06 09:46:25', 'user', '2020-04-08 13:12:12', 'Super Administrator', NULL, NULL, 0, 0, 0);
/*!40000 ALTER TABLE `_user` ENABLE KEYS */;

-- Dumping structure for trigger simbok_marketplace.delete_group
DROP TRIGGER IF EXISTS `delete_group`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `delete_group` AFTER DELETE ON `_group` FOR EACH ROW BEGIN
 DELETE FROM _group_menu WHERE group_id=old.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger simbok_marketplace.update_menu
DROP TRIGGER IF EXISTS `update_menu`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `update_menu` AFTER UPDATE ON `_menu` FOR EACH ROW BEGIN
    UPDATE _group_menu SET menu_id = NEW.id WHERE menu_id = OLD.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
