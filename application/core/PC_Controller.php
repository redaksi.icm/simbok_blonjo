<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PC_Controller extends MX_Controller{
    function render($content, $data = NULL){
        $config_pagination['full_tag_open'] = '<ul class="list-inline list-unstyled">';
        $config_pagination['full_tag_close'] = '</ul>';
        $config_pagination['attributes'] = ['class' => 'page-link'];
        $config_pagination["first_link"] = "&Lang;";
        $config_pagination["last_link"] = "&Rang;";
        $config_pagination['first_tag_open'] = '<li class="page-item">';
        $config_pagination['first_tag_close'] = '</li>';
        $config_pagination['prev_link'] = '&lang;';
        $config_pagination['prev_tag_open'] = '<li class="prev">';
        $config_pagination['prev_tag_close'] = '</li>';
        $config_pagination['next_link'] = '&rang;';
        $config_pagination['next_tag_open'] = '<li class="next">';
        $config_pagination['next_tag_close'] = '</li>';
        $config_pagination['last_tag_open'] = '<li class="page-item">';
        $config_pagination['last_tag_close'] = '</li>';
        $config_pagination['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
        $config_pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config_pagination['num_tag_open'] = '<li class="page-item">';
        $config_pagination['num_tag_close'] = '</li>';
        $config_pagination['num_links'] = 3;
        $this->pagination->initialize($config_pagination);
    // Load menu
        $this->load->model(array('_config/m_config'));
        $data['sidenav'] = $this->m_config->get_group_menu();
    // Load profile
        $this->load->model(array('_profile/m_profile'=>'profile'));
        $data['profile'] = $this->profile->get_first();
    // Load profile
        $this->load->model(array('p_home/m_statistik'=>'statistik'));
        $data['statistic'] = $this->statistik->get_resume_statistic();;
    // Load profile
        $this->load->model(array('pa_kategori/m_pa_kategori'=>'kategori'));
        $data['kategori'] = $this->kategori->get_kategori();;

    // Templating
        $data['header'] = $this->load->view('p_template/header', $data, TRUE);
        $data['topbar'] = $this->load->view('p_template/topbar', $data, TRUE);
        $data['content'] = $this->load->view($content, $data, TRUE);
        $data['footer'] = $this->load->view('p_template/footer', $data, TRUE);

        $this->load->view('p_template/index', $data);
    }

}

class PC_Error extends MX_Controller{

    function render($content, $data = NULL){
        $data['header'] = $this->load->view('p_template/error/header', $data, TRUE);
        $data['footer'] = $this->load->view('p_template/error/footer', $data, TRUE);
        $data['content'] = $this->load->view($content, $data, TRUE);

        $this->load->view('p_template/error/index', $data);
    }

}
