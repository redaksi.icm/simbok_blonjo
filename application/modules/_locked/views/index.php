<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$profile['singkatan']?> - Lockscreen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- STYLESHEETS -->
    <link rel="shortcut icon" href="<?=base_url()?>images/logos/<?=$profile['logo']?>" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?=base_url()?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- JAVASCRIPTS -->
    <!-- jQuery -->
    <script src="<?=base_url()?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url()?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>dist/js/adminlte.min.js"></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="#"><?=$profile['app']?></a>
      </div>
      <!-- User name -->
      <div class="lockscreen-name"><?=@$this->session->userdata('fullname')?></div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
          <img src="<?=base_url()?>images/users/<?=@$this->session->userdata('photo')?>" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" action="<?=site_url().'/_locked/action'?>" method="post" autocomplete="off">
          <div class="input-group">
            <input type="password" class="form-control" name="user_password" placeholder="Kata Sandi">
            <div class="input-group-append">
              <button type="submit" class="btn"><i class="fas fa-arrow-right text-muted"></i></button>
            </div>
          </div>
        </form>
        <!-- /.lockscreen credentials -->
      </div>
      <!-- /.lockscreen-item -->
      
      <?php if(@$this->session->flashdata('flash_error')):?>
        <div class="help-block text-center text-red">
          <?=$this->session->flashdata('flash_error')?>
        </div>
      <?php endif; ?>
      <div class="help-block text-center">
        Masukkan kata sandi Anda untuk mengembalikan sesi Anda
      </div>
      <div class="text-center">
        <a href="<?=site_url().'/_auth/logout/action'?>">Atau masuk sebagai pengguna lain.</a>
      </div>
      <div class="lockscreen-footer text-center">
        Copyright &copy; <?php if(date('Y') <= $profile['start_year']){echo $profile['start_year'];}else{$profile['start_year'].'-'.date('Y');}?> <b><a href="" class="text-black"><?=$profile['instansi']?></a></b><br>
        All rights reserved
      </div>
    </div>
    <!-- /.center -->

    <!-- jQuery -->
    <script src="<?=base_url()?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url()?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
