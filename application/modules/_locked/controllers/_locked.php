<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _locked extends MX_Controller {

  function __construct(){
    parent::__construct();
		$this->load->model(array(
      '_profile/m_profile',
      '_auth/m_login',
      '_config/m_config'
    ));
    if(@$this->session->userdata('logged') == false) redirect(site_url()."/_auth/login");
  }

  public function index()
  {
    $data = array();
    $data['profile'] = $this->m_profile->get_first();
    $this->load->view('index',$data);
  }

  public function locked()
  {
    $this->session->set_userdata(array('locked' => 1));
    redirect(site_url().'/_locked');
  }

  public function action()
  {
    $data = $this->input->post(null,true);
    if ($data != null) {
      $data['id'] = $this->session->userdata('user_id');
      $data['user_password'] = md5(md5(md5($data['user_password'])));
      $this->m_login->action($data);
      $result = $this->m_login->action($data);
			
			if($result->num_rows() >= 1){
				foreach ($result->result() as $sess) {
					$sess_data['user_id'] = $sess->id;
					$sess_data['group_id'] = $sess->group_id;
					$sess_data['photo'] = $sess->photo;
					$sess_data['fullname'] = $sess->fullname;
					$sess_data['logged'] = true;
					$sess_data['locked'] = false;
				}
				$this->session->set_userdata($sess_data);

				if (@$remember == 1) {
					$cookie = array(
						'name'   => 'uid',
						'value'  => $sess_data['user_id'],
						'expire' => '3600'
					);
					$this->input->set_cookie($cookie);
				}
				$this->input->set_cookie($cookie);

				createLog(6, 'Login');
				$menu = $this->m_config->get_first_menu($sess->group_id);
				redirect(site_url().'/'.$menu->controller);
			}else{
				createLog(7,'Login');
				$this->session->set_flashdata('flash_error', 'Kata Sandi salah!');
				redirect(site_url().'/_locked');
			}
    }else{
      redirect(site_url().'/_error/error_403');
    }
  }
	
}