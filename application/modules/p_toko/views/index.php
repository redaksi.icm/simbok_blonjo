<?php echo $map['js']; ?>
<?php $i = 1; $l=1; $r = 1; $k = 1;?>
<div class="breadcrumb">
  <div class="container">
    <div class="breadcrumb-inner">
      <ul class="list-inline list-unstyled">
      </ul>
    </div>
  </div>
</div>
<div class="body-content outer-top-xs">
  <div class='container'>
    <div class='row single-product'>


      <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class="detail-block">
          <div class="row">
            <div class="col-lg-4 col-md-4">  

              <div class="center-flex" style="padding-top: 30px">
                <div class="image-cropper">
                  <div class="center-flex">
                    <?php if (@$toko['photo']): ?>
                      <img  src="<?=base_url()?>images/users/<?=$toko['photo']?>" alt="" class=" profile-pic">
                      <?php else: ?>
                        <img  src="<?=base_url()?>images/users/no-img.jpg" alt="" class=" profile-pic">
                      <?php endif ?>
                    </div>
                  </div>
                </div>
                <p class="text-center" style="font-size: 20px; padding-top: 15px"><b><?=$toko['nama_market']?></b></p>

              </div>
              <div class="col-lg-4 col-md-4" style="margin-bottom: 15px">
                <div class="row" style="padding-top: 2em;">
                  <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="tel:<?=Tlp($toko['no_hp'])?>"><i title="Telephon" class="fas fa-phone-alt fa-2x text-primary"></i></a></div>
                  <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="sms:<?=Tlp($toko['no_hp'])?>?body=Pesan%20Dari%20SIMBOK%20BLONJO."><i title="SMS" class="fas fa-comments fa-2x text-primary"></i></a></div>
                  <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="https://wa.me/<?=Wa($toko['wa'])?>" target="blank"><i title="Whatsapp" class="fab fa-whatsapp text-primary" style="font-size: 2.5em;"></i></a></div>
                  <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="mailto:<?=$toko['email']?>?subject=SIMBOK%20BLONJO"><i title="Email" class="fas fa-envelope fa-2x text-primary"></i></a></div>
                </div>
                <div class="col-md-12" style="border-top: 1px solid #F2F2F2; margin-top: 1em; margin-bottom: 1em"></div>
                <div class="" style="padding-left: 1.5rem; padding-bottom: 2em;">
                  <span>Bergabung Pada :</span><br>
                  <span><?=date_gabung($toko['created_at'])?></span>
                  <br>
                  <br>
                  <span>Telephon :</span><br>
                  <span><?=Tlp($toko['no_hp'])?></span>
                  <br>
                  <br>
                  <span>Email :</span><br>
                  <span><?=$toko['email']?></span>
                  <br>
                  <br>
                  <span>Alamat :</span><br>
                  <span>KAB. KEBUMEN, KEC. <?=$toko['kecamatan']?>, <?=$toko['kelurahan']?>, <?=$toko['alamat_lengkap']?>.</span>
                </div>

              </div>
              <div class="col-md-4 col-lg-4">  
                <?=$map['html']?>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="clearfix"></div>

      <div class="search-result-container " style="margin-bottom: 20px; margin-top: 20px">
        <div class="tab-content category-list">
          <div class="tab-pane active">
            <div class="category-product">
              <?php if (@$produk): ?>
                <div class="row">
                  <?php foreach ($produk as $v): ?>
                    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                      <div class="item" title="<?=$v['nama_produk']?>">
                        <div class="products">
                          <div class="product main-product">

                            <div class="product-image">
                              <div class="image"> 
                                <a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>">
                                  <img src="<?=base_url()?>images/tamnel/<?=@$v['img']?>" alt=""> 
                                </a> 
                              </div>
                            </div>

                            <div class="product-info text-left">
                              <h3 class="name name-limit"><a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>"><?=$v['nama_produk']?></a></h3>
                              <div class="product-price">
                                <span class="price">Rp<?=@numSys($v['harga'])?>,-</span>
                              </div>
                              <div class="row">
                                <div class="col-md-6 text-left">
                                  <p class='card-bawah' style="margin-left: 0px; font-size: 10px "><?=@$v['kelurahan']?>, <?=@$v['kecamatan']?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                  <p class='card-bawah' style="margin-right: 10px; font-size: 10px "><?php if($v['updated_at'] != NULL){echo @dateCard($v['updated_at']);}else{echo @dateCard($v['created_at']);} ?></p>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach ?>

                </div>
                <?php else: ?>
                  <div class="center-flex">
                    <img src="<?=base_url()?>assets/images/404.png" style="height: 15rem;">
                  </div>
                  <div class="center-flex">
                    <p style="font-size: 20px;color: #aaaaaa;"><b>Produk Tidak Ditemukan!</b></p>
                  </div>
                <?php endif ?>

              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      $('.carousel').carousel({
        interval: false
      });
      jQuery(document).ready(function($) {
                //set here the speed to change the slides in the carousel
                $('#myCarousel').carousel({
                  interval: 5000
                });
                
//Loads the html to each slider. Write in the "div id="slide-content-x" what you want to show in each slide
$('#carousel-text').html($('#slide-content-0').html());

        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click( function(){
          var id = this.id.substr(this.id.lastIndexOf("-") + 1);
          var id = parseInt(id);
          $('#myCarousel').carousel(id);
        });


        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
         var id = $('.item.active').data('slide-number');
         $('#carousel-text').html($('#slide-content-'+id).html());
       });
      });
    </script>
