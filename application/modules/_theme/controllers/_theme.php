<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _theme extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      '_profile/m_profile'
    ));

    $this->controller = '_theme';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
	}

  public function form()
  {
		if ($this->menu['_read'] == 0) redirect(site_url().'/_error/error_403');
		createLog(3,$this->controller);
		$data['main'] = $this->m_profile->get_first();
    $data['menu'] = $this->menu;
    $this->render('form',$data);
  }

  public function save($type, $val)
  {
    if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
    $data[$type] = urldecode($val);
		$this->m_profile->update($data);
		createLog(3, $this->menu['menu']);
		$this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    redirect(site_url().'/'.$this->controller.'/form/');
  }
	
}