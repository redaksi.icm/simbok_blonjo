<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengaturan</li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
            <li class="breadcrumb-item active"><?php if($id == null){echo 'Tambah';}else{echo 'Ubah';}?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-md-12">
          <div class="card">
            <form id="form" action="<?=site_url().'/'.$menu['controller'].'/save/'.$id?>" enctype="multipart/form-data" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?=$this->session->flashdata('flash_error')?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="id" id="id" value="<?=@$main['id']?>" required>
                <?php if($id != null):?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?=@$main['user_name']?>" required>
                <?php endif;?>
                <div class="form-group row">
                  <label for="parent_id" class="col-sm-2 col-form-label text-right">Group</label>
                  <div class="col-sm-2">
                    <select class="form-control form-control-sm select2" name="group_id" id="group_id">
                      <?php foreach($group as $r):?>
                        <option value="<?=$r['id']?>" <?php if(@$main['group_id'] == $r['id']){echo 'selected';}?>><?=$r['group']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Nama Pengguna <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="user_name" id="user_name" value="<?=@$main['user_name']?>" required>
                  </div>
                </div>
                <?php if($id == null):?>
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Kata Sandi <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <input type="password" class="form-control form-control-sm" name="user_password" id="user_password" value="" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Ulang Kata Sandi <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <input type="password" class="form-control form-control-sm" name="user_password_confirm" id="user_password_confirm" value="" required>
                    </div>
                  </div>
                <?php endif;?>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Nama Lengkap <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" name="fullname" id="fullname" value="<?=@$main['fullname']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Foto <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="file" class="" name="photo" id="photo">
                    <?php if($id != null):?>
                      <input type="hidden" class="" name="photo_old" id="photo_old" value="<?=@$main['photo']?>">
                    <?php endif;?>
                    <br>
                    <img src="<?=base_url()?>images/users/<?=@$main['photo']?>" class="img-thumbnail mt-3 mb-3" style="max-height:150px" alt="User Image">
                  </div>
                </div>
                <?php if (@$main['group_id']!= "297a139cfbf6720acb448ee95d9f7a12"): ?>
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Aktif</label>
                    <div class="col-sm-3">
                      <div class="pretty p-icon">
                        <input class="icheckbox" type="checkbox" name="is_active" id="is_active" value="1" <?php if(@$main['is_active'] == 1){echo 'checked';}else{echo 'checked';} ?>>
                        <div class="state">
                          <i class="icon fas fa-check"></i><label></label>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endif ?>

              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?=site_url().'/'.$menu['controller']?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <?php if($id != null):?>
            <div class="card">
              <form id="form_password" action="<?=site_url().'/'.$menu['controller'].'/password_change/'.$id?>" method="post" autocomplete="off">
                <div class="card-header">
                  <h3 class="card-title">Ganti Kata Sandi</h3>
                </div>
                <div class="card-body">
                  <input type="hidden" class="form-control form-control-sm" name="id" id="id" value="<?=@$main['id']?>" required>
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Kata Sandi <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <input type="password" class="form-control form-control-sm" name="user_password" id="user_password" value="" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Ulang Kata Sandi <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <input type="password" class="form-control form-control-sm" name="user_password_confirm" id="user_password_confirm" value="" required>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-10 offset-md-2">
                      <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                      <a class="btn btn-sm btn-default btn-cancel" href="<?=site_url().'/'.$menu['controller']?>"><i class="fas fa-times"></i> Batal</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          <?php endif;?>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    $("#form").validate( {
      rules: {
        user_password : {
        },
        user_password_confirm : {
          equalTo : "#user_password"
        }
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
    $("#form_password").validate( {
      rules: {
        user_password : {
        },
        user_password_confirm : {
          equalTo : "#user_password"
        }
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>