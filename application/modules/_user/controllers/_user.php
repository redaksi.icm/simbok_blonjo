<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _user extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      '_group/m_group',
      '_user/m_user'
    ));

    $this->controller = '_user';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'created_at','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
	}

  public function index()
  {
    //auth
    if ($this->menu['_read'] == 0) redirect(site_url().'/_error/error_403');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_user->all_rows();
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_user->list_data($this->cookie);
    $data['pagination_info'] = paginationInfo(count($data['main']), $this->cookie);
    //set pagination
    setPagination($this->controller, $this->cookie);
    //render
    createLog(1,$this->controller);
    $this->render('index',$data);
  }

  public function form($id = null)
  {
    if ($id == null) {
      //auth
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      createLog(2,$this->controller);
      $data['main'] = null;
    }else{
      //auth
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      createLog(3,$this->controller);
      $data['main'] = $this->m_user->by_field('id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['group'] = $this->m_group->all_data();
    $this->render('form',$data);
  }

  public function save($id = null)
  {
    $data = html_remover($this->input->post(null,true));
    $cek = $this->m_user->by_field('user_name', $data['user_name']);
    if ($id == null) {
    if(!isset($data['is_active'])){$data['is_active'] = 0;}
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Data sudah ada di sistem.');
        redirect(site_url().'/'.$this->controller.'/form/');
      }
      $data['id'] = md5(date('YmdHis'));
      $data['user_password'] = md5(md5(md5($data['user_password'])));
      $data['photo'] = $this->_upload();
      unset($data['user_password_confirm']);
      $this->m_user->store($data);
      createLog(2, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    }else{
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      if ($data['old'] != $data['user_name'] && $cek != null) {
        $this->session->set_flashdata('flash_error', 'Data sudah ada di sistem.');
        redirect(site_url().'/'.$this->controller.'/form/'.$id);
      }
      if (!empty($_FILES["photo"]["name"])) {
        $data['photo'] = $this->_upload();
      } else {
        $data['photo'] = $data["photo_old"];
      }
      unset($data['old'], $data['photo_old']);
      $this->m_user->update($id,$data);
      createLog(3, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    if ($this->menu['_delete'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    $this->m_user->delete($id);
    createLog(4, $this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function status($type = null, $id = null)
  {
    if ($this->menu['_update'] == 0 || $type == null || $id == null) redirect(site_url().'/_error/error_403');
    if ($type == 'enable') {
      $this->m_user->update($id, array('is_active' => 1));
    }else{
      $this->m_user->update($id, array('is_active' => 0));
    }
    createLog(3,$this->this->menu['menu']);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function multiple($type = null)
	{
    if ($this->menu['_update'] == 0 || $this->menu['_delete'] == 0) redirect(site_url().'/_error/error_403');
    $data = $this->input->post(null,true);
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
          case 'delete':
            $this->m_user->delete($key);
            $flash = 'Data berhasil dihapus.';
            $t = 4;
            break;

          case 'enable':
            $this->m_user->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            $t = 3;
            break;

          case 'disable':
            $this->m_user->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            $t = 3;
            break;
        }
      }
    }
    createLog($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function password_change($id = null)
  {
    if ($this->menu['_update'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    $data = html_escape($this->input->post(null,true));
    $data['user_password'] = md5(md5(md5($data['user_password'])));
    unset($data['user_password_confirm']);
    $this->m_user->update($id,$data);
    createLog(3, $this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  private function _upload()
  {
    $config['upload_path']          = './images/users/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['overwrite']			      = true;
    $config['max_size']             = 10240; // 10MB
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);
    if ($this->upload->do_upload('photo')) return $this->upload->data("file_name");
    
    return "no-photo.png";
  }
	
}