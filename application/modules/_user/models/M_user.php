<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

  public function list_data($data)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($data['search']['term'] != '') {
      $where .= "AND a.user_name LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT a.*, b.group FROM _user a 
    LEFT JOIN _group b ON a.group_id = b.id
    $where
    ORDER BY "
    .$data['order']['field']." ".$data['order']['type'].
    " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM _user a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT COUNT(1) as total FROM _user a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM _user a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    if ($this->session->userdata('fullname')) {
      $data['created_by'] = $this->session->userdata('fullname');
    }
    $this->db->insert('_user', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('_user', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('_user');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('_user', $data);
    }
  }
  
}