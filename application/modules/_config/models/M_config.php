<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_config extends CI_Model {

	public function get_profile()
  {
    return $this->db->query("SELECT * FROM _profile")->row();
  }

  public function get_first_menu($group_id)
  {
    $query = $this->db->query(
      "SELECT * FROM _group_menu a
      JOIN _menu b ON a.menu_id = b.id
      WHERE a.group_id = '$group_id' 
        AND b.type = 3 AND b.is_active = 1
      ORDER BY a.menu_id ASC"
    );

    return $query->row();
  }

  public function get_group_menu($parent_id = null)
  {
    $group_id = $this->session->userdata('group_id');
    
    $sql_where = '';
    $sql_where .= ($parent_id != '') ? "b.parent_id = '$parent_id'" : 'b.parent_id = ""';

    $query = $this->db->query(
      "SELECT * 
      FROM
        _group_menu a
      JOIN _menu b ON a.menu_id = b.id
      WHERE
        a.group_id = '$group_id' AND 
        $sql_where AND b.is_active = 1
      ORDER BY a.menu_id"
    );
    if ($query->num_rows() > 0) {
      $result = $query->result_array();
      foreach ($result as $key => $val) {
        $result[$key]['child'] = $this->get_group_menu($result[$key]['menu_id']);
      }
      return $result;
    }else{
      return array();
    }
  }

  public function get_menu($group_id,$controller)
  {
    $query = $this->db->query(
      "SELECT 
        b.id,b.parent_id,b.menu,b.type,b.controller,b.icon,b.url,
        a._read,a._create,a._update,a._delete,a._report
      FROM _group_menu a 
      JOIN _menu b ON a.menu_id = b.id
      WHERE
        a.group_id = '$group_id' AND 
        b.controller = '$controller'"
    );

    return $query->row_array();
  }
  
}
