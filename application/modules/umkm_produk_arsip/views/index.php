
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-5">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->

        <div class="col-sm-5 m_pag">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola UMKM
            </li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
          </ol>
        </div>

        <div class="col-sm-2">
          <div class="d-flex justify-content-center">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
        <div class="col-sm-5 pc_pag">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola UMKM
            </li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
          </ol>
        </div><!-- /.col -->
        
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">

        <?php if ($this->session->userdata('group_id') == '297a139cfbf6720acb448ee95d9f7a12'): ?>
          <div class="col-md-2 col-lg-2 col-6 col-sm-3 col-xs-3">
            <a href="<?=site_url()?>/umkm_produk/form">
              <div class="card" style="height: 96% !important;">
                <div class="card-header" style="border: none !important;">
                  <h5 class="text-center">Tambah Produk</h5>
                </div>
                <div class="card-body text-center d-flex justify-content-center">
                  <div class="d-flex align-items-center">

                    <i class="fas fa-plus fa-7x"></i>

                  </div>
                </div>
              </div>
            </a>
          </div>
        <?php endif ?>

        <?php foreach ($main as $v): ?>
          <div class="col-md-2 col-lg-2 col-6 col-sm-3 col-xs-3">
            <div class="card" title="<?=$v['nama_produk']?>">
              <img class="card-img-top" src="<?=base_url()?>images/tamnel/<?=$v['img']?>" alt="Card image cap" style="height: 10rem">
              <div class="card-body">
                <p class="card-title"><?=character_limiter((string)$v['nama_produk'], 12, $end_char = '&#8230;')?></p>
                <p class="card-text"><b>Rp <?=numSys($v['harga'])?>,-</b></p>
                <div class="dropup">
                  <button type="button" class="btn btn-xs btn-primary btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Aksi
                  </button>
                  <div class="dropdown-menu">
                    <?php if($menu['_update'] == 1):?>
                      <a class="dropdown-item" href="<?=site_url().'/'.$menu['controller'].'/form/'.$v['id']?>">Ubah</a>
                      <a class="dropdown-item" href="<?=site_url().'/'.$menu['controller'].'/status/enable/'.$v['id']?>">Publikasi</a>
                    <?php endif; ?>
                    <?php if($menu['_delete'] == 1):?>
                      <a class="dropdown-item" href="<?=site_url().'/'.$menu['controller'].'/delete/'.$v['id']?>">Hapus</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach ?>

      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
  <!-- /.content-wrapper -->
</div>
