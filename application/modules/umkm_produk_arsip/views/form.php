<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola UMKM</li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
            <li class="breadcrumb-item active"><?php if($id == null){echo 'Tambah';}else{echo 'Ubah';}?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-md-12">
          <div class="card">
            <form id="form" action="<?=site_url().'/'.$menu['controller'].'/save/'.$id?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?=$this->session->flashdata('flash_error')?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="id" id="id" value="<?=@$main['id']?>" required>
                <?php if($id != null):?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?=@$main['nama_produk']?>" required>
                <?php endif;?>
                <div class="form-group row">
                  <label for="controller" class="col-sm-2 col-form-label text-right form-label">Nama Produk <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm" name="nama_produk" id="nama_produk" value="<?=@$main['nama_produk']?>" required>
                  </div>
                </div>
                <div class="form-group row mb-3">
                  <label for="controller" class="col-sm-2 col-form-label text-right form-label">Harga <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 11px">Rp.</span>
                      </div>
                      <input type="text" class="form-control form-control-sm num-int" name="harga" id="harga" value="<?=numId(@$main['harga'])?>" required>
                    </div>
                  </div>
                </div>

                <div id="parent-container" class="form-group row mb-3">
                  <label for="parent_id" class="col-sm-2 col-form-label text-right form-label">Kategori <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <select class="form-control form-control-sm select2" name="id_induk_kategori" id="kategori" required>
                      <option value="">---</option>
                      <?php foreach($kategori as $r):?>
                        <option value="<?=$r['id']?>" <?php if(@$main['id_induk_kategori'] == $r['id']){echo 'selected';}?>><?=$r['kategori']?></option>
                      <?php endforeach;?>
                    </select>

                    <select class="form-control form-control-sm select2" name="id_sub_kategori" id="sub" required>
                      <option value="">---</option>
                      <?php foreach($sub as $v):?>
                        <option value="<?=$v['id']?>" <?php if(@$main['id_sub_kategori'] == $v['id']){echo 'selected';}?> data-chained="<?=$v['parent_id']?>"><?=$v['kategori']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>

                <div id="parent-container" class="form-group row mb-3">
                  <label for="parent_id" class="col-sm-2 col-form-label text-right form-label">Alamat <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <select class="form-control form-control-sm select2" name="kecamatan" id="kecamatan" required>
                      <option value="">-- Pilih kecamatan</option>
                      <?php if (@$main['id']): ?>
                        <?php foreach($kecamatan as $r):?>
                          <option id="<?=$r['kecamatan']?>" value="<?=$r['kecamatan']?>" <?php if(@$main['kecamatan'] == $r['kecamatan']){echo 'selected';}?>><?=$r['kecamatan']?></option>
                        <?php endforeach;?>

                        <?php else: ?>
                          <?php foreach($kecamatan as $r):?>
                            <option id="<?=$r['kecamatan']?>" value="<?=$r['kecamatan']?>" <?php if(@$toko['kecamatan'] == $r['kecamatan']){echo 'selected';}?>><?=$r['kecamatan']?></option>
                          <?php endforeach;?>

                        <?php endif ?>
                      </select>
                      <select class="form-control form-control-sm select2" name="kelurahan" id="kelurahan" required>
                        <option value="">-- Pilih Kelurahan</option>
                        <?php if ($main['id']): ?>

                          <?php foreach($kelurahan as $r):?>
                            <option value="<?=$r['kelurahan']?>" <?php if(@$main['kelurahan'] == $r['kelurahan']){echo 'selected';}?> data-chained="<?=$r['kecamatan']?>"><?=$r['kelurahan']?></option>
                          <?php endforeach;?>
                          <?php else: ?>
                            <?php foreach($kelurahan as $r):?>
                              <option value="<?=$r['kelurahan']?>" <?php if(@$toko['kelurahan'] == $r['kelurahan']){echo 'selected';}?> data-chained="<?=$r['kecamatan']?>"><?=$r['kelurahan']?></option>
                            <?php endforeach;?>
                          <?php endif ?>
                        </select>
                        <textarea class="form-control form-control-sm" name="alamat_lengkap" style="height: 6rem;"><?php if (@$main['alamat_lengkap']) {echo @$main['alamat_lengkap'];}else{echo $toko['alamat_lengkap'];}?></textarea>                  
                      </div>
                      <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                      <div class="row">
                        <div class="form-group col-md-12">
                          <input id="pac-input" class="controls form-control form-control-sm mt-3 map-src" type="text" placeholder="Search Box" style="width: 50%">
                          <div id="map" style="width:100%;height: 200px;"></div>
                        </div>

                        <input type="hidden" name="ordinat_s" id="latitude" class="form-control input-sm" value="<?php if(@$main['ordinat_s']){echo @$main['ordinat_s'];}else{echo @$toko['ordinat_s'];}?>">
                        <input type="hidden" name="ordinat_e" id="longitude" class="form-control input-sm" value="<?php if(@$main['ordinat_e']){echo @$main['ordinat_e'];}else{echo @$toko['ordinat_e'];}?>">
                        <input type="hidden" name="latdegree" id="latdegree" class="form-control input-sm" value="<?php if(@$main['latdegree']){echo @$main['latdegree'];}else{echo @$toko['latdegree'];}?>">
                        <input type="hidden" name="latminute" id="latminute" class="form-control input-sm" value="<?php if(@$main['latminute']){echo @$main['latminute'];}else{echo @$toko['latminute'];}?>">
                        <input type="hidden" name="latsecond" id="latsecond" class="form-control input-sm" value="<?php if(@$main['latsecond']){echo @$main['latsecond'];}else{echo @$toko['latsecond'];}?>">
                        <input type="hidden" name="lngdegree" id="lngdegree" class="form-control input-sm" value="<?php if(@$main['lngdegree']){echo @$main['lngdegree'];}else{echo @$toko['lngdegree'];}?>">
                        <input type="hidden" name="lngminute" id="lngminute" class="form-control input-sm" value="<?php if(@$main['lngminute']){echo @$main['lngminute'];}else{echo @$toko['lngminute'];}?>">
                        <input type="hidden" name="lngsecond" id="lngsecond" class="form-control input-sm" value="<?php if(@$main['lngsecond']){echo @$main['lngsecond'];}else{echo @$toko['lngsecond'];}?>">
                      </div>
                    </div>
                    </div>

                   <div class="form-group row">
                      <label for="controller" class="col-sm-2 col-form-label text-right form-label">Deskripsi Produk <span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                        <textarea class="form-control form-control-sm" id="deskripsi" name="deskripsi" style="height: 6rem;"><?=@$main['deskripsi']?></textarea>                  
                      </div>
                    </div>

                    <div class="form-group row mb-3 mt-3">
                      <label for="url" class="col-sm-2 col-form-label text-right form-label">Thumbnail/Preview Foto <span class="text-danger">*</span></label>
                      <div class="col-md-2 col-lg-2 col-6 col-sm-3 col-xs-3">
                        <div class="dropzone tamnel">
                          <div class="dz-message">
                            <i class="fas fa-upload fa-4x"></i>
                            <br>
                            <span>Unggah File</span>
                          </div>
                        </div>
                      </div>
                      <?php if (@$main['id']): ?>
                        <span class="text-danger">Hapus tamnel terlebih dahulu untuk mengubah tamnel!</span>
                      <?php endif ?>
                    </div>

                    <div class="form-group row">
                      <label for="url" class="col-sm-2 col-form-label text-right form-label">Gambar <span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                        <div class="dropzone gambar">
                          <div class="dz-message">
                            <i class="fas fa-upload fa-4x"></i>
                            <br>
                            <span>Unggah File</span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="url" class="col-sm-2 col-form-label text-right"></label>
                      <div class="col-sm-3 mt-2">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="is_active1" name="is_active" <?php if(@$main['is_active'] == 1 || !@$main['is_active'] ){echo 'checked';}?> value="1">
                          <label for="is_active1" class="custom-control-label">Simpan Dan Tampilkan</label>
                        </div>

                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="is_active2" name="is_active" <?php if(@$main['is_active'] == 0 && @$main['id'] != NULL){echo 'checked';}?> value="0">
                          <label for="is_active2" class="custom-control-label">Simpan Dan Arsipkan</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="row">
                      <div class="col-md-10 offset-md-2">
                        <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                        <a class="btn btn-sm btn-default btn-cancel" href="<?=site_url().'/'.$menu['controller']?>"><i class="fas fa-times"></i> Batal</a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </div><!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php $this->load->view('js')?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDb3uAj8g901SdVT6zGq-M-GIa-XhLjotM&libraries=places&callback=initAutocomplete&callback=initMap"></script>
