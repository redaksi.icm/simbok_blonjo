<div class="body-content outer-top-vs" id="top-banner-and-menu">
  <div class="container">
    <div class="row">

      <div class="col-xs-12 col-sm-12 col-md-3 sidebar kategori-home"> 
        <div class="side-menu animate-dropdown outer-bottom-xs">
          <div class="head"><i class="icon fa fa-align-justify fa-fw"></i>Kategori</div>
          <nav class="yamm megamenu-horizontal">
            <ul class="nav">



              <?php foreach ($kategori as $key => $val): ?>
                <?php if (@$val['sub'] != NULL): ?>
                  <li class="dropdown menu-item">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon <?=@$val['icon']?>"></i><?=@$val['kategori']?></a>

                    <ul class="dropdown-menu mega-menu" style="min-width: 50rem">
                      <li>
                        <div class="row">
                          <?php foreach (array_chunk($kategori[$key]['sub'], 5) as $v): ?>
                            <div class="col-xs-12 col-sm-12 col-lg-4">
                              <ul>
                                <?php foreach ($v as $k): ?>
                                  <li><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>&sub=<?=$k['id']?>"><?=@$k['kategori']?></a></li>
                                <?php endforeach ?>
                              </ul>
                            </div>
                          <?php endforeach ?>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <?php else: ?>
                    <li class="dropdown menu-item">
                      <a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>" class="dropdown-toggle">
                        <i class="icon <?=@$val['icon']?>"></i><?=@$val['kategori']?>
                      </a>   
                    </li>
                  <?php endif ?>
                  <?php if ($key == 7): ?>
                    <?php break;?>
                  <?php endif ?>
                <?php endforeach ?>

                <li class="dropdown menu-item">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon fas fa-angle-double-right"></i>Lainnya
                  </a>
                  <ul class="dropdown-menu mega-menu" style="min-width: 100rem">
                    <li>
                      <div class="row">
                        <?php foreach ($kategori as $key => $val): ?>
                          <?php if (@$val['sub'] != NULL && $key >= 8): ?>
                            <div class="col-xs-12 col-sm-12 col-lg-2">
                              <ul>
                                <li><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>"><b><?=@$val['kategori']?></b></a></li>
                                <?php foreach ($kategori[$key]['sub'] as $v): ?>
                                  <li><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>&sub=<?=$v['id']?>"><?=@$v['kategori']?></a></li>
                                <?php endforeach ?>
                              </ul>
                            </div>
                          <?php endif ?>
                        <?php endforeach ?>
                        <div class="col-xs-12 col-sm-12 col-lg-2">
                          <ul>
                            <?php foreach ($kategori as $key => $val): ?>
                              <?php if (@$val['sub'] == NULL && $key >= 8): ?>
                                <li><b><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>"><?=@$val['kategori']?></a></b></li>
                              <?php endif ?>
                            <?php endforeach ?>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>   
                </li>

              </ul>
            </nav>
          </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
          <div id="hero">
            <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
              <?php foreach ($slide as $v): ?>
                <div class="item" style="background-image: url(<?=base_url()?>images/slide/<?=@$v['img']?>);">
                  <div class="container-fluid">
                    <div class="caption bg-color vertical-center text-left">
                      <div class="big-text fadeInDown-1" style="color: white;"><?=@$v['judul']?></div>
                      <div class="excerpt fadeInDown-2 hidden-xs" style="color: white;"><span><?=@$v['deskripsi']?></span></div>
                    </div>
                  </div>
                </div>
              <?php endforeach ?>
            </div>
          </div>
        </div>

      </div>

      <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 

          <div id="product-tabs-slider" class="scroll-tabs outer-top-vs">
            <div class="more-info-tab clearfix ">
              <h3 class="new-product-title pull-left">Produk Terbaru</h3>
            </div>

            <div class="tab-content outer-top-xs">

              <div class="tab-pane in active" id="all">
                <div class="product-slider">
                  <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                    <?php foreach ($produk_new as $v): ?>
                      <div class="item item-carousel" title="<?=$v['nama_produk']?>">
                        <div class="products">
                          <div class="product main-product">

                            <div class="product-image">
                              <div class="image"> 
                                <a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>">
                                  <img src="<?=base_url()?>images/tamnel/<?=@$v['img']?>" alt=""> 
                                </a>
                              </div>
                            </div>

                            <div class="product-info text-left">
                              <h3 class="name name-limit"><a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>"><?=$v['nama_produk']?></a></h3>
                              <div class="product-price">
                                <span class="price">Rp<?=@numSys($v['harga'])?>,-</span>
                              </div>
                              <div class="row">
                                <div class="col-md-6 text-left">
                                  <p class='card-bawah' style="margin-left: 0px; font-size: 10px "><?=@$v['kelurahan']?>, <?=@$v['kecamatan']?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                  <p class='card-bawah' style="margin-right: 10px; font-size: 10px "><?php if($v['updated_at'] != NULL){echo @dateCard($v['updated_at']);}else{echo @dateCard($v['created_at']);} ?></p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach ?>
                  </div>
                </div>
              </div>

            </div>
          </div>

        <!-- <div class="wide-banners outer-bottom-xs">
          <div class="row">

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="<?=base_url()?>assets/images/banners/home-banner1.jpg" alt=""> </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="<?=base_url()?>assets/images/banners/home-banner2.jpg" alt=""> </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="<?=base_url()?>assets/images/banners/home-banner1.jpg" alt=""> </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="<?=base_url()?>assets/images/banners/home-banner2.jpg" alt=""> </div>
              </div>
            </div>

          </div>
        </div>
      -->

      <!-- <div class="clearfix filters-container m-t-10"> -->

        <div class="more-info-tab clearfix" style="background-color: white; border-bottom: 1px solid #eaeaea;">
          <h3 class="new-product-title" style="padding-left: 0.9em">Produk Populer</h3>
        </div>
        <div class="search-result-container " style="margin-bottom: 20px;">
          <div class="tab-content category-list">
            <div class="tab-pane active">
              <div class="category-product">
                <div class="row" id="wadah" da>
                  <?php foreach ($produk_update as $v): ?>
                    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                      <div class="item" title="<?=$v['nama_produk']?>">
                        <div class="products">
                          <div class="product main-product">

                            <div class="product-image">
                              <div class="image"> 
                                <a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>">
                                  <img src="<?=base_url()?>images/tamnel/<?=@$v['img']?>" alt=""> 
                                </a> 
                              </div>
                            </div>

                            <div class="product-info text-left">
                              <h3 class="name name-limit"><a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>"><?=$v['nama_produk']?></a></h3>
                              <div class="product-price">
                                <span class="price">Rp<?=@numSys($v['harga'])?>,-</span>
                              </div>
                              <div class="row">
                                <div class="col-md-6 text-left">
                                  <p class='card-bawah' style="margin-left: 0px; font-size: 10px "><?=@$v['kelurahan']?>, <?=@$v['kecamatan']?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                  <p class='card-bawah' style="margin-right: 10px; font-size: 10px "><?php if($v['updated_at'] != NULL){echo @dateCard($v['updated_at']);}else{echo @dateCard($v['created_at']);} ?></p>
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach ?>
                </div>
                <?php if (count($produk_update) >= 12): ?>
                  <div class="center-flex btn-plus">
                    <button id="tambah" data-id="12" class="btnn infoo" onc>Lebih Banyak</button>
                  </div>
                <?php endif ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- </div> -->

<script>
  $('#tambah').on('click', function(){
    page = $('#tambah').data('id');
    conten = "";
    $.ajax({
      type: 'POST',
      url: "<?=site_url().'/p_home/plus/'?>"+page+"",
      dataType: 'json',
      success: function(result){
        if (result.status == true) {
          $.each(result.data, function(i, data){
            conten += "<div class='col-xs-6 col-sm-6 col-md-2 col-lg-2'>"+
            "<div class='item' title='"+data.nama_produk+"'>"+
            "<div class='products'>"+
            "<div class='product main-product'>"+

            "<div class='product-image'>"+
            "<div class='image'>"+
            "<a href='<?=site_url()?>/p_detail/index/"+data.id+"'>"+
            "<img src='<?=base_url()?>images/tamnel/"+data.img+"' alt=''>"+ 
            "</a>"+ 
            "</div>"+
            "</div>"+

            "<div class='product-info text-left'>"+
            "<h3 class='name name-limit'><a href='<?=site_url()?>/p_detail/index/"+data.id+"'>"+data.nama_produk+"</a></h3>"+
            "<div class='product-price'>"+
            "<span class='price num-int'>Rp "+addCommas(data.harga)+",-</span>"+
            "</div>"+
            "<div class='row'>"+
            "<div class='col-md-6 text-left'>"+
            "<p class='card-bawah' style='margin-left: 0px; font-size: 10px'>"+data.kelurahan+", "+data.kecamatan+"</p>"+
            "</div>"+
            "<div class='col-md-6 text-right'>"+
            "<p class='card-bawah' style='margin-right: 10px; font-size: 10px '>"+ToDateId(data.updated_at)+"</p>"+
            "</div>"+
            "</div>"+
            "</div>"+
            "</div>"+
            "</div>"+
            "</div>"+
            "</div>";

          })
          $('#wadah').append(conten);
          $('#tambah').data('id', page + 12);
          if (result.total < 12) {
            $('#tambah').hide();
          }
        }
      }
    });
  });
</script>

