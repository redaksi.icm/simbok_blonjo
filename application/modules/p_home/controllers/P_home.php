<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_home extends PC_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    $this->load->helper('text');
    $this->load->model(array(
      'pa_kategori/m_pa_kategori',
      'p_home/m_p_home',
      'p_home/m_statistik',
      'pa_slide/m_pa_slide',
      '_group/m_group',
      '_user/m_user'
    ));

    $this->controller = 'p_home';
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['cur_page'] == null) 0;
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 12;
    //
    // insert arsip
    $this->m_p_home->insert_arsip();
  }

  public function index()
  {
    //cookie
    $this->m_statistik->insert_count();
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_p_home->all_rows();
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['produk_update'] = $this->m_p_home->list_data($this->cookie, 'updated_at');
    $data['produk_new'] = $this->m_p_home->list_data(NULL, 'created_at');
    $data['slide'] = $this->m_pa_slide->all_data();
    $data['pagination_info'] = paginationInfo(count($data['produk_update']), $this->cookie);
    //set pagination
    setPagination($this->controller, $this->cookie);
    //render
    $this->render('index', $data);
  }

  public function plus()
  {
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_p_home->all_rows();
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_p_home->list_data($this->cookie, 'updated_at');
    $data['pagination_info'] = paginationInfo(count($data['main']), $this->cookie);
    //set pagination
    setPagination($this->controller, $this->cookie);
    if (count($data['main']) != 0) {
      $result = [
        'status' => TRUE,
        'data' => $data['main'],
        'total' => count($data['main'])
      ];
      echo json_encode($result);
    }
  }

}