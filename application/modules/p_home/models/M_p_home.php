<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_p_home extends CI_Model {

  public function list_data($data, $filed)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND a.is_close = 1 AND b.ins_number = 0 ";
    if (isset($data['search']['term']) != '') {
      $where .= "AND a.nama_produk LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }
    if ($filed == 'updated_at') {
      $where .= "AND a.updated_at IS NOT NULL ";
    }
    if ($data == NULL) {
      $sql = "SELECT a.*, b.img FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id
      $where
      ORDER BY "
      .$filed." "."DESC".
      " LIMIT 20";
    } else {
      $sql = "SELECT a.*, b.img FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id
      $where
      ORDER BY "
      .$filed." "."DESC".
      " LIMIT ".$data['cur_page'].",".$data['per_page'];
    }
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1";

    $sql = "SELECT COUNT(1) as total FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('tb_produk', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('tb_produk', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('tb_produk');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('tb_produk', $data);
    }
  }

  public function insert_arsip()
  {
    $sql = "UPDATE tb_produk a 
            INNER JOIN 
            (
              SELECT a.*,(a.count_day - a.exp_product_day) as last_day 
              FROM 
              (
                  SELECT a.*,
                  DATEDIFF(DATE(NOW()), a.publish_date) as count_day
                  FROM 
                  (
                      SELECT a.id,a.created_at,a.updated_at,a.is_active,IF(a.updated_at IS NULL, a.created_at, a.updated_at) as publish_date,b.exp_product_day
                      FROM tb_produk a 
                      LEFT JOIN _profile b ON 1=1
                      WHERE a.is_active='1'
                  ) as a
              ) as a
              WHERE (a.count_day - a.exp_product_day) > 0
            ) b ON a.id=b.id
            SET a.is_active = '0'";
    $this->db->query($sql);
  }
}