<div class="body-content outer-top-xs" style="margin-bottom: 20px;">
  <div class='container'>
    <div class='row'>
      <div class='col-xs-12 col-sm-12 col-md-3 sidebar'> 

        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 

            <form action="<?=site_url()?>/p_pencarian">
              <div class="sidebar-widget">
                <div class="widget-header">
                  <h4 class="widget-title">Rentang Harga</h4>
                </div>
                <div class="sidebar-widget-body">
                  <div class="form-group">
                    <label>Minimal</label>
                    <input type="number" class="form-control" name="min" placeholder="Minimal" value="<?php if (@$pencarian['min']){ echo $pencarian['min'];}?>">
                  </div>
                  <div class="form-group">
                    <label>Maksimal</label>
                    <input type="number" class="form-control" name="max" placeholder="Maksimal" value="<?php if (@$pencarian['max']){ echo $pencarian['max'];}?>">
                  </div>
                </div>
              </div>

              <div class="sidebar-widget">
                <div class="widget-header">
                  <h4 class="widget-title">Wilayah</h4>
                </div><br>
                <div class="sidebar-widget-body">
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class="form-control  select2" name="kecamatan" id="kec">
                      <option value="">-- Pilih kecamatan</option>
                      <?php foreach($kec as $r):?>
                        <option id="<?=$r['kecamatan']?>" value="<?=$r['kecamatan']?>" <?php if(@$pencarian['kecamatan'] == $r['kecamatan']){echo 'selected';}?>><?=$r['kecamatan']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>


                  <div class="form-group">
                    <label>Kelurahan</label>
                    <select class="form-control select2" name="kelurahan" id="kel">
                      <option value="">-- Pilih Kelurahan</option>
                      <?php foreach($kel as $r):?>
                        <option value="<?=$r['kelurahan']?>" <?php if(@$pencarian['kelurahan'] == $r['kelurahan']){echo 'selected';}?> data-chained="<?=$r['kecamatan']?>"><?=$r['kelurahan']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
              </div>

              <!-- hidden input -->
              
              <input type="hidden" name="kategori" value="<?=@$pencarian['kategori']?>">
              <input type="hidden" name="search" value="<?=@$pencarian['search']?>">
              <!-- end -->
              <div class="sidebar-widget">
                <div class="row">
                  <div class="col-md-6">
                    <div class="center-flex">
                      <button class="btn btn-info">Terapkan</button>
                    </div> 
                  </div>
                  <div class="col-md-6">
                    <div class="center-flex">
                      <a href="?kategori=<?=@$pencarian['kategori']?>&search=<?=@$pencarian['search']?>">
                        <button type="button" class="btn btn-danger">Hilangkan</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-9 rht-col"> 

        <div class="search-result-container ">
          <div id="myTabContent" class="tab-content category-list">
            <div class="tab-pane active " id="grid-container">
              <div class="category-product">

                <?php if (count($main) != 0): ?>

                  <div id="wadah" class="row">
                    <?php foreach ($main as $k => $v): ?>
                      <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <div class="item" title="<?=$v['nama_produk']?>">
                          <div class="products">
                            <div class="product main-product">

                              <div class="product-image">
                                <div class="image"> 
                                  <a href="<?=site_url()?>/p_detail/index/<?=$v['id']?>">
                                    <img src="<?=base_url()?>images/tamnel/<?=$v['img']?>" alt=""> 
                                  </a> 
                                </div>
                              </div>

                              <div class="product-info text-left">
                                <h3 class="name name-limit"><a href="<?=site_url()?>/p_detail/index/<?=$v['id']?>"><?=$v['nama_produk']?></a></h3>
                                <div class="product-price">
                                  <span class="price">Rp <?=numSys($v['harga'])?>,-</span>
                                </div>
                                <div class="row">
                                  <div class="col-md-6 text-left">
                                    <p class='card-bawah' style="margin-left: 0px; font-size: 10px "><?=@$v['kelurahan']?>, <?=@$v['kecamatan']?></p>
                                  </div>
                                  <div class="col-md-6 text-right">
                                    <p class='card-bawah' style="margin-right: 10px; font-size: 10px "><?php if($v['updated_at'] != NULL){echo @dateCard($v['updated_at']);}else{echo @dateCard($v['created_at']);} ?></p>
                                  </div>
                              </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach ?>
                  </div>
                  <?php if (count($main) >= 12): ?>
                    <div class="center-flex btn-plus">
                      <button id="tambah" data-id="12" class="btnn infoo" onc>Lebih Banyak</button>
                    </div>
                  <?php endif ?>

                  <?php else: ?>
                    <div class="center-flex">
                      <img src="<?=base_url()?>assets/images/404.png" style="height: 15rem;">
                    </div>
                    <div class="center-flex">
                      <p style="font-size: 20px;color: #aaaaaa;"><b>Produk Tidak Ditemukan!</b></p>
                    </div>
                  <?php endif ?>


                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $("#kel").chained("#kec");
    $(".num-int").autoNumeric('init',{
      aSep: '.',
      aDec: ',',
      aForm: true,
      vMax: '999999999999999999',
      vMin: '-999999999999999999'
    });
  </script>
  <script>
    $('#tambah').on('click', function(){
      page = $('#tambah').data('id');
      conten = "";
      $.ajax({
        type: 'POST',
        url: "<?=site_url().'/p_pencarian/plus/'?>"+page+"",
        data: <?php echo json_encode($pencarian); ?>,
        dataType: 'json',
        success: function(result){
          if (result.status == true) {
            $.each(result.data, function(i, data){
              conten += "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>"+
              "<div class='item' title='"+data.nama_produk+"'>"+
              "<div class='products'>"+
              "<div class='product main-product'>"+

              "<div class='product-image'>"+
              "<div class='image'>"+
              "<a href='<?=site_url()?>/p_detail/index/"+data.id+"'>"+
              "<img src='<?=base_url()?>images/tamnel/"+data.img+"' alt=''>"+ 
              "</a>"+ 
              "</div>"+
              "</div>"+

              "<div class='product-info text-left'>"+
              "<h3 class='name name-limit'><a href='<?=site_url()?>/p_detail/index/"+data.id+"'>"+data.nama_produk+"</a></h3>"+
              "<div class='product-price'>"+
              "<span class='price num-int'>Rp "+addCommas(data.harga)+",-</span>"+
              "</div>"+
              "<div class='row'>"+
              "<div class='col-md-6 text-left'>"+
              "<p class='card-bawah' style='margin-left: 0px; font-size: 10px'>"+data.kelurahan+", "+data.kecamatan+"</p>"+
              "</div>"+
              "<div class='col-md-6 text-right'>"+
              "<p class='card-bawah' style='margin-right: 10px; font-size: 10px '>"+ToDateId(data.updated_at)+"</p>"+
              "</div>"+
              "</div>"+
              "</div>"+
              "</div>"+
              "</div>"+
              "</div>"+
              "</div>";

            })
            $('#wadah').append(conten);
            $('#tambah').data('id', page + 12);
            if (result.total < 12) {
              $('#tambah').hide();
            }
          }
        }
      });
    });
  </script>