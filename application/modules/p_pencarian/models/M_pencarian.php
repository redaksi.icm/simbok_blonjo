<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pencarian extends CI_Model {

  public function list_data($data, $s)
  {
    $umkm = $this->session->userdata('user_id');
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND a.is_close = 1 AND b.ins_number = 0 ";
    if (@$s['kategori'] && @$s['kategori'] != '') {
      $where .= "AND a.id_induk_kategori LIKE '%".$this->db->escape_like_str($s['kategori'])."%' ";
    }
    if (@$s['sub'] && @$s['sub'] != '') {
      $where .= "AND a.id_sub_kategori LIKE '%".$this->db->escape_like_str($s['sub'])."%' ";
    }
    if (@$s['kecamatan'] && @$s['kecamatan'] != '') {
      $where .= "AND a.kecamatan LIKE '%".$this->db->escape_like_str($s['kecamatan'])."%' ";
    }
    if (@$s['kelurahan'] && @$s['kelurahan'] != '') {
      $where .= "AND a.kelurahan LIKE '%".$this->db->escape_like_str($s['kelurahan'])."%' ";
    }
    if (@$s['search'] && @$s['search'] != '') {
      $where .= "AND a.nama_produk LIKE '%".$this->db->escape_like_str($s['search'])."%' ";
    }

    if (@$s['min'] && @$s['min'] != '' && @$s['max'] == '') {
      $where .= "AND a.harga >= ".$this->db->escape($s['min'])." ";
    }

    if (@$s['max'] && @$s['max'] != '') {
      if (@$s['min'] && @$s['min'] != '') {
        $where .= "AND a.harga BETWEEN ".$this->db->escape($s['min'])." AND ".$this->db->escape($s['max'])." ";
      }else{
        $where .= "AND a.harga <= ".$this->db->escape($s['max'])." ";
      }
    }


    $sql = "SELECT a.*, b.img FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id
    $where
    GROUP BY b.produk_id
    ORDER BY "
    .$data['order']['field']." ".$data['order']['type'].
    " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM _user a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($s)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND a.is_close = 1 AND b.ins_number = 0 ";
    if (@$s['kategori'] && @$s['kategori'] != '') {
      $where .= "AND a.id_induk_kategori LIKE '%".$this->db->escape_like_str($s['kategori'])."%' ";
    }
    if (@$s['sub'] && @$s['sub'] != '') {
      $where .= "AND a.id_sub_kategori LIKE '%".$this->db->escape_like_str($s['sub'])."%' ";
    }
    if (@$s['kecamatan'] && @$s['kecamatan'] != '') {
      $where .= "AND a.kecamatan LIKE '%".$this->db->escape_like_str($s['kecamatan'])."%' ";
    }
    if (@$s['kelurahan'] && @$s['kelurahan'] != '') {
      $where .= "AND a.kelurahan LIKE '%".$this->db->escape_like_str($s['kelurahan'])."%' ";
    }
    if (@$s['search'] && @$s['search'] != '') {
      $where .= "AND a.nama_produk LIKE '%".$this->db->escape_like_str($s['search'])."%' ";
    }

    if (@$s['min'] && @$s['min'] != '' && @$s['max'] == '') {
      $where .= "AND a.harga >= ".$this->db->escape($s['min'])." ";
    }

    if (@$s['max'] && @$s['max'] != '') {
      if (@$s['min'] && @$s['min'] != '') {
        $where .= "AND a.harga BETWEEN ".$this->db->escape($s['min'])." AND ".$this->db->escape($s['max'])." ";
      }else{
        $where .= "AND a.harga <= ".$this->db->escape($s['max'])." ";
      }
    }

    $sql = "SELECT COUNT(1) as total FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM _user a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('_user', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('_user', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('_user');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('_user', $data);
    }
  }
  
}