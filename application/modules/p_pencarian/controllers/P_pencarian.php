<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_pencarian extends PC_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      'pa_kategori/m_pa_kategori',
      'p_pencarian/m_pencarian',
      'p_signup/mp_signup',
      'umkm_produk/m_umkm_produk'
    ));
    $this->load->helper('text');

    $this->controller = 'p_pencarian';
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'created_at','type' => 'DESC');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 12;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    $data['pencarian'] = html_remover($this->input->get(NULL, TRUE));
    // var_dump($data['pencarian']); die();
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_pencarian->all_rows($data['pencarian']);
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_pencarian->list_data($this->cookie, $data['pencarian']);
    $data['pagination_info'] = paginationInfo(count($data['main']), $this->cookie);
    $data['kec'] = $this->mp_signup->get_kecamatan();
    $data['kel'] = $this->mp_signup->get_kelurahan();
    //set pagination
    setPagination($this->controller, $this->cookie);
    //render
    createLog(1,$this->controller);
    
    $this->render('pencarian',$data);

  }

  public function order($controller,$field = null,$type = null)
  {
    $this->auth($controller);
    if ($field == null || $type == null) redirect(site_url().'/_error/error_403');
    $cookie = getCookieMenu($controller);
    $cookie['order'] = array(
      'field' => $field,
      'type' => $type
    );
    setCookieMenu($controller,$cookie);
    redirect(site_url().'/'.$controller);
  }

  public function plus()
  {
    $data['pencarian'] = $this->input->post(NULL, TRUE);
    // echo json_encode($result); die();
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_pencarian->all_rows($data['pencarian']);
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_pencarian->list_data($this->cookie, $data['pencarian']);
    $data['pagination_info'] = paginationInfo(count($data['main']), $this->cookie);
    setPagination($this->controller, $this->cookie);
    //render
    createLog(1,$this->controller);
    // var_dump($data['main']); die();
    if (count($data['main']) != 0) {
      $result = [
        'status' => TRUE,
        'data' => $data['main'],
        'total' => count($data['main'])
      ];
      echo json_encode($result);
    }

  }
} 