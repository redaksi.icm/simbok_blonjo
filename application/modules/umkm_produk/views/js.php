<script>
  var tumnell = false;

  Dropzone.autoDiscover = false;

  
  var foto_tamnel= new Dropzone(".tamnel",{
    url: "<?=site_url().'/'.$menu['controller']?>/upload_tamnel",
    maxFiles: 1,
    maxFilesize: 15,
    method:"post",
    acceptedFiles:"image/*",
    paramName:"img_tamnel",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    init: function() {
      this.on("thumbnail", function(file) {
        if (file.compressed) {
          return;
        }
        const imageCompressor = new ImageCompressor();
        imageCompressor.compress(file, {
          checkOrientation: true,
          quality: 0.6,
          convertSize: 0,
        })
        .then((result) => {
          result.compressed = true;
          this.removeFile(file);
          this.addFile(result);
        })
      });
    },
    autoProcessQueue: false
  });


  var foto_upload= new Dropzone(".gambar",{
    url: "<?=site_url().'/'.$menu['controller']?>/upload",
    maxFilesize: 10,
    method:"post",
    acceptedFiles:"image/*",
    parallelUploads : 20,
    autoQueue : true,
    paramName:"img",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    init: function() {
      this.on("thumbnail", function(file) {
        if (file.compressed) {
          return;
        }
        const imageCompressor = new ImageCompressor();
        imageCompressor.compress(file, {
          checkOrientation: true,
          quality: 0.6,
          convertSize: 0,
        })
        .then((result) => {
          result.compressed = true;
          this.removeFile(file);
          this.addFile(result);
        })
      });
    },
    autoProcessQueue: false
  });


  foto_upload.on("removedfile", function (file) {
    delet_img(file);
  });
  foto_tamnel.on("removedfile", function (file) {
    delet_img(file);
  });

  function delet_img(file) {
    if (file.path == "tamnel") {
      tumnell = false;
    }
    $.ajax({
      type: 'POST',
      url: '<?=site_url().'/'.$menu['controller']?>/del_imgs/',
      data: {id: file.id, name: file.name, path: file.path},
      dataType: 'json',
      success: function(hasil){
        if (hasil.status = true) {
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 5000,
            timerProgressBar: true,
            onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })

          Toast.fire({
            icon: 'success',
            title: 'Gambar berhasil dihapus!'
          });
        }
      }
    });
  }

  $(document).ready(function () {
    $('#deskripsi').summernote({
      height: 150,
      toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['para', ['ul', 'ol']],
      ['table', ['table']]
      ]
    });

    $(".btn-submit").click(function (e) {
      if (tumnell == false && foto_tamnel.files == '') {
        e.preventDefault();
        Swal.fire({
          title: 'Harap Masukan Tamnel',
          type: 'warning',
          customClass: 'swal-wide'
        });
      }
    });



    // function masukan() {
    //   foto_upload.processQueue();
    //   foto_upload.on("complete", function (files) {
    //     if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
    //       form.submit();
    //     }
    //   });
    // }
    

    function showimg() {
      var id = '<?=@$main['id']?>'
      $.ajax({
        type: 'POST',
        url: '<?=site_url().'/'.$menu['controller']?>/get_images/',
        data: {id : id},
        dataType: 'json',
        success : function (imgs) {
          $.each(imgs, function(key,value) {
            if (value.ins_number == 0) {
              var mockFile = { name: value.img, id: value.id, path: 'tamnel'};
              foto_tamnel.options.addedfile.call(foto_tamnel, mockFile);
              foto_tamnel.options.thumbnail.call(foto_tamnel, mockFile, "<?=base_url()?>images/tamnel/"+value.img);
              foto_upload.emit("complete", mockFile);
              tumnell = true;
            }else{
              var mockFile = { name: value.img, id: value.id, path: 'produk'};
              foto_upload.options.addedfile.call(foto_upload, mockFile);
              foto_upload.options.thumbnail.call(foto_upload, mockFile, "<?=base_url()?>images/produk/"+value.img);
            }
          });
        }
      })
    }
    showimg();
  })
</script>
<script>
  $(document).ready(function () {
    $("#kelurahan").chained("#kecamatan");
    $("#sub").chained("#kategori");
    $("#form").validate( {
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        // form.submit();
        if (tumnell == true && foto_tamnel.files != '') {
          foto_tamnel.processQueue();
          foto_tamnel.on("complete", function (fileu) {
            if (foto_upload.files == '') {
              if (foto_tamnel.getUploadingFiles().length === 0 && foto_tamnel.getQueuedFiles().length === 0){
                form.submit();
              }
            }else{
              if (foto_tamnel.getUploadingFiles().length === 0 && foto_tamnel.getQueuedFiles().length === 0){
                foto_upload.processQueue();
                foto_upload.on("success", function (files) {
                  if (foto_upload.getUploadingFiles().length === 0 && foto_upload.getQueuedFiles().length === 0){
                    form.submit();
                  }
                });
              }
            }
          });
        }else{
          if (tumnell == true && foto_upload.files != '') {
            foto_upload.processQueue();
            foto_upload.on("success", function (files) {
              if (foto_upload.getUploadingFiles().length === 0 && foto_upload.getQueuedFiles().length === 0){
                form.submit();
              }
            });
          }else{
            if (tumnell == false && foto_tamnel.files != '') {
              foto_tamnel.processQueue();
              foto_tamnel.on("complete", function (fileu) {
                if (foto_upload.files == '') {
                  if (foto_tamnel.getUploadingFiles().length === 0 && foto_tamnel.getQueuedFiles().length === 0){
                    form.submit();
                  }
                }else{
                  if (foto_tamnel.getUploadingFiles().length === 0 && foto_tamnel.getQueuedFiles().length === 0){
                    foto_upload.processQueue();
                    foto_upload.on("success", function (files) {
                      if (foto_upload.getUploadingFiles().length === 0 && foto_upload.getQueuedFiles().length === 0){
                        form.submit();
                      }
                    });
                  }
                }
              });
            }else{
              form.submit();
            }
          }
        }
      }
    });
  })
</script>