
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-5">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->

        <div class="col-sm-5 m_pag">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola UMKM
            </li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
          </ol>
        </div>

        <div class="col-sm-2">
          <div class="d-flex justify-content-center">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
        <div class="col-sm-5 pc_pag">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola UMKM
            </li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
          </ol>
        </div><!-- /.col -->
        
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- filter -->
  <?php if ($this->session->userdata('group_id') == 'c4ca4238a0b923820dcc509a6f75849b'): ?>
    <form id="form" action="<?=site_url().'/'.$menu['controller'].'/filter'?>" method="post" autocomplete="off">
      <div class="d-flex justify-content-center">
        <div class="col-md-2">     
          <select class="form-control form-control-sm select2" name="toko" id="toko">
            <option value="">-- Toko --</option>
            <?php foreach($toko as $r):?>
              <option value="<?=$r['id']?>" <?php if(@$cookie['filter']['toko'] == $r['id']){echo 'selected';}?>><?=$r['nama_market']?></option>
            <?php endforeach;?>
          </select>
        </div>
        <div class="col-md-2">     
          <select class="form-control form-control-sm select2" name="kategori" id="kategori">
            <option value="">-- Kategori --</option>
            <?php foreach($kategori as $r):?>
              <option value="<?=$r['id']?>" <?php if(@$cookie['filter']['kategori'] == $r['id']){echo 'selected';}?>><?=$r['kategori']?></option>
            <?php endforeach;?>
          </select>
        </div>
        <div class="col-md-2">     
          <select class="form-control form-control-sm select2" name="status" id="status">
            <option value="">-- Status --</option>
            <option value="Dipublikasi" <?php if(@$cookie['filter']['status'] == 'Dipublikasi'){echo 'selected';}?>>Dipublikasi</option>
            <option value="Diarsipkan" <?php if(@$cookie['filter']['status'] == 'Diarsipkan'){echo 'selected';}?>>Diarsipkan</option>
            
          </select>
        </div>
        <div class="col-md-4">
          <div class="row">
            <div class="col-md-6 input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" style="font-size: 11px">Rp.</span>
              </div>
              <input type="text" class="form-control form-control-sm num-int" name="min" id="min" value="<?=@$cookie['filter']['min']?>" placeholder="Minimal">
            </div>
            <div class="col-md-6 input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" style="font-size: 11px">Rp.</span>
              </div>
              <input type="text" class="form-control form-control-sm num-int" name="max" id="max" value="<?=@$cookie['filter']['max']?>" placeholder="Maksimal">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <button class="btn btn-sm btn-primary">Terapkan</button>
          </div>
          <?php if (@$cookie['filter']): ?>
            <div class="col-md-6">
              <a href="<?=site_url().'/_app/reset/'.$menu['controller']?>">
                <button type="button" class="btn btn-sm btn-danger">Batal</button>
              </a>
            </div>
          <?php endif ?>
        </div>

      </div>
    </form>
  <?php endif ?>
  <!-- end filter -->

  <!-- Main content -->
  <div class="content mt-3">
    <div class="container-fluid">
      <div class="row">

        <?php if ($this->session->userdata('group_id') == '297a139cfbf6720acb448ee95d9f7a12'): ?>
          <div class="col-md-2 col-lg-2 col-6 col-sm-3 col-xs-3">
            <a href="<?=site_url()?>/umkm_produk/form">
              <div class="card" style="height: 96% !important;">
                <div class="card-header" style="border: none !important;">
                  <h5 class="text-center">Tambah Produk</h5>
                </div>
                <div class="card-body text-center d-flex justify-content-center">
                  <div class="d-flex align-items-center">

                    <i class="fas fa-plus fa-7x"></i>

                  </div>
                </div>
              </div>
            </a>
          </div>
        <?php endif ?>

        <?php foreach ($main as $v): ?>
          <div class="col-md-2 col-lg-2 col-6 col-sm-3 col-xs-3">
            <div class="card" title="<?=$v['nama_produk']?>">
              <img class="card-img-top" src="<?=base_url()?>images/tamnel/<?=$v['img']?>" alt="Card image cap" style="height: 10rem">
              <div class="card-body">
                <p class="card-title"><?=character_limiter((string)$v['nama_produk'], 12, $end_char = '&#8230;')?></p>
                <p class="card-text"><b>Rp <?=numSys($v['harga'])?>,-</b></p>
                <div class="dropup">
                  <button type="button" class="btn btn-xs btn-primary btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Aksi
                  </button>
                  <div class="dropdown-menu">
                    <?php if($menu['_update'] == 1):?>
                      <a class="dropdown-item" href="<?=site_url().'/'.$menu['controller'].'/form/'.$v['id']?>">Ubah</a>
                      <a class="dropdown-item" href="<?=site_url().'/'.$menu['controller'].'/status/disable/'.$v['id']?>">Arsipkan</a>
                    <?php endif; ?>
                    <?php if($menu['_delete'] == 1):?>
                      <a class="dropdown-item" href="<?=site_url().'/'.$menu['controller'].'/delete/'.$v['id']?>">Hapus</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach ?>

      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
  <!-- /.content-wrapper -->
</div>
  <script>
  $(document).ready(function () {
    $("#form").validate( {
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>