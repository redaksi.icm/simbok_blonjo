<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umkm_produk extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      'pa_kategori/m_pa_kategori',
      'p_signup/mp_signup',
      'p_toko/m_p_toko',
      'umkm_produk/m_umkm_produk'
    ));
    $this->load->helper('text');
    if ($this->session->userdata('group_id') == '297a139cfbf6720acb448ee95d9f7a12') {
      $ord = 11;
    }else{
      $ord = 12;
    }

    $this->controller = 'umkm_produk';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'created_at','type' => 'DESC');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = $ord;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    //auth
    if ($this->menu['_read'] == 0) redirect(site_url().'/_error/error_403');
    //cookie
    if ($this->session->userdata('group_id') == '297a139cfbf6720acb448ee95d9f7a12') {
      $this->cookie['filter']['status'] = "Dipublikasi";
    }
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_umkm_produk->all_rows($this->cookie);
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_umkm_produk->list_data($this->cookie);
    $data['pagination_info'] = paginationInfo(count($data['main']), $this->cookie);
    $data['toko'] = $this->m_p_toko->all_toko();
    // var_dump($data['toko']); die();
    $data['kategori'] = $this->m_pa_kategori->get_induk_kategori();
    //set pagination
    setPagination($this->controller, $this->cookie);
    //render
    createLog(1,$this->controller);
    $this->render('index',$data);
  }
  public function filter()
  {
    $this->cookie['filter'] = html_remover($this->input->post(NULL, TRUE));
    $this->cookie['filter']['min'] = numId($this->cookie['filter']['min']);;
    $this->cookie['filter']['max'] = numId($this->cookie['filter']['max']);;
    setCookieMenu($this->controller, $this->cookie);
    redirect(site_url("$this->controller"));
  }

  public function form($id = null)
  {
    $this->session->unset_userdata('img');
    $data['toko'] = $this->m_p_toko->toko($this->session->userdata('umkm_id'));
    if ($id == null) {
      //auth
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      createLog(2,$this->controller);
      $data['main'] = ['ordinat_s' => $data['toko']['ordinat_s'], 'ordinat_e' => $data['toko']['ordinat_e']];
    }else{
      //auth
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      createLog(3,$this->controller);
      $data['main'] = $this->m_umkm_produk->by_field('id', $id);
    }
    $data['id'] = $id;
    // var_dump($data['toko']); die();
    $data['menu'] = $this->menu;
    $data['kategori'] = $this->m_pa_kategori->get_induk_kategori();
    $data['sub'] = $this->m_pa_kategori->get_sub_kategori();
    $data['all'] = $this->m_umkm_produk->all_data();
    $data['kecamatan'] = $this->mp_signup->get_kecamatan();
    $data['kelurahan'] = $this->mp_signup->get_kelurahan();
    // maps : init
    $this->load->library('googlemaps');
    $config['center']   = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $config['zoom']   = '15';
    $this->googlemaps->initialize($config);
    // maps : marker
    $marker = array();
    $marker['position'] = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $marker['infowindow_content'] = ''.@$data['main']['ordinat_s'].'';
    $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter_withshadow&chld=A|9999FF|000000';
    $this->googlemaps->add_marker($marker);
    // var_dump($data['main']); die();
    // maps : render
    $data['map'] = $this->googlemaps->create_map();

    $this->render('form',$data);
    $this->load->view('js_maps');
  }

  public function save($id = null)
  {

    $data = html_remover($this->input->post(null,true));
    $data['deskripsi'] = html_escape($this->input->post("deskripsi",true));
    unset($data['files']);
    if(!isset($data['is_active'])){$data['is_active'] = 0;}
    if ($id == null) {
      $data['id'] = md5(date('YmdHis').$this->session->userdata('user_id').time());
      $data['id_umkm'] = $this->session->userdata('umkm_id');
      $data['harga'] = numId($data['harga']);
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      if ($this->session->userdata('img')) {
      }
      $this->m_umkm_produk->store($data);
      $tamnel = ['id' => md5(date('YmdHis').$data['id'].'0'),
      'produk_id' => $data['id'],
      'img' => $this->session->userdata('img_tamnel'),
      'ins_number' => 0];
      if ($this->session->userdata('img_tamnel') != FALSE) {
        $this->m_umkm_produk->save_img($tamnel);
        // var_dump($this->session->userdata('img_tamnel')); die();
        $this->session->unset_userdata('img_tamnel');
      }
      $img = $this->session->userdata('img');
      $i = 1;
      foreach ($img as $v) {
        $image = ['id' => md5(date('YmdHis').$data['id'].$i++),
        'produk_id' => $data['id'],
        'img' => $v,
        'ins_number' => $i];
        $this->m_umkm_produk->save_img($image);
      }
      createLog(2, $this->menu['menu']);
      if ($this->session->userdata('img')) {
        $this->session->unset_userdata('img');
      }
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    }else{
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      unset($data['old']);
      $data['harga'] = numId($data['harga']);
      $this->m_umkm_produk->update($id,$data);
      $tamnel = ['id' => md5(date('YmdHis').$data['id'].'0'),
      'produk_id' => $id,
      'img' => $this->session->userdata('img_tamnel'),
      'ins_number' => 0];
      if ($this->session->userdata('img_tamnel') != FALSE) {
        $this->m_umkm_produk->save_img($tamnel);
        $this->session->unset_userdata('img_tamnel');
      }
      $img = $this->session->userdata('img');
      $i = 1;
      foreach ($img as $v) {
        $image = ['id' => md5(date('YmdHis').$id.$i++),
        'produk_id' => $id,
        'img' => $v,
        'ins_number' => $i];
        $this->m_umkm_produk->save_img($image);
      }
      createLog(3, $this->menu['menu']);
      if ($this->session->userdata('img')) {
        $this->session->unset_userdata('img');
      }
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    if ($this->menu['_delete'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    $cek = $this->m_umkm_produk->get_img($id);
    foreach ($cek as $k) {
      if ($k['ins_number'] == 0) {
        unlink('./images/tamnel/'.$k['img']);
      }
      unlink('./images/produk/'.$k['img']);
    }
    // die();
    $this->m_umkm_produk->delete($id);
    $this->m_umkm_produk->del_img($id, 1);
    createLog(4, $this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function status($type = null, $id = null)
  {
    if ($this->menu['_update'] == 0 || $type == null || $id == null) redirect(site_url().'/_error/error_403');
    if ($type == 'enable') {
      $this->m_umkm_produk->update($id, array('is_active' => 1));
    }else{
      $this->m_umkm_produk->update($id, array('is_active' => 0));
    }
    createLog(3,$this->menu['menu']);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function multiple($type = null)
  {
    if ($this->menu['_update'] == 0 || $this->menu['_delete'] == 0) redirect(site_url().'/_error/error_403');
    $data = $this->input->post(null,true);
    // var_dump($data); die();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {          
          case 'delete':
          $cek = $this->m_umkm_produk->img_cek($key);
          unlink('./images/produk/'.$cek->img);
          $this->m_umkm_produk->delete($key);
          $flash = 'Data berhasil dihapus.';
          $t = 4;
          break;

          case 'enable':
          $this->m_umkm_produk->update($key, array('is_active' => 1));
          $flash = 'Data berhasil diaktifkan.';
          $t = 3;
          break;

          case 'disable':
          $this->m_umkm_produk->update($key, array('is_active' => 0));
          $flash = 'Data berhasil dinonaktifkan.';
          $t = 3;
          break;
        }
      }
    }
    createLog($t,$this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }
  
  public function authorization($id = null)
  {
    if ($this->menu['_update'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    createLog(3,$this->controller);
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['main'] = $this->m_umkm_produk->by_field('id', $id);
    $data['detail'] = $this->m_umkm_produk->detail_data($id);
    createLog(3,$this->menu['menu']);
    $this->render('authorization', $data);
  }

  public function authorize()
  {
    $data = html_escape($this->input->post(null,true));
    if ($this->menu['_update'] == 0 || $data == null) redirect(site_url().'/_error/error_403');
    $this->m_umkm_produk->authorize($data);
    createLog(3,$this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function upload()
  {
    $config['upload_path']          = './images/produk/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['overwrite']            = true;
    $config['max_size']             = 10240; // 10MB
    $config['encrypt_name']         = TRUE;
    if ($this->session->userdata('img')) {
      $data = $this->session->userdata('img');
    } else {
      $data = array();
    }
    
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('img')) 
      $data[] = $this->upload->data('file_name');
    $this->session->set_userdata('img', $data);
    
    return "no-image.png";
  }

  public function upload_tamnel()
  {
    $config['upload_path']          = './images/tamnel/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['overwrite']            = true;
    $config['max_size']             = 10240; // 10MB
    $config['encrypt_name']         = TRUE;
    
    $this->load->library('upload', $config);
    // $this->upload->resize();  
    if ($this->upload->do_upload('img_tamnel')) $this->session->set_userdata('img_tamnel', $this->upload->data("file_name"));
    return "no-image.png";
  }

  public function img_rm($id = null, $img = null)
  {
    if ($id != null || $img != null) {
      $cek = $this->m_umkm_produk->img_cek($id);
      if ($cek->img == $img) {
        unlink('./images/produk/'.$img);
        $this->m_umkm_produk->del_img($id, 1);
        redirect(site_url().'/'.$this->controller.'/form/'.$id);
      }else{
        redirect(site_url().'/_error/error_403');
      }

    } else {
      redirect(site_url().'/_error/error_403');
    }

  }

  public function get_images()
  {
    $id = $this->input->post('id');
    $img = $this->m_umkm_produk->get_img($id);
    echo json_encode($img);
  }

  public function del_imgs()
  {
    $data = $this->input->post(NULL, TRUE);
    unlink('./images/'.$data['path'].'/'.$data['name']);
    $del = $this->m_umkm_produk->del_img(1, $data['id']);
    if ($del) {
      echo json_encode(['status' => true]);
    }else{
      echo json_encode(['status' => false]);
    }
  }
}