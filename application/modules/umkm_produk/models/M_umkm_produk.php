<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_umkm_produk extends CI_Model {

  public function list_data($data)
  {
    $umkm = $this->session->userdata('umkm_id');
    $where = "WHERE a.is_deleted = 0 AND b.ins_number = 0 ";
    if ($this->session->userdata('group_id') == '297a139cfbf6720acb448ee95d9f7a12') {
      $where .= " AND a.id_umkm = '$umkm'";
    }
    if (@$data['filter']['kategori'] && $data['filter']['kategori'] != '') {
      $where .= "AND a.id_induk_kategori LIKE '%".$this->db->escape_like_str($data['filter']['kategori'])."%' ";
    }
    if (@$data['filter']['toko'] && $data['filter']['toko'] != '') {
      $where .= "AND a.id_umkm LIKE '%".$this->db->escape_like_str($data['filter']['toko'])."%' ";
    }
    if (@$data['filter']['status'] && $data['filter']['status'] != '') {
      $data['filter']['status'] = ($data['filter']['status'] == 'Dipublikasi' ? '1' : '0');
      $where .= "AND a.is_active = ".@$data['filter']['status']." ";
    }
    if (@$data['filter']['min'] && $data['filter']['min'] != '' && $data['filter']['max'] == '') {
      $where .= "AND a.harga >= ".$this->db->escape($data['filter']['min'])." ";
    }

    if (@$data['filter']['max'] && $data['filter']['max'] != '') {
      if (@$data['filter']['min'] && $data['filter']['min'] != '') {
        $where .= "AND a.harga BETWEEN ".$this->db->escape($data['filter']['min'])." AND ".$this->db->escape($data['filter']['max'])." ";
      }else{
        $where .= "AND a.harga <= ".$this->db->escape($data['filter']['max'])." ";
      }
    }
    if ($data['search']['term'] != '') {
      $where .= "AND a.nama_produk LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT a.*, b.img FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id
    $where
    GROUP BY b.produk_id
    ORDER BY "
    .$data['order']['field']." ".$data['order']['type'].
    " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM tb_produk a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($data)
  {
    $umkm = $this->session->userdata('umkm_id');
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND b.ins_number = 0 ";
    if ($this->session->userdata('group_id') == '297a139cfbf6720acb448ee95d9f7a12') {
      $where .= " AND a.id_umkm = '$umkm'";
    }
    if (@$data['filter']['kategori'] && $data['filter']['kategori'] != '') {
      $where .= "AND a.id_induk_kategori LIKE '%".$this->db->escape_like_str($data['filter']['kategori'])."%' ";
    }
    if (@$data['filter']['toko'] && $data['filter']['toko'] != '') {
      $where .= "AND a.id_umkm LIKE '%".$this->db->escape_like_str($data['filter']['toko'])."%' ";
    }
    
    if (@$data['filter']['status'] && $data['filter']['status'] != '') {
      $data['filter']['status'] = ($data['filter']['status'] == 'Dipublikasi' ? '1' : '0');
      $where .= "AND a.is_active = ".@$data['filter']['status']." ";
    }

    if (@$data['filter']['min'] && $data['filter']['min'] != '' && $data['filter']['max'] == '') {
      $where .= "AND a.harga >= ".$this->db->escape($data['filter']['min'])." ";
    }

    if (@$data['filter']['max'] && $data['filter']['max'] != '') {
      if (@$data['filter']['min'] && $data['filter']['min'] != '') {
        $where .= "AND a.harga BETWEEN ".$this->db->escape($data['filter']['min'])." AND ".$this->db->escape($data['filter']['max'])." ";
      }else{
        $where .= "AND a.harga <= ".$this->db->escape($data['filter']['max'])." ";
      }
    }
    if ($data['search']['term'] != '') {
      $where .= "AND a.nama_produk LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }
    $sql = "SELECT COUNT(1) as total FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('tb_produk', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('tb_produk', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('tb_produk');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('tb_produk', $data);
    }
  }

  public function save_img($data)
  {
    $this->db->insert('img_produk', $data);
  }

  public function get_img($id)
  {
    $where = "WHERE a.produk_id = '$id' ";
    $sql = "SELECT a.* FROM img_produk a $where ORDER BY ins_number ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function del_img($id_produk, $id_gambar)
  {
    if ($id_produk != 1) {
      $del_by = 'produk_id';
      $id = $id_produk;
    }
    if ($id_gambar != 1) {
      $del_by = 'id';
      $id = $id_gambar;
    }
    $this->db->where($del_by, $id)->delete('img_produk');
  }

  public function updateBymarket($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id_umkm', $id)->update('tb_produk', $data);
  }
  public function deletBymarket($id)
  {
    $this->db->where('id_umkm', $id)->delete('tb_produk');
  }
  public function getBymarket($id)
  {
    return $this->db->get_where('tb_produk', array('id_umkm' => $id))->result_array();
  }

}

// CREATE EVENT reset
//     ON SCHEDULE
//       EVERY 1 HOUR
//         DO
// update T1 
// set state=1 
//   where time < date_sub(now(),interval 24 hour) 
//   and (state=0 or state=2) ;