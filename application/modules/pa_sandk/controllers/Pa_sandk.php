<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pa_sandk extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      'pa_sandk/m_pa_sandk'
    ));

    $this->controller = 'pa_sandk';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'judul','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    //auth
    if ($this->menu['_read'] == 0) redirect(site_url().'/_error/error_403');
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_pa_sandk->by_field('id', 'sk');
    createLog(1,$this->controller);
    $this->render('index',$data);
  }

  public function save($id = null)
  {
    $data['deskripsi'] = html_escape($_POST['deskripsi']);
    // var_dump($data); die();
    // unset($data['files']);
    if(!isset($data['is_active'])){$data['is_active'] = 0;}
    if ($id == null) {
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      if ($this->session->userdata('img')) {
        $data['img'] = $this->session->userdata('img');
      }
      $this->m_pa_sandk->store($data);
      createLog(2, $this->menu['menu']);
      if ($this->session->userdata('img')) {
        $this->session->unset_userdata('img');
      }
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    }else{
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      $this->m_pa_sandk->update($id,$data);
      createLog(3, $this->menu['menu']);
      if ($this->session->userdata('img')) {
        $this->session->unset_userdata('img');
      }
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }
}