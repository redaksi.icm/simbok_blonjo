<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_survei extends CI_Model {

  function insert_survey($data= NULL) {
    if (get_ip_address() == '::1') {
      $ip_address = '127.0.0.1';
    }else{
      $ip_address = get_ip_address();
    }
    $get_device = get_device();

    $data_survey['survei_id'] = $data['survei_id'];
    $data_survey['date_survei'] = date('Y-m-d H:i:s');
    $data_survey['msg'] = @$data['msg'];

    $data_survey['ip_address'] = $ip_address;
    $data_survey['hostname'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
    $data_survey['browser_type'] = $get_device['browser_type'];
    $data_survey['os_type'] = $get_device['os_type'];
    $data_survey['device_type'] = $get_device['device_type'];
        // check insert survey
    $check_survey = $this->check_insert_survey($ip_address);
        //
    if ($check_survey !=0) {
      $outp = false;
    }else{
      $this->db->insert('hasil_survei', $data_survey);
      $outp = true;
    }
        //
    return $outp;
  }

  function check_insert_survey($ip_address=null) {
    $sql = "SELECT 
    COUNT(a.id) AS count_data 
    FROM hasil_survei AS a
    WHERE a.ip_address=?";
    $query = $this->db->query($sql, $ip_address);
    $row = $query->row_array();
    $count_data = $row['count_data'];
    return $count_data;
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1";

    $sql = "SELECT COUNT(1) as total FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('tb_produk', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('tb_produk', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('tb_produk');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('tb_produk', $data);
    }
  }

  public function insert_arsip()
  {
    $sql = "UPDATE tb_produk a 
    INNER JOIN 
    (
    SELECT a.*,(a.count_day - a.exp_product_day) as last_day 
    FROM 
    (
    SELECT a.*,
    DATEDIFF(DATE(NOW()), a.publish_date) as count_day
    FROM 
    (
    SELECT a.id,a.created_at,a.updated_at,a.is_active,IF(a.updated_at IS NULL, a.created_at, a.updated_at) as publish_date,b.exp_product_day
    FROM tb_produk a 
    LEFT JOIN _profile b ON 1=1
    WHERE a.is_active='1'
    ) as a
    ) as a
    WHERE (a.count_day - a.exp_product_day) > 0
    ) b ON a.id=b.id
    SET a.is_active = '0'";
    $this->db->query($sql);
  }
}