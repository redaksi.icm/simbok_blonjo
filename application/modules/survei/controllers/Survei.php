<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survei extends PC_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    $this->load->model(array(
      'survei/m_survei'
    ));

    $this->controller = 'survei';
  }

  public function ins()
  {
    $data = html_remover($this->input->post(NULL, TRUE));
    if ($data == NULL) {redirect(site_url('p_err/error404'));}
    $output = $this->m_survei->insert_survey($data);
    echo json_encode($output);
  }

}