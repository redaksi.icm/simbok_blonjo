<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _app extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model(array(
      '_config/m_config'
    ));
  }

  public function auth($controller)
  {
    $controller = $controller;
		$this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$controller);
		if ($this->menu == null) redirect(site_url().'/_error/error_403');
  }
  
  public function search($controller)
  {
    $this->auth($controller);
    $data = html_remover($this->input->post(null,true));
    if ($data == null) redirect(site_url().'/_error/error_403');
    $cookie = getCookieMenu($controller);
    $cookie['search'] = $data;
    setCookieMenu($controller,$cookie);
    redirect(site_url().'/'.$controller);
  }

  public function order($controller,$field = null,$type = null)
  {
    $this->auth($controller);
    if ($field == null || $type == null) redirect(site_url().'/_error/error_403');
    $cookie = getCookieMenu($controller);
    $cookie['order'] = array(
      'field' => $field,
      'type' => $type
    );
    setCookieMenu($controller,$cookie);
    redirect(site_url().'/'.$controller);
  }
  public function per_page($controller,$val = 10)
  {
    $this->auth($controller);
    $cookie = getCookieMenu($controller);
    $cookie['per_page'] = $val;
    setCookieMenu($controller,$cookie);
    redirect(site_url().'/'.$controller);
  }

  public function reset($controller)
  {
    $this->auth($controller);
    delete_cookie($controller);
    redirect(site_url().'/'.$controller);
  }
  
}