<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
		</div>
	</div>
</div>
<div class="center-flex">
	<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
		<div class="sign-in-page">
			<div class="center-flex" style="padding-bottom: 1rem;">
				<img src="<?=base_url()?>images/logos/<?=$profile['logo']?>" alt="logo" style="max-height: 7rem;">
			</div>
			<div class="text-center" style="padding-bottom: 2rem;">
				<span style="font-size: 2em; color: #39ea0e;">Pendaftaran Berhasil!</span>
			</div>

			<div>
				<?=@htmlspecialchars_decode($main['deskripsi'])?>
			</div>

			<div class="row">
				<div class="center-flex" style="padding-top: 2rem; padding-bottom: 2rem;">
					<div class="col-md-4 text-right">
						<p style="font-size: 25px;"><b>Username :</b></p>
						<p style="font-size: 25px;"><b>Password :</b></p>
					</div>
					<div class="col-md-4 text-left">
						<p style="font-size: 25px;"><b><?=@$user['u']?></b></p>
						<p style="font-size: 25px;"><b><?=@$user['p']?></b></p>
					</div>
				</div>
			</div>

			<div class="text-center" style="padding-bottom: 5rem;">
				<p style="font-size: 20px;">Rahasiakan Detail Akun Anda Agar Akun Anda Selalu Aman. Gunakan Akun Anda Dengan Bijak Dan Manfaatkan Kesempatan Ini Dengan Sebaik Baiknya. </p>
			</div>
			
		</div>
	</div>
</div>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
		</div>
	</div>
</div>