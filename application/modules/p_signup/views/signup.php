<style type="text/css">
	.form-control.is-invalid, .was-validated .form-control:invalid {
		border-color: #dc3545;
		padding-right: 2.25rem;
		background-size: calc(.75em + .375rem) calc(.75em + .375rem);
	}

	.invalid-feedback {
		display: none;
		width: 100%;
		margin-top: .25rem;
		font-size: 80%;
		color: #dc3545;
	}
</style>

<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
		</div>
	</div>
</div>

<div class="center-flex">
	<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
		<div class="sign-in-page">
			<div class="center-flex" style="padding-bottom: 1rem;">
				<img src="<?=base_url()?>images/logos/<?=$profile['logo']?>" alt="logo" style="max-height: 7rem;">
			</div>
			<div class="text-center" style="padding-bottom: 2rem;">
				<span style="font-size: 2em;">PENDAFTARAN</span>
			</div>
			<?php if(@$this->session->flashdata('flash_error')):?>
				<div class="center-flex">
					<div class="col-md-10">
						<div class="row" id="flash_error">
							<div class="col-12">
								<div class="alert alert-danger alert-dismissible">
									<?=$this->session->flashdata('flash_error')?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif;?>
			<form id="form" action="<?=site_url()?>/p_signup/proses" method="post">
				<div class="row">
					<div class="col-md-6 col-lg-6">

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Nama Market<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="text" class="form-control" name="nama_market" required placeholder="Masukan Nama Market" value="<?=@$nama_market?>">
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Nama Pemilik<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="text" class="form-control" name="nama_pemilik" required placeholder="Masukan Nama Lengkap" value="<?=@$nama_pemilik?>">
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">NIK/No. KTP<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="number" class="form-control" name="nik" required placeholder="Masukan NIK / No. KTP" value="<?=@$nik?>">
							</div>
						</div>
						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Foto KTP<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-4 col-ld-4">
								<div class="dropzone">
									<div class="dz-message">
										<i class="fas fa-upload fa-4x"></i>
										<br>
										<span>Unggah KTP</span>
									</div>
								</div>
							</div>
						</div>
						<br>
						<br>
						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Username<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="text" class="form-control" name="username" required placeholder="Digunakan Untuk Login">
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Password<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input id="password" type="password" class="form-control" name="password" required placeholder="Masukan Password">
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Repassword<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="password" class="form-control" name="repassword" required placeholder="Masukan Kembali Password">
							</div>
						</div>
					</div>

					<div class="col-md-6 col-lg-6">

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">No.Telepon/HP<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="number" class="form-control" name="no_hp" required placeholder="Masukan No. Telepon" value="<?=@$no_hp?>">
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">No. WhatsApp<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="number" class="form-control" name="wa" required placeholder="Masukan No. WhatsApp" value="<?=@$wa?>">
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Email<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<input type="email" class="form-control" name="email" required placeholder="Masukan Email Aktif" value="<?=@$email?>">
							</div>
						</div>
						<br>
						<br>
						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Legalitas Market</p>
							</div>
							<div class="col-md-8 col-ld-8">
								<textarea class="form-control" name="legalitas"><?=@$legalitas?></textarea>
								<span style="color: blue; font-size: 80%;">* Di Isi legalitas Market Seperti SIUP, SKDP, TDP, dan lain-lain Bila Ada </span>
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Kecamatan<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<select class="form-control select2" name="kecamatan" id="kec" required>
									<option value="">-- Pilih kecamatan</option>
									<?php foreach($kec as $r):?>
										<option id="<?=$r['kecamatan']?>" value="<?=$r['kecamatan']?>" <?php if(@$pencarian['kecamatan'] == $r['kecamatan']){echo 'selected';}?>><?=$r['kecamatan']?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Kelurahan<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<select class="form-control select2" name="kelurahan" id="kel" required>
									<option value="">-- Pilih Kelurahan</option>
									<?php foreach($kel as $r):?>
										<option value="<?=$r['kelurahan']?>" <?php if(@$pencarian['kelurahan'] == $r['kelurahan']){echo 'selected';}?> data-chained="<?=$r['kecamatan']?>"><?=$r['kelurahan']?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>

						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-3 col-md-3 text-right lable-form">
								<p class="text-form">Alamat Lengkap<span class="text-danger">*</span></p>
							</div>
							<div class="col-md-8 col-ld-8">
								<textarea class="form-control" name="alamat_lengkap" required><?=@$alamat_lengkap?></textarea>
								<span style="color: blue; font-size: 80%;">* Di Isi Nama Jalan, RT/RW, Dan Nomor Gedung Bila Ada </span>
							</div>
						</div>

						

						<input type="hidden" name="ordinat_s" id="latitude" class="form-control input-sm" value="<?=@$main['ordinat_s']?>">
						<input type="hidden" name="ordinat_e" id="longitude" class="form-control input-sm" value="<?=@$main['ordinat_e']?>">
						<input type="hidden" name="latdegree" id="latdegree" class="form-control input-sm" value="<?=@$main['latdegree']?>">
						<input type="hidden" name="latminute" id="latminute" class="form-control input-sm" value="<?=@$main['latminute']?>">
						<input type="hidden" name="latsecond" id="latsecond" class="form-control input-sm" value="<?=@$main['latsecond']?>">
						<input type="hidden" name="lngdegree" id="lngdegree" class="form-control input-sm" value="<?=@$main['lngdegree']?>">
						<input type="hidden" name="lngminute" id="lngminute" class="form-control input-sm" value="<?=@$main['lngminute']?>">
						<input type="hidden" name="lngsecond" id="lngsecond" class="form-control input-sm" value="<?=@$main['lngsecond']?>"	>

					</div>
				</div>
				<div class="center-flex" style="padding-top: 1em;">
					<input id="pac-input" class="controls form-control form-control-sm map-src" type="text" placeholder="Pencarian">
					<div id="map" class="map"></div>
				</div>
				<div class="center-flex" style="padding-top: 1em;">
					<span class="text-primary">*Tarik Dan Arahkan Titik Merah Untuk Mengisi letak Market</span>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-md-6 col-lg-6">
						<div class="row" style="margin-bottom: 0.5em">
							<div class="col-md-2 text-right form-group form-check">
								<input type="checkbox" class="form-check-input" required id="agriment" name="agriment">
							</div>
							<div class="col-md-8 col-ld-8">
								<label class="form-check-label" for="agriment">Saya Setuju Dengan Semua <a href="p_signup/sk">Persyaratan Dan Ketentuan</a></label>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-6">
						<div class="row">
							<div class="col-md-3">
								<div class="row">
									<button type="text" class="btn btn-dark btn-disabled disabled" id="chapta" style="font-size: 15px; font-weight: bold;" data-id=""></button>
									<i class="fas fa-sync btn text-primary" onclick="gantiChapta()"></i>
								</div>
							</div>
							<div class="col-md-4">
								<input type="text" class="form-control" name="chapta" id="rechapta" required placeholder="Masukan Chapta" onkeyup="ups()">
							</div>
						</div>
					</div>
				</div>

				<div class="form-group" style="margin-top: 3rem;">
					<button type="submit" class="btn btn-primary btn-block btn-submit"><b>Daftar</b></button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
		</div>
	</div>
</div>
<?php $this->load->view('js')?>