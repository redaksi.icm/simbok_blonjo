<script>
	$("#kel").chained("#kec");
	$(document).ready(function () {
		$("#form").validate( {
			rules: {
				nik : {
					maxlength: 16,
					minlength: 16
				},
				repassword : {
					equalTo : "#password"
				}
			},
			messages: {

			},
			errorElement: "em",
			errorPlacement: function (error,element) {
				error.addClass("invalid-feedback");
				if (element.prop("type") === "checkbox") {
					error.insertAfter(element.next("label"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function (element,errorClass,validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			},
			submitHandler: function (form) {
				$(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
				$(".btn-submit").addClass('disabled');
				$(".btn-cancel").addClass('disabled');
				profil.processQueue();
				profil.on("complete", function (fileu) {
					if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
						form.submit();
					}
				});
			}
		});

		setTimeout(function () {
			$('#flash_error').fadeOut() 
		},5000);
	})
</script>
<script type="text/javascript">
	Dropzone.autoDiscover = false;

	var profil= new Dropzone(".dropzone",{
		url: '<?=site_url()?>/p_signup/upload/',
		maxFiles: 1,
		maxFilesize: 5,
		method:"post",
		acceptedFiles:"image/*",
		paramName:"ktp",
		dictInvalidFileType:"Type file ini tidak dizinkan",
		addRemoveLinks:true,
		autoProcessQueue: false
	});

	$(".btn-submit").click(function (e) {
		var formId = '<?=@$main['id']?>';
		if (profil.files == '') {
			e.preventDefault();
			Swal.fire({
				title: 'Wajib Upload KTP!',
				text: "Silahkan Isi / Upload pada form foto KTP.",
				type: 'warning',
				customClass: 'swal-wide'
			});
		}else{
			var	latitude = $("#latitude").val();
			var	longitude = $("#longitude").val();
			if (longitude =="" && latitude == "") {
				e.preventDefault();
				Swal.fire({
					title: 'Wajib Tentukan Letak Market Pada Map!',
					text: "Silahkan isi letak market pada Map dengan menggeser tanda lokasi berwarna merah pada Map.",
					type: 'warning',
					customClass: 'swal-wide'
				});
			}else{
				if($("#agriment").is(':unchecked')) {
					e.preventDefault();
					Swal.fire({
						title: 'Wajib Setuju Dengan Segala Syaratan Dan Ketentuan!',
						text: "Harap centang pada form Syarat dan Ketentuan Simbok Blonjo.",
						type: 'warning',
						customClass: 'swal-wide'
					});
				}else{
					var chapta = $("#chapta").data("id");
					var rechapta = $("#rechapta").val();
					if (chapta != rechapta) {
						e.preventDefault();
						Swal.fire({
							title: 'Chapta Wajib Sama!',
							text: "Masukan kode chapta yang sama.",
							type: 'warning',
							customClass: 'swal-wide'
						});
					}
				}
			}
		}

	});
</script>
<script>
	function gantiChapta() {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < 6; i++ ) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		$("#chapta").html(result);
		$("#chapta").data("id", result);
	}
	function ups() {
		var ganti = $("#rechapta").val();
		$("#rechapta").val(ganti.toUpperCase());
	}
	gantiChapta();
</script>