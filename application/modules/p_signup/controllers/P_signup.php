<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_signup extends PC_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model(array(
      'p_signup/mp_signup',
      'pa_kategori/m_pa_kategori',
      '_group/m_group',
      'pa_sandk/m_pa_sandk',
      'pa_success/m_pa_success',
      '_user/m_user'
    ));
    if (@$this->session->userdata('user_id') != "") {redirect(site_url('p_err/error404'));}
  }

  public function index()
  {
    // maps : init
    $this->load->library('googlemaps');
    $config['center']   = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $config['zoom']   = '15';
    $this->googlemaps->initialize($config);
        // maps : marker
    $marker = array();
    $marker['position'] = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $marker['infowindow_content'] = ''.@$data['main']['ordinat_s'].'';
    $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter_withshadow&chld=A|9999FF|000000';
    $this->googlemaps->add_marker($marker);
        // maps : render
    $data['map'] = $this->googlemaps->create_map();
    $data['kec'] = $this->mp_signup->get_kecamatan();
    $data['kel'] = $this->mp_signup->get_kelurahan();
    $this->render('signup', $data );
    $this->load->view('umkm_pendaftar/js_maps');
  }

  public function proses()
  {
    $data = $this->input->post(NULL, TRUE);
    if ($data == "") {redirect(site_url('p_err/error404'));}
    // var_dump($data); echo($this->session->userdata('ktp')); die();
    $user_cek = $this->mp_signup->user_cek($data['username']);

    if ($user_cek->num_rows() >= 1) {
      $this->session->set_flashdata('flash_error', 'Username Sudah Terdaftar.');
      redirect(site_url('p_signup'), $data);
    }else{
      $_user = ['id' => md5(date('YmdHis').$data['username']),
      'user_password'=> md5(md5(md5($data['password']))),
      'user_name' => $data['username'],
      'group_id' => '297a139cfbf6720acb448ee95d9f7a12',
      'fullname' => $data['nama_pemilik'],
      'is_active' => 0,
      'created_by' => 'user'];
      $this->m_user->store($_user);

      // echo $data['username']; die();
      if(!isset($data['id'])){$data['id'] = md5(date('YmdHis'));}
      if(!isset($data['is_active'])){$data['is_active'] = 0;}
      if(!isset($data['id_user'])){$data['id_user'] = $_user['id'];}
      $data["ktp"] = @$this->session->userdata('ktp');
      unset($data['username']);
      unset($data['password']);
      unset($data['repassword']);
      unset($data['agriment']);
      unset($data['chapta']);
      $this->mp_signup->simpan($data);

      redirect('/p_signup/succsess?u='.html_remover($this->input->post('username')).'&p='.html_remover($this->input->post('password')).'');
    }
  }

  public function succsess()
  {
    $data['main'] = $this->m_pa_success->by_field('id', 'ss');
    $data['user'] = $this->input->get(NULL, TRUE);
    $this->render('succsess', $data);
  }
  public function sk()
  {
    $data['main'] = $this->m_pa_sandk->by_field('id', 'sk');
    $this->render('sk', $data);
  }

  public function upload()
  {
    $config['upload_path']          = './images/ktp/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['quality']              = '60%';
    $config['overwrite']            = true;
    $config['max_size']             = 10240; // 10MB
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);
    if ($this->upload->do_upload('ktp')) $this->session->set_userdata('ktp', $this->upload->data("file_name"));
    
    return "no-image.png";
  }

}