<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mp_signup extends CI_Model {

  public function user_cek($data)
  {
    return $this->db->get_where('_user',['user_name' => $data]);
  }

  public function simpan($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $data['username'];
    unset($data['username']);
    $this->db->insert('tb_umkm', $data);
  }

  public function get_kecamatan()
  {
    return $this->db->get('kecamatan')->result_array();
  }

  public function get_kelurahan()
  {
    $this->db->select('a.*, b.kecamatan');
    $this->db->from('kelurahan a');
    $this->db->join('kecamatan b', 'b.id = a.id_kecamatan', 'left');
    return $this->db->get()->result_array();
  }
  
}