<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_err extends PC_Controller {

  public function error404()
  {
    $data['err'] = TRUE;
    $this->render('index', $data);
  }

} 