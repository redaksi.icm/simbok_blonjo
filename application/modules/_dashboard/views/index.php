<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissible" id="login-alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Info!</h5>
            Selamat Datang Di Dashboard DISPERINDAG Kab.Kebumen
          </div>
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small card -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3><?=$pendaftar?></h3>

                  <p>UMKM Pendaftar</p>
                </div>
                <div class="icon">
                  <i class="fas fa-user-plus"></i>
                </div>
                <a href="<?=site_url()?>/umkm_pendaftar/index" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small card -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3><?=$terdaftar?></h3>

                  <p>UMKM Terdaftar</p>
                </div>
                <div class="icon">
                  <i class="fas fa-user-check"></i>
                </div>
                <a href="<?=site_url()?>/umkm_terdaftar/index" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>

            <div class="col-lg-3 col-6">
              <!-- small card -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3><?=$produk?></h3>

                  <p>Produk Aktif</p>
                </div>
                <div class="icon">
                  <i class="fas fa-gifts"></i>
                </div>
                <a href="<?=site_url()?>/umkm_produk/index" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->

            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small card -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3><?=$kategori?></h3>

                  <p>Total Kategori</p>
                </div>
                <div class="icon">
                  <i class="fas fa-cubes"></i>
                </div>
                <a href="<?=site_url()?>/pa_kategori/index" class="small-box-footer">
                  More info <i class="fas fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-3 col-12">
                  <div class="info-box mb-3 bg-primary">
                    <span class="info-box-icon"><i class="fas fa-grin-hearts"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Sangat Puas</span>
                      <span class="info-box-number"><?=numSys(@$survei['sp'])?></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <div class="info-box mb-3 bg-success">
                    <span class="info-box-icon"><i class="fas fa-smile-beam"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Puas</span>
                      <span class="info-box-number"><?=numSys(@$survei['p'])?></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <div class="info-box mb-3 bg-danger">
                    <span class="info-box-icon"><i class="fas fa-frown"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Kurang Puas</span>
                      <span class="info-box-number"><?=numSys(@$survei['kp'])?></span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                </div>
                <div class="col-md-9 col-12">
                  <div class="card">
                    <div class="card-header bg-warning">
                      <h2 class="card-title">Rekaman Survei Terbaru</h2>
                    </div>
                    <div class="card-body table-responsive p-0" style="height: 250px;">
                      <table class="table table-sm table-head-fixed text-nowrap">
                        <thead>
                          <tr>
                            <th>Hostname</th>
                            <th>IP Address</th>
                            <th>Perangkat</th>
                            <th>Sistem Oprasi</th>
                            <th>Jawaban Survei</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($survei_terbaru as $v): ?> 
                            <tr>
                              <td><?=$v['hostname']?></td>
                              <td><?=$v['ip_address']?></td>
                              <td><?=$v['device_type']?></td>
                              <td><?=$v['os_type']?></td>
                              <?php
                              switch ($v['survei_id']) {
                                case 1:
                                echo "<td>Sangat Puas</td>";
                                break;
                                case 2:
                                echo "<td>Puas</td>";
                                break;
                                case 3:
                                echo "<td>Kurang Puas</td>";
                                break;
                              }
                              ?>
                            </tr>
                          <?php endforeach ?>

                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="card">
                    <div class="card-header bg-secondary">
                      <h2 class="card-title">Total Produk Perkategori</h2>
                    </div>
                    <div class="card-body table-responsive p-0" style="height: 300px;">
                      <table class="table table-sm table-head-fixed text-nowrap">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th class="text-center">Total Produk</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;foreach ($prokat as $v): ?> 
                          <tr>
                            <td><?=$i++?></td>
                            <td><?=$v['kategori']?></td>
                            <td class="text-center"><?=$v['total']?></td>
                          </tr>
                        <?php endforeach ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                <div class="card direct-chat direct-chat-primary" style="position: relative; left: 0px; top: 0px;">
                  <div class="card-header ui-sortable-handle bg-info">
                    <h3 class="card-title">Masukan Terbaru Dari Pengunjung</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages" style="height: 300px !important">

                      <!-- Message to the right -->
                      <?php foreach ($msg as $val): ?>
                        <div class="direct-chat-msg right">
                          <div class="direct-chat-infos clearfix">
                            <span class="direct-chat-name float-right"><?=@$val['hostname']?></span>
                            <span class="direct-chat-timestamp float-right mr-2"><?=convertDateId(@$val['date_survei'])?></span>
                          </div>
                          <div class="direct-chat-text" style="margin: 0px !important">
                            <?=@$val['msg']?>
                          </div>
                        </div>
                      <?php endforeach ?>
                    </div>
                    <!--/.direct-chat-messages-->
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">

          </div>
        </div>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $("#login-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#login-alert").slideUp(1000);
  });
</script>
