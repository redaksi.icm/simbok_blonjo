<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _dashboard extends MY_Controller {

  var $controller, $menu;

  function __construct(){
    parent::__construct();
    
		
    $this->load->model(array(
      '_config/m_config',
      '_dashboard/m_dashboard'
    ));

    $controller = '_dashboard';
		$this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$controller);
		if ($this->menu == null) redirect(site_url().'/_error/error_403');
	}

  public function index()
  {
    $data['pendaftar'] = $this->m_dashboard->get_total_umkm(0);
    $data['terdaftar'] = $this->m_dashboard->get_total_umkm(1);
    $data['survei'] = $this->m_dashboard->get_total_survei();
    $data['produk'] = $this->m_dashboard->get_total_produk();
    $data['kategori'] = $this->m_dashboard->get_total_kategori();
    $data['prokat'] = $this->m_dashboard->total_produk_perkategori();
    $data['new_dafter'] = $this->m_dashboard->pendaftar_terbaru();
    $data['survei_terbaru'] = $this->m_dashboard->survei_terbaru();
    $data['msg'] = $this->m_dashboard->survei_terbaru(TRUE);
    $data['menu'] = $this->menu;
    $this->render('index',$data);
  }
	
}