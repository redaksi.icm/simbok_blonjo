<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	public function get_total_umkm($s)
	{
		return $this->db->get_where('tb_umkm',['is_active' => $s])->num_rows();
	}
	public function get_total_survei()
	{
		$data['sp'] = $this->db->get_where('hasil_survei',['survei_id' => 1])->num_rows();
		$data['p'] = $this->db->get_where('hasil_survei',['survei_id' => 2])->num_rows();
		$data['kp'] = $this->db->get_where('hasil_survei',['survei_id' => 3])->num_rows();
		return $data;
	}

	public function get_total_produk()
	{
		return $this->db->get_where('tb_produk',['is_active' => 1])->num_rows();
	}

	public function get_total_kategori()
	{
		return $this->db->get_where('pa_kategori',['is_active' => 1])->num_rows();
	}

	public function total_produk_perkategori()
	{
		$kategori = $this->db->get_where('pa_kategori',['is_active' => 1, 'type' => 1])->result_array();
		foreach ($kategori as $k => $v) {
			$data[$k]['kategori'] = $v['kategori'];
			$data[$k]['total'] = $this->db->get_where('tb_produk',['id_induk_kategori' => $v['id'], 'is_active' => 1])->num_rows();
		}
		return $data;
	}

	public function pendaftar_terbaru()
	{
		$this->db->select('*');
		$this->db->from('tb_umkm');
		$this->db->order_by('created_at', 'DESC');
		$this->db->limit(15);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function survei_terbaru($where = false)
	{
		$this->db->select('*');
		$this->db->from('hasil_survei');
		if ($where != false) {
			$this->db->where('msg !=', null);
		}
		$this->db->order_by('date_survei', 'DESC');
		$this->db->limit(25);
		$query = $this->db->get();
		return $query->result_array();
	}

}