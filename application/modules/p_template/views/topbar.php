<header class="header-style-1"> 

  <div class="top-bar animate-dropdown signup-mobail">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <?php if (!$this->session->userdata('user_id')): ?>
            <a href="<?=site_url()?>/p_signup" style="color: white;">
              <button class="btn btn-xs btn-yellow"><b>Daftar</b></button>
            </a>
            <a href="<?=site_url()?>/login" style="color: white; padding-left: 1rem;" target="_blank">
              <button class="btn btn-lg btn-primary"><b>Masuk</b></button>
            </a>
            <?php else :?>
              <a href="<?=site_url()?>/login" style="color: white; padding-left: 1rem;">
                <i class="fas fa-user fa-2x" style="padding-left: 1.8rem;"></i>
              </a>
            <?php endif ?>
          </div>
        </div>
      </div> 
    </div>

    <div class="main-header">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
            <div class="logo">
              <a href="<?=base_url()?>">
                <img src="<?=base_url()?>assets/images/logo.png" alt="logo" style="max-height: 6rem;">
              </a> 
            </div>
          </div>

          <div class="col-lg-7 col-md-6 col-sm-8 col-xs-12 top-search-holder"> 
            <div class="search-area">
              <form action="<?=site_url()?>/p_pencarian">
                <div class="control-group">
                  <ul class="categories-filter animate-dropdown">
                    <li class="dropdown"> 
                      <a class="dropdown-toggle"  data-toggle="dropdown" href="<?=site_url()?>/p_pencarian">
                        <?php if (@$pencarian['kategori'] == NULL || $pencarian['kategori'] == ''): ?>
                         Kategori
                         <?php else: ?>
                          <?php foreach ($kategori as $key => $val): ?>
                            <?php if (@$pencarian['kategori'] && $val['id'] == $pencarian['kategori']){echo wordLimiter($val['kategori'], 1);}?>
                          <?php endforeach ?> 
                        <?php endif ?>
                        <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu" role="menu" >
                        <li class="menu-header">
                          <a role="menuitem" tabindex="-1" href="<?=site_url()?>/p_pencarian?search=<?=@$pencarian['search']?>">Semua Kategori</a>
                        </li>
                        <?php foreach ($kategori as $key => $val): ?>
                          <li class="menu-header">
                            <a role="menuitem" tabindex="-1" href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>&search=<?=@$pencarian['search']?>"><?=$val['kategori']?></a>
                          </li>
                        <?php endforeach ?>
                      </ul>
                    </li>
                  </ul>
                  <input type='hidden' name='kategori' value="<?=@$pencarian['kategori']?>" />
                  <input class="search-field" name="search" placeholder="Search here..." value="<?=@$pencarian['search']?>" />

                  <button class="search-button" type="submit" style="border: none;"></button>
                </div>
              </form>
            </div>
          </div>

          <div class="logo signup-pc">
            <div class="center-flex middle-flex">
              <?php if (!$this->session->userdata('user_id')): ?>
                <a href="<?=site_url()?>/p_signup" style="color: white;">
                  <button class="btn-md btn-yellow"><b>Daftar</b></button>
                </a>
                <a href="<?=site_url()?>/login" style="color: white; padding-left: 1rem;" target="_blank">
                  <button class="btn-md btn-primary"><b>Masuk</b></button>
                </a>
                <?php else :?>
                  <a href="<?=site_url()?>/login" style="color: white; padding-left: 1rem;">
                    <i class="fas fa-user fa-2x" style="padding-left: 1.8rem;"></i>
                    <p>Akun Saya</p>
                  </a>
                <?php endif ?>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div class="header-nav animate-dropdown">
        <div class="container">
          <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
              <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="nav-bg-class">
              <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                <div class="nav-outer">
                  <ul class="nav navbar-nav">
                    <li class="dropdown"> <a href="<?=base_url()?>">Home</a> </li>

                    <?php foreach ($kategori as $key => $val): ?>
                      <?php if (@$val['sub'] != NULL): ?>
                        <li class="dropdown yamm mega-menu"> <a href="<?=base_url()?>" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown"><?=@$val['kategori']?></a>

                          <ul class="dropdown-menu container">
                            <li>
                              <div class="yamm-content ">
                                <div class="row">

                                  <?php foreach (array_chunk($kategori[$key]['sub'], 6) as $v): ?>
                                    <div class="col-xs-12 col-sm-6 col-md-2 col-menu">
                                      <ul class="links">
                                        <?php foreach ($v as $k): ?>
                                          <li><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>&sub=<?=$k['id']?>"><?=@$k['kategori']?></a></li>
                                        <?php endforeach ?>
                                      </ul>
                                    </div>
                                  <?php endforeach ?>

                                </div>
                              </div>
                            </li>
                          </ul>
                        </li>
                        <?php else: ?>
                          <li class="dropdown"> <a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>"><?=@$val['kategori']?> </a> </li>
                        <?php endif ?>
                        <?php if ($key == 7): ?>
                          <?php break;?>
                        <?php endif ?>
                      <?php endforeach ?>
                      <li class="dropdown yamm mega-menu"> <a href="<?=base_url()?>" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">lainnya</a>
                        <ul class="dropdown-menu container">
                          <li>
                            <div class="yamm-content ">
                              <div class="row">
                                <?php foreach ($kategori as $key => $val): ?>
                                  <?php if (@$val['sub'] != NULL && $key >= 8): ?>
                                    <div class="col-xs-12 col-sm-6 col-md-1 col-menu">
                                      <h2 class="title"><?=@$val['kategori']?></h2>
                                      <?php foreach ($kategori[$key]['sub'] as $v): ?>
                                        <ul class="links">
                                          <li><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>&sub=<?=$v['id']?>"><?=@$v['kategori']?></a>
                                          </li>
                                        </ul>
                                      <?php endforeach ?>
                                    </div>
                                  <?php endif ?>
                                <?php endforeach ?>
                                <div class="col-xs-12 col-sm-6 col-md-1 col-menu">
                                  <ul class="links">
                                    <h2 class="title">Kategori Lainnya</h2>
                                    <?php foreach ($kategori as $key => $val): ?>
                                      <?php if (@$val['sub'] == NULL && $key >= 8): ?>
                                        <li><a href="<?=site_url()?>/p_pencarian?kategori=<?=$val['id']?>"><?=@$val['kategori']?></a></li>
                                      <?php endif ?>
                                    <?php endforeach ?>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>