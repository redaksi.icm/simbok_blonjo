<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="keywords" content="MediaCenter, Template, eCommerce">
	<meta name="robots" content="all">
	<title><?=$profile['app']?></title>
	<!-- STYLESHEETS -->
	<link rel="shortcut icon" href="<?=base_url()?>images/logos/<?=$profile['logo']?>" type="image/x-icon">

	<!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">

	<!-- Customizable CSS -->
	<link rel="stylesheet" href="<?=base_url()?>plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="<?=base_url()?>plugins/select2/css/select2.css">
	<link rel="stylesheet" href="<?=base_url()?>plugins/select2-bootstrap4-theme/select2-bootstrap.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/blue.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.carousel.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/owl.transitions.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/animate.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/rateit.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-select.min.css">
	<!-- dropzone -->
	<link rel="stylesheet" href="<?=base_url()?>plugins/dropzone/dropzone.css">
	<!-- Sweetalert2 -->
	<link rel="stylesheet" href="<?=base_url()?>plugins/sweetalert2/sweetalert2.css">
	<link rel="stylesheet" href="<?=base_url()?>plugins/overlayScrollbars/css/OverlayScrollbars.css">

	<!-- Icons/Glyphs -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.css">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Barlow:200,300,300i,400,400i,500,500i,600,700,800" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>


	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script> 
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script> 
	<script src="<?=base_url()?>assets/js/bootstrap-hover-dropdown.min.js"></script> 
	<script src="<?=base_url()?>assets/js/owl.carousel.min.js"></script> 
	<script src="<?=base_url()?>assets/js/echo.min.js"></script> 
	<script src="<?=base_url()?>assets/js/jquery.easing-1.3.min.js"></script> 
	<script src="<?=base_url()?>assets/js/bootstrap-slider.min.js"></script> 
	<script src="<?=base_url()?>assets/js/jquery.rateit.min.js"></script> 
	<script src="<?=base_url()?>assets/js/lightbox.min.js"></script> 
	<script src="<?=base_url()?>assets/js/bootstrap-select.min.js"></script> 
	<script src="<?=base_url()?>assets/js/wow.min.js"></script> 
	<script src="<?=base_url()?>assets/js/scripts.js"></script>
	<script src="<?=base_url()?>plugins/jquery-validation/jquery.validate.js"></script>
	<script src="<?=base_url()?>plugins/jquery-validation/localization/messages_id.js"></script>
	<script src="<?=base_url()?>plugins/select2/js/select2.full.js"></script>
	<script src="<?=base_url()?>plugins/jquery-chained/jquery.chained.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDb3uAj8g901SdVT6zGq-M-GIa-XhLjotM&libraries=places&callback=initAutocomplete&callback=initMap"></script>
	<!-- dropzone -->
	<script src="<?=base_url()?>plugins/dropzone/dropzone.js"></script>
	<!-- Sweetalert2 -->
	<script src="<?=base_url()?>plugins/sweetalert2/sweetalert2.js"></script>
	<!-- autoNumeric -->
	<script src="<?=base_url()?>plugins/autoNumeric/autoNumeric-min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			// $.fn.select2.defaults.set( "theme", "bootstrap" );
			$('.select2').select2();
			$('.select2-hidden').select2({
				minimumResultsForSearch: Infinity
			});
		});
	</script>
	<script>
		function addCommas(nStr)
		{
			nStr += '';
			x = nStr.split('.');
			x1 = x[0];
			x2 = x.length > 1 ? '.' + x[1] : '';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + '.' + '$2');
			}
			return x1 + x2;
		}
		function ToDateId(string) {
			bulanIndo = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September' , 'Oktober', 'November', 'Desember'];

			date = string.split(" ")[0];
			time = string.split(" ")[1];

			tanggal = date.split("-")[2];
			bulan = date.split("-")[1];
			tahun = date.split("-")[0];

			return tanggal + " " + bulanIndo[Math.abs(bulan)];
		}
	</script>
	<style>
		.name-limit, .card-bawah{
			white-space: nowrap;
			overflow: hidden;
			display: block;
			text-overflow: ellipsis;
		}
	</style>
</head>
<body class="cnt-home">