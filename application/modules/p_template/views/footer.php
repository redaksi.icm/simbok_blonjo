<?php if (@$err != TRUE): ?>
  <footer id="footer" class="footer color-bg">
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="address-block">

              <!-- /.module-heading -->

              <div class="module-body">
                <ul class="toggle-footer" style="">
                  <li class="media">
                    <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span> </div>
                    <div class="media-body">
                      <p>Jl. HM Sarbini No. 17, Kebumen, Bumirejo, Kec. Kebumen, Kabupaten Kebumen, Jawa Tengah 54311</p>
                    </div>
                  </li>
                  <li class="media">
                    <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
                    <div class="media-body">
                      <span><a href="tel:<?=@$profile['telp']?>"><?=@$profile['telp']?></a></span>
                    </div>
                  </li>
                  <li class="media">
                    <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
                    <div class="media-body"> <span><a href="mailto:<?=@$profile['email']?>"><?=@$profile['email']?></a></span> </div>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /.module-body --> 
          </div>
          <!-- /.col -->

          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="module-heading">
              <h4 class="module-title">Link Terkait</h4>
            </div>
            <!-- /.module-heading -->

            <div class="module-body">
              <ul class="toggle-footer" style="">
                <li class="media">
                  <div class="pull-left">
                    <img src="<?=base_url()?>assets/images/logo_dins.png" style="height: 40px">
                  </div>
                  <div class="media-body">
                    <span><a href="https://kebumenkab.go.id/" target="_blank">PEMKAB KEBUMEN</a></span>
                  </div>
                </li>
                <li class="media">
                  <div class="pull-left">
                    <img src="<?=base_url()?>assets/images/logo_dins.png" style="height: 40px">
                  </div>
                  <div class="media-body">
                    <span><a href="https://disperindag.kebumenkab.go.id/" target="_blank">DISPERINDAG KEBUMEN</a></span>
                  </div>
                </li>
                <li class="media">
                  <div class="pull-left">
                    <img src="<?=base_url()?>assets/images/simbok.png" style="height: 35px; width: 30px">
                  </div>
                  <div class="media-body">
                    <span><a href="https://simbok.kebumenkab.go.id/" target="_blank">SIMBOK</a></span>
                  </div>
                </li>
              </ul>
            </div>
            <!-- /.module-body --> 
          </div>
          <!-- /.col -->

          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="module-heading">
              <h4 class="module-title">Survey Kepuasan Masyarakat</h4>
            </div>
            <!-- /.module-heading -->

            <div class="module-body">
              <ul class="toggle-footer" style="">
                <li class="media">
                  <div class="pull-left">
                    <img src="<?=base_url()?>assets/images/emot/in-love.png" style="height: 40px">
                  </div>
                  <div class="media-body">
                    <span class="survei" data-id="1"><a href="#" target="_blank">Sangat Puas</a></span>
                  </div>
                </li>
                <li class="media">
                  <div class="pull-left">
                    <img src="<?=base_url()?>assets/images/emot/happy.png" style="height: 40px">
                  </div>
                  <div class="media-body">
                    <span class="survei" data-id="2"><a href="https://disperindag.kebumenkab.go.id/" target="_blank">Puas</a></span>
                  </div>
                </li>
                <li class="media">
                  <div class="pull-left">
                    <img src="<?=base_url()?>assets/images/emot/sad.png" style="height: 40px">
                  </div>
                  <div class="media-body">
                    <span class="survei" data-id="3"><a href="https://simbok.kebumenkab.go.id/" target="_blank">Kurang Puas</a></span>
                  </div>
                </li>
              </ul>
            </div>
            <!-- /.module-body --> 
          </div>
          <!-- /.col -->

          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="module-heading">
              <h4 class="module-title">Statistik Pengunjung</h4>
            </div>
            <!-- /.module-heading -->

            <div class="module-body">
              <table class="list-unstyled" width="100%" style="font-weight: bold; color: #abafb1;">
                <tr>
                  <td width="40%">Hari Ini</td>
                  <td width="60%">: <?=numSys($statistic['today'])?></td>
                </tr>
                <tr>
                  <td>Kemarin</td>
                  <td>: <?=numSys($statistic['yesterday'])?></td>
                </tr>
                <tr>
                  <td>Bulan Ini</td>
                  <td>: <?=numSys($statistic['month'])?></td>
                </tr>
                <tr>
                  <td>Tahun Ini</td>
                  <td>: <?=numSys($statistic['year'])?></td>
                </tr>
                <tr>
                  <td>Semua Waktu</td>
                  <td>: <?=numSys($statistic['alltime'])?></td>
                </tr>
              </table>
            </div>
            <!-- /.module-body --> 
          </div>
        </div>
      </div>
    </div>
    <div class="copyright-bar">
      <div class="container">
        <div class="col-xs-12 col-sm-4 no-padding social">
        </div>
        <div class="col-xs-12 col-sm-4 no-padding copyright"><a href="<?=base_url()?>">Copyright  &copy; Dinas Perindustrian dan Perdagangan Kab. Kebumen</a></div>
        <div class="col-xs-12 col-sm-4 no-padding">
          <div class="clearfix payment-methods">
          </div>
          <!-- /.payment-methods --> 
        </div>
      </div>
    </div>
  </footer>
<?php endif ?>
<?php $this->load->view('p_template/js')?>
<!-- ============================================================= FOOTER : END============================================================= --> 
