<script>
	$('.survei').on('click', function(e) {
		e.preventDefault();
		var	survei = $(this).data("id");
		// alert(survei);
		if (survei != 3) {	
			Swal.fire({
				title: 'Memproses Survei!',
				onBeforeOpen: () => {
					Swal.showLoading()
				},
			});
			$.ajax({
				type : 'post',
				url : '<?=site_url('survei/ins')?>',
				dataType : 'json',
				data : {survei_id:survei},
				success : function (data) {
					console.log(data);
					if (data == true) {
						Swal.fire(
							'Berhasil!',
							'Survei Berhasil Di Rekam.',
							'success'
							)
					}else{
						Swal.fire(
							'Gagal!',
							'Mungkin Anda Sudah Survei.',
							'error'
							)
					}
				}
			})
		}else{
			Swal.fire({
				title: 'Apa yang membuat anda kurang puas?',
				input: 'textarea',
				inputAttributes: {
					autocapitalize: 'off'
				},
				showCancelButton: true,
				confirmButtonText: 'Kirim',
			}).then((result) => {
				$.ajax({
					type : 'post',
					url : '<?=site_url('survei/ins')?>',
					dataType : 'json',
					data : {survei_id:survei, msg:result.value},
					success : function (data) {
						console.log(data);
						if (data == true) {
							Swal.fire(
								'Berhasil!',
								'Survei Berhasil Di Rekam.',
								'success'
								)
						}else{
							Swal.fire(
								'Gagal!',
								'Mungkin Anda Sudah Survei.',
								'error'
								)
						}
					}
				})
			})
		}
	})
</script>