<script>
  $(document).ready(function () {
    $("#kelurahan").chained("#kecamatan");
    $("#form").validate( {
      rules: {
        repassword : {
          equalTo : "#password"
        }
      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
      }
    });
  })
</script>
<script type="text/javascript">
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
</script>
<script type="text/javascript">
  Dropzone.autoDiscover = false;

  var profil= new Dropzone(".profil",{
    url: '<?=site_url().'/'.$menu['controller']?>/upload/',
    maxFiles: 1,
    maxFilesize: 10,
    method:"post",
    acceptedFiles:"image/*",
    paramName:"img",
    dictInvalidFileType:"Type file ini tidak dizinkan",
    addRemoveLinks:true,
    init: function() {
      this.on("thumbnail", function(file) {
        if (file.compressed) {
          return;
        }
        const imageCompressor = new ImageCompressor();
        imageCompressor.compress(file, {
          checkOrientation: true,
          quality: 0.6,
          convertSize: 0,
        })
        .then((result) => {
          result.compressed = true;
          this.removeFile(file);
          this.addFile(result);
        })
      });
    },
    autoProcessQueue: false
  });

  profil.on("removedfile", function (file) {
    $.ajax({
      type: 'POST',
      url: '<?=site_url().'/'.$menu['controller']?>/del_img/',
      data: {id: '<?=@$main['id_user']?>', name: file.name},
      dataType: 'json',
      success: function(hasil){
        if (hasil.status = true) {
          Toast.fire({
            icon: 'success',
            title: 'Gambar berhasil dihapus!'
          });
        }
      }
    });
  });

  $(".btn-submit").click(function (e) {
    var formId = '<?=@$main['id']?>';
    if (profil.files == '') {
      form.submit();
    }else{
      profil.processQueue();
      profil.on("complete", function (fileu) {
        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
          form.submit();
        }
      });
    }
  });

  function showimg() {
    var mockFile = { name: '<?=@$main['photo']?>'};
    profil.options.addedfile.call(profil, mockFile);
    profil.options.thumbnail.call(profil, mockFile, "<?=base_url()?>images/users/"+mockFile.name);
  }
  var img = '<?=@$main['photo']?>';
  if (img != '') {
    showimg();
  }
</script>