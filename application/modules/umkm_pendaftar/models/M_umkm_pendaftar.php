<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_umkm_pendaftar extends CI_Model {

  public function list_data($data, $riq = 0)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = ".$riq." ";
    if ($data['search']['term'] != '') {
      $where .= "AND a.nama_market LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT a.*, b.user_name FROM tb_umkm a LEFT JOIN _user b ON a.id_user = b.id
    $where
    ORDER BY a."
    .$data['order']['field']." ".$data['order']['type'].
    " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM tb_umkm a LEFT JOIN _user b ON a.id_user = b.id $where ORDER BY a.created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($riq = 0)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = ".$riq."";

    $sql = "SELECT COUNT(1) as total FROM tb_umkm a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND a.$field='".$val."' ";
    $sql = "SELECT a.*, b.user_name, b.photo FROM tb_umkm a LEFT JOIN _user b ON a.id_user = b.id $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('tb_umkm', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('tb_umkm', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('tb_umkm');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('tb_umkm', $data);
    }
  }

  public function umkm_cek($data)
  {
    return $this->db->get_where('tb_umkm',['id' => $data]);
  }
  public function user_cek($data)
  {
    return $this->db->get_where('_user',['id' => $data]);
  }
  public function get_umkm($id_user)
  {
    return $this->db->get_where('tb_umkm',['id_user' => $id_user])->result_array();
  }
}