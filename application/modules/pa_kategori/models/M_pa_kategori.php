<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pa_kategori extends CI_Model {

  public function list_data($data)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($data['search']['term'] != '') {
      $where .= "AND a.kategori LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT * FROM pa_kategori a 
    $where
    ORDER BY "
    .$data['order']['field']." ".$data['order']['type'].
    " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 ";
    $sql = "SELECT * FROM pa_kategori a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT COUNT(1) as total FROM pa_kategori a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM pa_kategori a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('pa_kategori', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('pa_kategori', $data);
    $this->db->where('parent_id',$id)->update('pa_kategori', array('parent_id' => $data['id']));
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('pa_kategori');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('pa_kategori', $data);
    }
  }

  public function get_last_id($id)
  {
    $where = "WHERE a.parent_id = ".$id."";

    $sql = "SELECT * FROM pa_kategori a $where ORDER BY a.id DESC";
    $query = $this->db->query($sql);
    return $query->row();
  }

  public function get_parent()
  {
    return $this->db->get_where('pa_kategori', ['parent_id' => ''])->result_array();
  }
  
  public function get_sub_kategori()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND a.parent_id != '' ";

    $sql = "SELECT * FROM pa_kategori a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function get_induk_kategori()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND a.parent_id = '' ";

    $sql = "SELECT * FROM pa_kategori a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function get_kategori($parent = '')
    {
      $kategori = $this->db->get_where('pa_kategori', ['parent_id' => $parent, 'is_active' => 1])->result_array();
      foreach ($kategori as $k => $v) {
        $kategori[$k]['sub'] = $this->get_kategori($kategori[$k]['id']);
      }
      return $kategori;
    }  
}