<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pa_kategori extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      'pa_kategori/m_pa_kategori'
    ));

    $this->controller = 'pa_kategori';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 999;
    if ($this->cookie['cur_page'] == null) 0;
	}

  public function index()
  {
    //auth
    if ($this->menu['_read'] == 0) redirect(site_url().'/_error/error_403');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(3, 0);
    $this->cookie['total_rows'] = $this->m_pa_kategori->all_rows();
    setCookieMenu($this->controller, $this->cookie);
    //main data
    $data['menu'] = $this->menu;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_pa_kategori->list_data($this->cookie);
    $data['pagination_info'] = paginationInfo(count($data['main']), $this->cookie);
    //set pagination
    setPagination($this->controller, $this->cookie);
    //render
    createLog(1,$this->controller);
    $this->render('index',$data);
  }

  public function form($id = null)
  {
    if ($id == null) {
      //auth
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      createLog(2,$this->controller);
      $data['main'] = null;
    }else{
      //auth
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      createLog(3,$this->controller);
      $data['main'] = $this->m_pa_kategori->by_field('id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['paren'] = $this->m_pa_kategori->get_parent();
    $this->render('form',$data);
  }

  public function save($id = null)
  {
    $data = html_remover($this->input->post(null,true));
    if(!isset($data['is_active'])){$data['is_active'] = 0;}
    $cek = $this->m_pa_kategori->by_field('id', $data['id']);
    if ($id == null) {
      if (@$data['parent_id']) {
        $get_id = $this->m_pa_kategori->get_last_id($data['parent_id']);
        if ($get_id != null) {
          $data['id'] = (float)$get_id->id+0.1;
        }else{
          $data['id'] = (float)$data['parent_id'].'.1';
        }
      }

      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url().'/'.$this->controller.'/form/');
      }
      $this->m_pa_kategori->store($data);
      createLog(2, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    }else{
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      if ($data['old'] != $data['id'] && $cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url().'/'.$this->controller.'/form/'.$id);
      }
      unset($data['old']);
      var_dump($data);
      $this->m_pa_kategori->update($id,$data);
      createLog(3, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    if ($this->menu['_delete'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    $this->m_pa_kategori->delete($id);
    createLog(4, $this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function status($type = null, $id = null)
  {
    if ($this->menu['_update'] == 0 || $type == null || $id == null) redirect(site_url().'/_error/error_403');
    if ($type == 'enable') {
      $this->m_pa_kategori->update($id, array('is_active' => 1));
    }else{
      $this->m_pa_kategori->update($id, array('is_active' => 0));
    }
    createLog(3,$this->this->menu['menu']);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function multiple($type = null)
	{
    if ($this->menu['_update'] == 0 || $this->menu['_delete'] == 0) redirect(site_url().'/_error/error_403');
    $data = $this->input->post(null,true);
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
          case 'delete':
            $this->m_pa_kategori->delete($key);
            $flash = 'Data berhasil dihapus.';
            $t = 4;
            break;

          case 'enable':
            $this->m_pa_kategori->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            $t = 3;
            break;

          case 'disable':
            $this->m_pa_kategori->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            $t = 3;
            break;
        }
      }
    }
    createLog($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
	}
	
}