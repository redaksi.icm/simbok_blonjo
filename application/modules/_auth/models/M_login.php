<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

  public function action($data)
  {
    return $this->db->get_where('_user',$data);
  }

  public function market_byUser($id)
  {
  	$this->db->select('*');
    $this->db->from('tb_umkm a');
    $this->db->where('a.id_user', $id);
    $query = $this->db->get();
    return $query->row_array();
  }
  
}