<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array(
			'm_login',
			'_user/m_user',
			'_profile/m_profile',
			'_config/m_config',
		));
		//check if user has logged before.
		if($this->session->userdata('logged')){
			if ($this->session->userdata('locked') == true) {redirect(site_url().'/_locked/');}
			$menu = $this->m_config->get_first_menu($this->session->userdata('group_id'));
			redirect(site_url().'/'.$menu->controller);
		}else{
			//check if client browser has cookie.
			$uid = get_cookie('uid');
			if ($uid != null) {
				$user = $this->m_user->by_field('id', $uid);
				$umkm = $this->m_login->market_byUser($user['id']);
				$sess['user_id'] = $user['id'];
				$sess['umkm_id'] = $umkm['id'];
				$sess['group_id'] = $user['group_id'];
				$sess['photo'] = $user['photo'];
				$sess['fullname'] = $user['fullname'];
				$sess['logged'] = true;
				$sess['locked'] = false;

				$this->session->set_userdata($sess);

				$menu = $this->m_config->get_first_menu($sess['group_id']);
				redirect(site_url().'/'.$menu->controller);
			}
		}
	}

	public function index()
	{
		$data['profile'] = $this->m_profile->get_first();
		
		createLog(1, 'Login');
		$this->load->view('login', $data);
	}
	
	public function action()
	{
		$data = $this->input->post(null,true);
		$remember = @$data['remember'];

		unset($data['remember']);
		if ($data != null) {
			$data['user_password'] = md5(md5(md5($data['user_password'])));
			$result = $this->m_login->action($data);
			$valid = $result->row();

			if($result->num_rows() >= 1){
				if ($valid->is_active == 0) {
					createLog(7,'Login');
					$this->session->set_flashdata('flash_error', 'User Belum Atau Tidak Di Verifikasi.');
					redirect(site_url().'/_auth/login');
				}
				foreach ($result->result() as $sess) {
					$sess_data['user_id'] = $sess->id;
					$sess_data['group_id'] = $sess->group_id;
					$sess_data['photo'] = $sess->photo;
					$sess_data['fullname'] = $sess->fullname;
					$sess_data['logged'] = true;
					$sess_data['locked'] = false;
				}

				$this->session->set_userdata($sess_data);
				$umkm = $this->m_login->market_byUser($this->session->userdata('user_id'));
				$this->session->set_userdata(['umkm_id' => $umkm['id']]);
				// var_dump($umkm); die();
				if (@$remember == 1) {
					$cookie = array(
						'name'   => 'uid',
						'value'  => $sess_data['user_id'],
						'expire' => '3600'
					);
					$this->input->set_cookie($cookie);
				}
				$this->input->set_cookie($cookie);

				createLog(6, 'Login');
				$menu = $this->m_config->get_first_menu($sess->group_id);
				redirect(site_url().'/'.$menu->controller);
			}else{
				createLog(7,'Login');
				$this->session->set_flashdata('flash_error', 'Nama Pengguna dan Kata Sandi tidak cocok.');
				redirect(site_url().'/_auth/login');
			}
		}else{
			redirect(site_url().'/_error/error_403');
		}
	}

}
