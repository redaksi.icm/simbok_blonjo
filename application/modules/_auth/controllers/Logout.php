<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {

	public function action()
	{
		createLog(8,'Logout');
		$this->session->sess_destroy();
		delete_cookie('uid');
		redirect(site_url().'/_auth/login');
	}
  
}
