<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _profile extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      '_profile/m_profile'
    ));

    $this->controller = '_profile';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
  }

  public function form()
  {
    if ($this->menu['_read'] == 0) redirect(site_url().'/_error/error_403');
    createLog(3,$this->controller);
    $data['main'] = $this->m_profile->get_first();
    $data['menu'] = $this->menu;
    $this->render('form',$data);
  }

  public function save($id = null)
  {
    $data = html_remover($this->input->post(null,true));
    if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
    if (!empty($_FILES["logo"]["name"])) {
     $data['logo'] = $this->_upload();
     unlink('./images/logos/'.$data["logo_old"]);
   } else {
     $data['logo'] = $data["logo_old"];
   }
   unset($data['logo_old']);
   $this->m_profile->update($data);
   createLog(3, $this->menu['menu']);
   $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
   redirect(site_url().'/'.$this->controller.'/form/');
 }

 private function _upload()
 {
  $config['upload_path']          = './images/logos/';
  $config['allowed_types']        = 'gif|jpg|png|jpeg';
  $config['overwrite']			      = true;
    $config['max_size']             = 10240; // 10MB
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);
    if ($this->upload->do_upload('logo')) return $this->upload->data("file_name");
    
    return "no-image.png";
  }

}