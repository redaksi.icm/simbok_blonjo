<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_profile extends CI_Model {

	public function get_first()
  {
    return $this->db->get('_profile')->row_array();
  }

  public function update($data)
  {
    $this->db->update('_profile', $data);
  }

}
