<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_group extends CI_Model {

  public function list_data($data)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($data['search']['term'] != '') {
      $where .= "AND a.group LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
      $where .= "OR a.description LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT * FROM _group a 
      $where
      ORDER BY '"
        .$data['order']['field']."' ".$data['order']['type'].
      " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM _group a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT COUNT(1) as total FROM _group a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM _group a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('_group', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('_group', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('_group');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('_group', $data);
    }
  }

  public function detail_data($id)
  {
    $sql = "SELECT 
      a.id,a.parent_id,a.menu,a.type,a.controller,a.icon,a.url,
      a.is_read,a.is_create,a.is_update,a.is_delete,a.is_report,
      b._read,b._create,b._update,b._delete,b._report
      FROM _menu a 
      LEFT JOIN _group_menu b ON b.menu_id = a.id AND b.group_id = '$id' 
      ORDER BY a.id";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function authorize($data)
  {
    //delete all data
    $this->db->where('group_id', $data['id'])->delete('_group_menu');
    //insert new data
    foreach ($data['_read'] as $r) {
      $d = array(
        'group_id' => $data['id'],
        'menu_id' => $r,
        '_read' => 1
      );
      $this->db->insert('_group_menu', $d);
    }
    foreach (@$data['_create'] as $r) {
      $this->db
        ->where('menu_id', $r)
        ->where('group_id', $data['id'])
        ->update('_group_menu', array('_create' => 1));
    }
    foreach (@$data['_update'] as $r) {
      $this->db
        ->where('menu_id', $r)
        ->where('group_id', $data['id'])
        ->update('_group_menu', array('_update' => 1));
    }
    foreach (@$data['_delete'] as $r) {
      $this->db
        ->where('menu_id', $r)
        ->where('group_id', $data['id'])
        ->update('_group_menu', array('_delete' => 1));
    }
    foreach (@$data['_report'] as $r) {
      $this->db
        ->where('menu_id', $r)
        ->where('group_id', $data['id'])
        ->update('_group_menu', array('_report' => 1));
    }
  }
  
}