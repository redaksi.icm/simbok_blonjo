<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengaturan</li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
            <li class="breadcrumb-item active"><?php if($id == null){echo 'Tambah';}else{echo 'Ubah';}?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-md-12">
          <div class="card">
            <form id="form" action="<?=site_url().'/'.$menu['controller'].'/authorize/'.$id?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?=$this->session->flashdata('flash_error')?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="id" id="id" value="<?=@$main['id']?>" required>
                <div id="controller-container" class="form-group row">
                  <label for="controller" class="col-sm-2 col-form-label text-right">Group <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="group" id="group" value="<?=@$main['group']?>" required readonly>
                  </div>
                </div>
                <div id="url-container" class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Deskripsi <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm" name="description" id="description" value="<?=@$main['description']?>" required readonly>
                  </div>
                </div>
                <div class="row">
                  <table class="table table-bordered table-sm">
                    <thead>
                      <tr>
                        <th class="text-center" width="70">Kode</th>
                        <th class="text-center">Menu</th>
                        <th class="text-center" class="text-center" width="50"><i class="fas fa-eye"></i></th>
                        <th class="text-center" class="text-center" width="50"><i class="fas fa-plus-circle"></i></th>
                        <th class="text-center" class="text-center" width="50"><i class="fas fa-pencil-alt"></i></th>
                        <th class="text-center" class="text-center" width="50"><i class="fas fa-trash-alt"></i></th>
                        <th class="text-center" class="text-center" width="50"><i class="fas fa-copy"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                    <form id="form-multiple" action="" method="post">
                      <?php foreach($detail as $r):?>
                        <tr>
                          <td><?=$r['id']?></td>
                          <td>
                            <?php 
                              $div = intval(strlen($r['id']) / 5);
                              $strip = '';
                              $margin = 0;
                              if ($div != 0) {
                                $strip .= '|--';
                                $l = strlen($r['id']) - 5;
                                for ($j=0; $j < $l; $j++) { 
                                  $margin += 5;
                                }
                              }
                            ?>
                            <span style="margin-left: <?=$margin?>px"></span><?=$strip." ".$r['menu']?>
                          </td>
                          <td class="text-center">
                            <?php if($r['is_read'] == 1):?>
                              <div class="pretty p-icon">
                                <input class="read_<?=str_replace('.','',$r['id'])?>" type="checkbox" data-id="<?=str_replace('.','',$r['id'])?>" value="<?=$r['id']?>" name="_read[]" onclick="checkAll(this);" <?php if($r['_read'] == 1){echo 'checked';}?>/>
                                <div class="state">
                                  <i class="icon fas fa-check"></i><label></label>
                                </div>
                              </div>
                            <?php endif;?>
                          </td>
                          <td class="text-center">
                            <?php if($r['is_create'] == 1):?>
                              <div class="pretty p-icon">
                                <input class="create_<?=str_replace('.','',$r['id'])?>" type="checkbox" data-id="<?=str_replace('.','',$r['id'])?>" value="<?=$r['id']?>" name="_create[]" onclick="checkRead(this);" <?php if($r['_create'] == 1){echo 'checked';}?>/>
                                <div class="state">
                                  <i class="icon fas fa-check"></i><label></label>
                                </div>
                              </div>
                            <?php endif;?>
                          </td>
                          <td class="text-center">
                            <?php if($r['is_update'] == 1):?>
                              <div class="pretty p-icon">
                                <input class="update_<?=str_replace('.','',$r['id'])?>" type="checkbox" data-id="<?=str_replace('.','',$r['id'])?>" value="<?=$r['id']?>" name="_update[]" onclick="checkRead(this);" <?php if($r['_update'] == 1){echo 'checked';}?>/>
                                <div class="state">
                                  <i class="icon fas fa-check"></i><label></label>
                                </div>
                              </div>
                            <?php endif;?>
                          </td>
                          <td class="text-center">
                            <?php if($r['is_delete'] == 1):?>
                              <div class="pretty p-icon">
                                <input class="delete_<?=str_replace('.','',$r['id'])?>" type="checkbox" data-id="<?=str_replace('.','',$r['id'])?>" value="<?=$r['id']?>" name="_delete[]" onclick="checkRead(this);" <?php if($r['_delete'] == 1){echo 'checked';}?>/>
                                <div class="state">
                                  <i class="icon fas fa-check"></i><label></label>
                                </div>
                              </div>
                            <?php endif;?>
                          </td>
                          <td class="text-center">
                            <?php if($r['is_report'] == 1):?>
                              <div class="pretty p-icon">
                                <input class="report_<?=str_replace('.','',$r['id'])?>" type="checkbox" data-id="<?=str_replace('.','',$r['id'])?>" value="<?=$r['id']?>" name="_report[]" onclick="checkRead(this);" <?php if($r['_report'] == 1){echo 'checked';}?>/>
                                <div class="state">
                                  <i class="icon fas fa-check"></i><label></label>
                                </div>
                              </div>
                            <?php endif;?>
                          </td>
                        </tr>
                      <?php endforeach;?>
                    </form>
                  </tbody>
                  </table>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?=site_url().'/'.$menu['controller']?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    $("#form").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })

  function checkAll(obj) {
    if (obj.checked) {
      $('.create_'+$(obj).data('id')).prop('checked', true);
      $('.update_'+$(obj).data('id')).prop('checked', true);
      $('.delete_'+$(obj).data('id')).prop('checked', true);
      $('.report_'+$(obj).data('id')).prop('checked', true);
    }else{
      $('.create_'+$(obj).data('id')).prop('checked', false);
      $('.update_'+$(obj).data('id')).prop('checked', false);
      $('.delete_'+$(obj).data('id')).prop('checked', false);
      $('.report_'+$(obj).data('id')).prop('checked', false);
    }
  }

  function checkRead(obj) {
    if (obj.checked) {
      $('.read_'+$(obj).data('id')).prop('checked', true);
    }
  }
</script>