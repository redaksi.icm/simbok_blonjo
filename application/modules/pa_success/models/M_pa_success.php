<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pa_success extends CI_Model {
  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM pa_page a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('pa_page', $data);
  }
}