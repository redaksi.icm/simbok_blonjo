<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola Aplikasi</li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-md-12">
          <div class="card">
            <form id="form" action="<?=site_url().'/'.$menu['controller'].'/save/'.$main['id']?>" method="post">
              <div class="card-body">
                <textarea id="summernote" name="deskripsi" required><?=@$main['deskripsi']?></textarea>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-sm btn-block btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div><!-- /.container-fluid -->
      </div><!-- /.content -->
    </div>
  </div>

</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    $('#summernote').summernote({
      height: 350,
      toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['insert', ['link']],
      ['view', ['help']]
      ]
    });
  })
</script>
