<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengaturan</li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
            <li class="breadcrumb-item active"><?php if($id == null){echo 'Tambah';}else{echo 'Ubah';}?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-md-12">
          <div class="card">
            <form id="form" action="<?=site_url().'/'.$menu['controller'].'/save/'.$id?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?=$this->session->flashdata('flash_error')?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="id" id="id" value="<?=@$main['id']?>" required>
                <?php if($id != null):?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?=@$main['parameter']?>" required>
                <?php endif;?>
                <div class="form-group row">
                  <label for="controller" class="col-sm-2 col-form-label text-right">Parameter <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="parameter" id="parameter" value="<?=@$main['parameter']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="controller" class="col-sm-2 col-form-label text-right">Nilai <span class="text-danger">*</span></label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control form-control-sm" name="value" id="value" value="<?=@$main['value']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Deskripsi <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm" name="description" id="description" value="<?=@$main['description']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Aktif</label>
                  <div class="col-sm-3">
                    <div class="pretty p-icon">
                      <input class="icheckbox" type="checkbox" name="is_active" id="is_active" value="1" <?php if(@$main['is_active'] == 1){echo 'checked';}else{echo 'checked';} ?>>
                      <div class="state">
                        <i class="icon fas fa-check"></i><label></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?=site_url().'/'.$menu['controller']?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    $("#form").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>