<div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
<div class="flash_error" data-flashsuccess="<?=$this->session->flashdata('flash_error')?>"></div>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><i class="<?=@$menu['icon']?>"></i> <?=@$menu['menu']?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengelola UMKM</li>
            <li class="breadcrumb-item active"><?=@$menu['menu']?></li>
            <li class="breadcrumb-item active"><?php if($id == null){echo 'Tambah';}else{echo 'Ubah';}?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">       
        <div class="col-md-12">

          <div class="card">
            <div class="card-body">
              <div class="form-group row">
                <label for="status" class="col-sm-3 col-form-label text-right" style="font-size: 20px;">Setatus Market/Toko :</label>
                <div class="col-sm-3">
                  <?= $main['is_close'] == 1 ? '<label class="col-form-label" style="color: green; font-size: 20px;">Buka</label>' : '<label class="col-form-label" style="color: red; font-size: 20px;">Tutup</label>'?>
                </div>
                <div class="col text-right" style="margin-top: 0.7em;">
                  <?= @$main['is_close'] == 1 ? '<a href="'.site_url().'/'.$menu['controller'].'/status/close/'.$main['id'].'"><button class="btn btn-danger btn-sm">Tutup Market/Toko</button></a>' : '<a href="'.site_url().'/'.$menu['controller'].'/status/open/'.$main['id'].'"><button class="btn btn-success btn-sm">Buka Market/Toko</button></a>'?>
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <form id="form" action="<?=site_url().'/'.$menu['controller'].'/save/'?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?=$this->session->flashdata('flash_error')?>"></div>
                <input type="hidden" class="form-control form-control-sm" name="id" id="id" value="<?=@$main['id']?>" required>
                <?php if($id != null):?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?=@$main['nama_market']?>" required>
                <?php endif;?>
                <div class="form-group row">
                  <label for="controller" class="col-sm-2 col-form-label text-right">Nama Market <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" name="nama_market" id="parameter" value="<?=@$main['nama_market']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="controller" class="col-sm-2 col-form-label text-right">Nama Pemilik <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control form-control-sm" name="nama_pemilik" id="nama_pemilik" value="<?=@$main['nama_pemilik']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">NIK/No.KTP <span class="text-danger">*</span></label>
                  <div class="col-sm-4">
                    <input type="number" class="form-control form-control-sm" name="nik" id="nik" value="<?=@$main['nik']?>" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Foto KTP</label>
                  <div class="col-sm-4">
                    <?php if (@$main['ktp']): ?>
                      <img id="ktp" src="<?=base_url()?>/images/ktp/<?=@$main['ktp']?>" data-toggle="modal" data-target="#staticBackdrop" style="height: 100px; width: 200px;">
                      <?php else: ?>
                        <p class="mt-2">Foto Belum Upload</p>
                      <?php endif ?>
                    </div>
                  </div>

                  <div class="form-group row mt-4">
                    <label for="url" class="col-sm-2 col-form-label text-right">Username <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <?php if (@$main['user_name'] != NULL): ?>
                        <input type="hidden" name="old_username" id="old_username" value="<?=$main['user_name']?>">
                        <input type="hidden" name="id_username" id="id_username" value="<?=@$main['id_user']?>">
                      <?php endif ?>
                      <input type="text" class="form-control form-control-sm" name="username" id="username" value="<?=@$main['user_name']?>" required>
                    </div>
                  </div>
                  <?php if ($id == null): ?>

                    <div class="form-group row">
                      <label for="url" class="col-sm-2 col-form-label text-right">Password <span class="text-danger">*</span></label>
                      <div class="col-sm-4">
                        <input type="password" class="form-control form-control-sm" name="password" id="password" required>
                      </div>
                    </div>

                    <div class="form-group row mb-4">
                      <label for="url" class="col-sm-2 col-form-label text-right">Repassword <span class="text-danger">*</span></label>
                      <div class="col-sm-4">
                        <input type="password" class="form-control form-control-sm" name="repassword" id="repassword" required>
                      </div>
                    </div>
                  <?php endif ?>  

                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">No. Telpon <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" name="no_hp" id="no_hp" value="<?=@Tlp($main['no_hp'])?>" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">No. WhatsApp <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" name="wa" id="wa" value="<?=@Tlp($main['wa'])?>" required>
                    </div>
                  </div>

                  <div class="form-group row mb-4">
                    <label for="url" class="col-sm-2 col-form-label text-right">Email <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control form-control-sm" name="email" id="email" value="<?=@$main['email']?>" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Legalitas Market</label>
                    <div class="col-sm-4">
                      <textarea class="form-control" name="legalitas"><?=@$main['legalitas']?></textarea>
                    </div>
                  </div>

                  <div id="parent-container" class="form-group row">
                    <label for="parent_id" class="col-sm-2 col-form-label text-right form-label">kecamatan <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <select class="form-control form-control-sm select2" name="kecamatan" id="kecamatan" required>
                        <option value="">-- Pilih kecamatan</option>
                        <?php foreach($kecamatan as $r):?>
                          <option id="<?=$r['kecamatan']?>" value="<?=$r['kecamatan']?>" <?php if(@$main['kecamatan'] == $r['kecamatan']){echo 'selected';}?>><?=$r['kecamatan']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>

                  <div id="parent-container" class="form-group row">
                    <label for="parent_id" class="col-sm-2 col-form-label text-right form-label">Kelurahan <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <select class="form-control form-control-sm select2" name="kelurahan" id="kelurahan" required>
                        <option value="">-- Pilih Kelurahan</option>
                        <?php foreach($kelurahan as $r):?>
                          <option value="<?=$r['kelurahan']?>" <?php if(@$main['kelurahan'] == $r['kelurahan']){echo 'selected';}?> data-chained="<?=$r['kecamatan']?>"><?=$r['kelurahan']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row mb-4">
                    <label for="url" class="col-sm-2 col-form-label text-right">Alamat Lengkap <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <textarea class="form-control" name="alamat_lengkap" required><?=@$main['alamat_lengkap']?></textarea>
                    </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                      <label for="url" class="col-md-2 col-form-label text-right"></label>

                      <div class="form-group col-md-6">
                        <input id="pac-input" class="controls form-control form-control-sm mt-3 map-src" type="text" placeholder="Search Box" style="width: 50%">
                        <div id="map" style="width:100%;height: 400px;"></div>
                      </div>

                      <input type="hidden" name="ordinat_s" id="latitude" class="form-control input-sm" value="<?=@$main['ordinat_s']?>">
                      <input type="hidden" name="ordinat_e" id="longitude" class="form-control input-sm" value="<?=@$main['ordinat_e']?>">
                      <input type="hidden" name="latdegree" id="latdegree" class="form-control input-sm" value="<?=@$main['latdegree']?>">
                      <input type="hidden" name="latminute" id="latminute" class="form-control input-sm" value="<?=@$main['latminute']?>">
                      <input type="hidden" name="latsecond" id="latsecond" class="form-control input-sm" value="<?=@$main['latsecond']?>">
                      <input type="hidden" name="lngdegree" id="lngdegree" class="form-control input-sm" value="<?=@$main['lngdegree']?>">
                      <input type="hidden" name="lngminute" id="lngminute" class="form-control input-sm" value="<?=@$main['lngminute']?>">
                      <input type="hidden" name="lngsecond" id="lngsecond" class="form-control input-sm" value="<?=@$main['lngsecond']?>">


                    </div>
                  </div>

                  <div class="form-group row mb-3 mt-3">
                    <label for="url" class="col-sm-2 col-form-label text-right form-label">Foto Profil Market</label>
                    <div class="col-md-2 col-lg-2 col-6 col-sm-3 col-xs-3">
                      <div class="dropzone profil">
                        <div class="dz-message">
                          <i class="fas fa-upload fa-4x"></i>
                          <br>
                          <span>Unggah File</span>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-10 offset-md-2">
                      <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </div>

              </form>
            </div>

            <div class="card">
              <form id="form_password" action="<?=site_url().'/'.$menu['controller'].'/password_change/'?>" method="post" autocomplete="off">
                <div class="card-header">
                  <h3 class="card-title">Ganti Kata Sandi</h3>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Kata Sandi <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <input type="password" class="form-control form-control-sm" name="user_password" id="user_password" value="" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="url" class="col-sm-2 col-form-label text-right">Ulang Kata Sandi <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <input type="password" class="form-control form-control-sm" name="user_password_confirm" id="user_password_confirm" value="" required>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-10 offset-md-2">
                      <button type="submit" class="btn btn-sm btn-primary btn-subm"><i class="fas fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div><!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Foto KTP</h5>
        </div>
        <div class="modal-body">
          <img id="ktp" src="<?=base_url()?>/images/ktp/<?=@$main['ktp']?>" style="width: 100%">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('js');?>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDb3uAj8g901SdVT6zGq-M-GIa-XhLjotM&libraries=places&callback=initAutocomplete&callback=initMap"></script>