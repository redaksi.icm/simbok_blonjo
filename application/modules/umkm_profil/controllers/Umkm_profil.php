<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Umkm_profil extends MY_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    
    $this->load->model(array(
      '_config/m_config',
      '_user/m_user',
      'p_signup/mp_signup',
      'umkm_pendaftar/m_umkm_pendaftar',
      'umkm_produk/m_umkm_produk'
    ));

    $this->controller = 'umkm_profil';
    $this->menu = $this->m_config->get_menu($this->session->userdata('group_id'),$this->controller);
    if ($this->menu == null) redirect(site_url().'/_error/error_403');
    
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'nama_market','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    $id = $this->session->userdata('user_id');
    if ($id == null) {
      //auth
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      createLog(2,$this->controller);
      $data['main'] = null;
    }else{
      //auth
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      createLog(3,$this->controller);
      $data['main'] = $this->m_umkm_pendaftar->by_field('id_user', $id);
    }
    // echo json_encode($data['main']['user_name']); die();
    // maps : init
    $this->load->library('googlemaps');
    $config['center']   = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $config['zoom']   = '15';
    $this->googlemaps->initialize($config);
        // maps : marker
    $marker = array();
    $marker['position'] = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $marker['infowindow_content'] = ''.@$data['main']['ordinat_s'].'';
    $marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter_withshadow&chld=A|9999FF|000000';
    $this->googlemaps->add_marker($marker);
        // maps : render
    $data['marker'] = $marker['icon'];
    $data['map'] = $this->googlemaps->create_map();

    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['all'] = $this->m_umkm_pendaftar->all_data();
    $data['kecamatan'] = $this->mp_signup->get_kecamatan();
    $data['kelurahan'] = $this->mp_signup->get_kelurahan();
    $this->render('form',$data);
    $this->load->view('js_maps');
  }

  public function save()
  {
    $id = 1;
    $data = html_remover($this->input->post(null,true));
    $cek = $this->m_umkm_pendaftar->by_field('nama_market', $data['nama_market']);
    $user_cek = $this->mp_signup->user_cek($data['username']);
    if ($id == null) {
      if ($this->menu['_create'] == 0) redirect(site_url().'/_error/error_403');
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Data sudah ada di sistem.');
        redirect(site_url().'/'.$this->controller.'/form/');
      }
      if ($user_cek->num_rows() >= 1) {
        $this->session->set_flashdata('flash_error', 'Username Telah terdaftar.');
        redirect(site_url().'/'.$this->controller.'/form/');
      }
      $_user = ['id' => md5(date('YmdHis').$data['username']),
      'user_password'=> md5(md5(md5($data['password']))),
      'user_name' => $data['username'],
      'group_id' => '297a139cfbf6720acb448ee95d9f7a12',
      'fullname' => $data['nama_pemilik'],
      'photo' => $this->session->userdata('img'),
      'is_active' => 0,
      'created_by' => $this->session->userdata('fullname')];
      $this->m_user->store($_user);

      $data['id'] = md5(date('YmdHis').$data['nama_pemilik']);
      if(!isset($data['is_active'])){$data['is_active'] = 0;}
      if(!isset($data['id_user'])){$data['id_user'] = $_user['id'];}
      unset($data['username']);
      unset($data['password']);
      unset($data['repassword']);
      $this->m_umkm_pendaftar->store($data);
      if ($this->session->userdata('img')) {
        $this->session->unset_userdata('img');
      }
      createLog(2, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    }else{
      if ($this->menu['_update'] == 0) redirect(site_url().'/_error/error_403');
      $user_id = $data['id_username'];
      if ($data['old_username'] != $data['username']) {
        if ($user_cek->num_rows() >= 1) {
          $this->session->set_flashdata('flash_error', 'Username sudah ada di sistem.');
          redirect(site_url().'/'.$this->controller.'/index/');
        }else{
          $_user = ['user_name' => $data['username']];
          $this->m_user->update($user_id, $_user);
        }
      }
      if ($this->session->userdata('img') != NULL) {
        $_user = ['photo' => $this->session->userdata('img')];
        $this->m_user->update($user_id, $_user);
      }
      // echo json_encode($data); die();
      unset($data['old_username']);
      unset($data['username']);
      unset($data['id_username']);
      unset($data['old']);

      $this->m_umkm_pendaftar->update($data['id'],$data);
      if ($this->session->userdata('img')) {
        $this->session->unset_userdata('img');
      }
      createLog(3, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    if ($this->menu['_delete'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    $data = $this->m_umkm_pendaftar->umkm_cek($id)->row();
    if ($data != NULL) {
      $user_cek = $this->m_umkm_pendaftar->user_cek($data->id_user)->row();
      if ($user_cek != NULL) {
        unlink('./images/users/'.$user_cek->photo);
        $this->m_user->delete($data->id_user);
      }
      $this->m_umkm_pendaftar->delete($id);
      createLog(4, $this->menu['menu']);
      $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
      redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
    }else{
      redirect(site_url().'/_error/error_403');
    }
  }

  public function status($type = null, $id = null)
  {
    if ($this->menu['_update'] == 0 || $type == null || $id == null) redirect(site_url().'/_error/error_403');
    if ($type == 'close') {
      $this->m_umkm_pendaftar->update($id, array('is_close' => 0));
      $this->m_umkm_produk->updateBymarket($id, array('is_close' => 0));
    }else{
      $this->m_umkm_pendaftar->update($id, array('is_close' => 1));
      $this->m_umkm_produk->updateBymarket($id, array('is_close' => 1));
    }
    createLog(3,$this->menu['menu']);
    redirect(site_url().'/'.$this->controller.'/index');
  }

  public function multiple($type = null)
  {
    if ($this->menu['_update'] == 0 || $this->menu['_delete'] == 0) redirect(site_url().'/_error/error_403');
    $data = $this->input->post(null,true);
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
          case 'delete':
          $this->m_umkm_pendaftar->delete($key);
          $flash = 'Data berhasil dihapus.';
          $t = 4;
          break;

          case 'enable':
          $this->m_umkm_pendaftar->update($key, array('is_active' => 1));
          $flash = 'Data berhasil diaktifkan.';
          $t = 3;
          break;

          case 'disable':
          $this->m_umkm_pendaftar->update($key, array('is_active' => 0));
          $flash = 'Data berhasil dinonaktifkan.';
          $t = 3;
          break;
        }
      }
    }
    createLog($t,$this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }
  
  public function authorization($id = null)
  {
    if ($this->menu['_update'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    createLog(3,$this->controller);
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['main'] = $this->m_umkm_pendaftar->by_field('id', $id);
    $data['detail'] = $this->m_umkm_pendaftar->detail_data($id);
    createLog(3,$this->menu['menu']);
    $this->render('authorization', $data);
  }

  public function authorize()
  {
    $data = html_escape($this->input->post(null,true));
    if ($this->menu['_update'] == 0 || $data == null) redirect(site_url().'/_error/error_403');
    $this->m_umkm_pendaftar->authorize($data);
    createLog(3,$this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

  public function upload()
  {
    $config['upload_path']          = './images/users/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['overwrite']            = true;
    $config['max_size']             = 10240; // 10MB
    $config['encrypt_name']         = TRUE;
    
    $this->load->library('upload', $config);
    if ($this->upload->do_upload('img')) {
      $this->session->set_userdata('img', $this->upload->data("file_name"));
    } else {
      $this->session->set_userdata('img', NULL);
    }

  }

  public function del_img()
  {
    $data = $this->input->post(NULL, TRUE);
    unlink('./images/users/'.$data['name']);
    $del = $this->m_user->update($data['id'], ['photo' => NULL]);
    if ($del) {
      echo json_encode(['status' => true]);
    }else{
      echo json_encode(['status' => false]);
    }
  }

  public function password_change()
  {
    $id = $this->session->userdata('user_id');
    if ($this->menu['_update'] == 0 || $id == null) redirect(site_url().'/_error/error_403');
    $data = html_escape($this->input->post(null,true));
    $data['user_password'] = md5(md5(md5($data['user_password'])));
    unset($data['user_password_confirm']);
    $this->m_user->update($id,$data);
    createLog(3, $this->menu['menu']);
    $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    redirect(site_url().'/'.$this->controller.'/index/'.$this->cookie['cur_page']);
  }

}