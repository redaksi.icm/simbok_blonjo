<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pa_slide extends CI_Model {

  public function list_data($data)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($data['search']['term'] != '') {
      $where .= "AND a.judul LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT * FROM pa_slide a 
      $where
      ORDER BY "
        .$data['order']['field']." ".$data['order']['type'].
      " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1";

    $sql = "SELECT * FROM pa_slide a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT COUNT(1) as total FROM pa_slide a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM pa_slide a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function store($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('pa_slide', $data);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('id',$id)->update('pa_slide', $data);
  }

  public function delete($id, $type = 1)
  {
    if ($type == 1) {
      $this->db->where('id', $id)->delete('pa_slide');
    }else{
      $data['is_deleted'] = 1;
      $data['deleted_at'] = date('Y-m-d H:i:s');
      $data['deleted_by'] = $this->session->userdata('fullname');
      $this->db->where('id', $id)->update('pa_slide', $data);
    }
  }

  public function img_cek($id)
  {
    return $this->db->get_where('pa_slide', ['id' => $id])->row();
  }

  public function del_img($id)
  {
    $this->db->update('pa_slide', ['img' => NULL], "id = $id");
  }
  
}