<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_p_detail extends CI_Model {

  public function list_data($data, $filed)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 AND b.ins_number = 0";
    if ($data['search']['term'] != '') {
      $where .= "AND a.nama_produk LIKE '%".$this->db->escape_like_str($data['search']['term'])."%' ";
    }

    $sql = "SELECT a.*, b.img FROM tb_produk a LEFT JOIN img_produk b ON a.id = b.produk_id
    $where
    ORDER BY "
    .$filed." "."DESC".
    " LIMIT ".$data['cur_page'].",".$data['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1";

    $sql = "SELECT COUNT(1) as total FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function by_field($field, $val)
  {
    $where = "WHERE a.is_deleted = 0 AND $field='".$val."' ";
    $sql = "SELECT * FROM tb_produk a $where";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function detail($id)
  {
    return $this->db->get_where('tb_produk', ['id' => $id, 'is_close' => 1])->row_array();
  }
  public function toko($id)
  {
    $this->db->select('a.*, b.user_name, b.photo');
    $this->db->from('tb_umkm a');
    $this->db->join('_user b', 'b.id = a.id_user');
    $query = $this->db->get();
    return $query->row_array();
  }

  public function get_produk($id)
  {
    $this->db->select('a.*, b.img, b.ins_number');
    $this->db->from('tb_produk a');
    $this->db->join('img_produk b', 'b.produk_id = a.id');
    $this->db->where('b.ins_number', 0);
    $this->db->where('a.id_umkm', $id);
    $this->db->order_by('a.updated_at', 'DESC');
    $this->db->limit(12);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_img($id)
  {
    return $this->db->get_where('img_produk', ['produk_id' => $id])->result_array();
  }
  
}