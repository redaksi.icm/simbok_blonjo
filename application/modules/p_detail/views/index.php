<?php echo $map['js']; ?>
<?php $i = 1; $l=1; $r = 1; $k = 1;?>
<style type="text/css">
  .fac{
    display: inline-block;
    border-radius: 50%;
    border: 5px solid #428bca;
    /*box-shadow: 1px 2px 5px #428bca;*/
    padding: 0.3em 0.3em;
  }
</style>
<div class="body-content outer-top-xs">
  <div class='container'>
    <div class='row single-product'>

      <div class='col-xs-12 col-sm-12 col-md-8 col-lg-8' style="margin-bottom: 20px;">
        <div class="detail-block">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100%;">
            <div class="product-item-holder size-big single-product-gallery small-gallery">

              <div id="owl-single-product">
                <?php foreach ($img as $v): ?>
                  <div class="single-product-gallery-item single-slide" id="slide<?=$i++?>">
                    <img class="img-auto" alt="" src="<?=base_url()?>images/<?php if($v['ins_number'] == 0){echo "tamnel";}else{echo "produk";}?>/<?=$v['img']?>"/>
                  </div>
                <?php endforeach ?>
              </div>
              <?php if (count($img) >= 2): ?>
                <div class="single-product-gallery-thumbs gallery-thumbs" style="margin-top: 1rem;">
                  <div id="owl-single-product-thumbnails">
                    <?php foreach ($img as $v): ?>
                      <div class="item" style="display: flex; align-items: center;justify-content: center; margin-right: 1rem;">
                        <a class="horizontal-thumb active" data-target="#owl-single-product" href="#slide<?=$k++?>">
                          <img class="img-responsive" alt="" src="<?=base_url()?>images/<?php if($v['ins_number'] == 0){echo "tamnel";}else{echo "produk";}?>/<?=$v['img']?>" style="height: 7rem" />
                        </a>
                      </div>
                    <?php endforeach ?>
                  </div>
                </div>
              <?php endif ?>


            </div>
          </div>

          <div class='col-sm-12 col-md-12 col-lg-12 product-info-block'>
            <div class="product-info">

              <div class="price-container info-container m-t-30">
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="price-box">
                      <h3 class="name"><?=@$main['nama_produk']?></h3>
                      <span class="price">Rp <?=numSys(@$main['harga'])?>,-</span>
                    </div>
                  </div>
                </div>
              </div>

              <span><b>Deskripsi :</b></span>
              <div class="description-container m-t-20">
                <p><?=@htmlspecialchars_decode($main['deskripsi'])?></p>
              </div>

              <div class="price-container info-container m-t-30">
                <div class="row">
                  <div class="col-sm-12 col-xs-12">
                    <div class="price-box">
                      <span><b>Letak/Posisi Produk :</b> KEBUMEN, <?=@$main['kecamatan'].", ".@$main['kelurahan'].", ".@$main['alamat_lengkap']?></span><br>
                      <span><b>Dibuat :</b> <?=date_gabung(@$main['created_at'])?></span><br>
                      <span><b>Diubah :</b> <?=date_gabung(@$main['updated_at'])?></span><br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class='col-xs-12 col-sm-12 col-md-4 col-lg-4' style="margin-bottom: 10px">
        <div class="detail-block">

          <div class="center-flex">
            <div class="image-cropper">
              <div class="center-flex">
                <?php if (@$toko['photo']): ?>
                  <img  src="<?=base_url()?>images/users/<?=$toko['photo']?>" alt="" class=" profile-pic">
                  <?php else: ?>
                    <img  src="<?=base_url()?>images/users/no-img.jpg" alt="" class=" profile-pic">
                  <?php endif ?>
                </div>
              </div>
            </div>
            <a href="<?=site_url()?>/p_toko/index/<?=$toko['id']?>"><p class="text-center" style="font-size: 20px;"><b><?=$toko['nama_market']?></b></p></a>
            <div class="" style="padding-left: 1.5rem">
              <span>Bergabung Pada :</span><br>
              <span><?=date_gabung($toko['created_at'])?></span>
              <br>
              <br>
              <span>Telephon :</span><br>
              <span><?=Tlp($toko['no_hp'])?></span>
              <br>
              <br>
              <span>Email :</span><br>
              <span><?=$toko['email']?></span>
            </div>
            <div class="col-md-12" style="border-top: 1px solid #F2F2F2; margin-top: 1em; margin-bottom: 1em"></div>
            <div class="row">
              <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="tel:<?=Tlp($toko['no_hp'])?>"><i title="Telephon" class="fas fa-phone-alt fa-2x text-primary"></i></a></div>
              <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="sms:<?=Tlp($toko['no_hp'])?>?body=Pesan%20Dari%20SIMBOK%20BLONJO."><i title="SMS" class="fas fa-comments fa-2x text-primary"></i></a></div>
              <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="https://wa.me/<?=Wa($toko['wa'])?>" target="blank"><i title="Whatsapp" class="fab fa-whatsapp text-primary" style="font-size: 2.5em;"></i></a></div>
              <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 center-flex"><a href="mailto:<?=$toko['email']?>?subject=SIMBOK%20BLONJO"><i title="Email" class="fas fa-envelope fa-2x text-primary"></i></a></div>
            </div>
          </div>
        </div>

        <div class='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
          <div class="detail-block form-group">
            <?=$map['html']?>
          </div>
        </div>

      </div>
      <div class="clearfix"></div>

      <div class="search-result-container " style="margin-bottom: 20px; margin-top: 20px">
        <div class="tab-content category-list">
          <div class="tab-pane active">
            <div class="category-product">
              <div class="row">
                <?php foreach ($produk as $v): ?>
                  <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                    <div class="item" title="<?=$v['nama_produk']?>">
                      <div class="products">
                        <div class="product main-product">

                          <div class="product-image">
                            <div class="image"> 
                              <a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>">
                                <img src="<?=base_url()?>images/tamnel/<?=@$v['img']?>" alt=""> 
                              </a> 
                            </div>
                          </div>

                          <div class="product-info text-left">
                            <h3 class="name name-limit"><a href="<?=site_url()?>/p_detail/index/<?=@$v['id']?>"><?=$v['nama_produk']?></a></h3>
                            <div class="product-price">
                              <span class="price">Rp<?=@numSys($v['harga'])?>,-</span>
                            </div>
                            <div class="row">
                              <div class="col-md-6 text-left">
                                <p class='card-bawah' style="margin-left: 0px; font-size: 10px "><?=@$v['kelurahan']?>, <?=@$v['kecamatan']?></p>
                              </div>
                              <div class="col-md-6 text-right">
                                <p class='card-bawah' style="margin-right: 10px; font-size: 10px "><?php if($v['updated_at'] != NULL){echo @dateCard($v['updated_at']);}else{echo @dateCard($v['created_at']);} ?></p>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                <?php endforeach ?>

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
