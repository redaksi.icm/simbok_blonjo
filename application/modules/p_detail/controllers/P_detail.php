<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P_detail extends PC_Controller {

  var $controller, $menu, $cookie;

  function __construct(){
    parent::__construct();
    $this->load->helper('text');
    
    $this->load->model(array(
      'pa_kategori/m_pa_kategori',
      'p_detail/m_p_detail',
      'p_toko/m_p_toko',
      'pa_slide/m_pa_slide',
      '_group/m_group',
      '_user/m_user',
      '_app/maps_model'
    ));

    $this->controller = 'p_detail';
    //cookie 
    $this->cookie = getCookieMenu($this->controller);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['cur_page'] == null) 0;
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 12;
  }

  public function index($id = NULL)
  {
    if ($id == NULL) {redirect(site_url('p_err/error404'));}
    $data['main'] = $this->m_p_detail->detail($id);
    if ($data['main']['id_umkm'] == NULL) {redirect(site_url('p_err/error404'));}
    $data['toko'] = $this->m_p_toko->toko($data['main']['id_umkm']);
    // maps : init
    $this->load->library('googlemaps');
    $config['center']   = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $config['zoom']   = '10';
    $this->googlemaps->initialize($config);
    // maps : polygon
    $polygon = array();
    $polygon['points']      = $this->maps_model->list_points(); 
    $polygon['strokeColor']   = '#F00000'; // Color = RED
    $polygon['strokeOpacity']   = '0.8';
    $polygon['strokeWeight']  = '2';  
    $polygon['fillColor']     = '';
    $polygon['fillOpacity']   = '0';
    $this->googlemaps->add_polygon($polygon);
    // maps : marker
    $marker = array();
    $marker['position'] = @$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'];
    $marker['infowindow_content'] = '<div style="font-weight: bold; font-size: 17px; margin-bottom: 10px; text-align: center;">'.@$data['main']['nama_produk'].'</div><table><tr><td class="column-spacing">Kecamatan</td> <td class="column-spacing">&nbsp;:&nbsp;</td> <td class="column-spacing">'.@$data['main']['kecamatan'].'</td></tr><tr><td class="column-spacing">'.'Kelurahan</td> <td class="column-spacing">&nbsp;:&nbsp;</td> <td class="column-spacing">'.@$data['main']['kelurahan'].'</td></tr></table><div style="margin-top: 10px;"><a href="https://www.google.com/maps/search/?api=1&query='.@$data['main']['ordinat_s'].','.@$data['main']['ordinat_e'].'" class="btn btn-block btn-sm btn-danger" target="_blank"><i class="fa fa-map-marker"></i> Buka Lokasi Pasar di Google Maps</a></div>';
    $this->googlemaps->add_marker($marker);
        // maps : render
    $data['map'] = $this->googlemaps->create_map();
    // var_dump($data['toko']['ordinat_e']); die();
    $data['img'] = $this->m_p_detail->get_img($id);
    $data['produk'] = $this->m_p_detail->get_produk($data['main']['id_umkm']);
    $this->render('index', $data);
    $this->load->view('js_maps');
  }

}